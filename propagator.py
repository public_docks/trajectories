#!/usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 26/04/2024
#	Program  	: DOCKS Propagator
#	Name 		: propagator.py
#	Authors		: Nima TRAORE ; Florian JOUSSEAUME; Laëtitia LEBEC; Rashika JAIN; Tomas NUNES
#	Version 	: 6.5
######################################################################

# Main program of the Propagator

import time
import cProfile
import pstats
import sys
import argparse
from src import instances


def propagate(disable_tqdm):
    # computing trajectory
    instances.builder.compute_traj(disable_tqdm=disable_tqdm)
    # (future feature ) #sampling quality of trajectory
    # (future feature ) instances.samp_qual.sampling_quality()
    # (future feature ) print("Results saved as png images in output directory !!\n")
    # generating ephemerides
    # (future feature ) instances.ref_eph.export_eph(trj)
    # (future feature ) print ("Success in creating ephemerides !!"


if __name__ == '__main__':
    disable_tqdm = False
    if sys.argv[-1] == "disable_tqdm":
        disable_tqdm = True
        sys.argv = sys.argv[:-1]

    # If Peformance Mode Enabled
    if sys.argv[-1]:
        # Create a cProfile object and enable profiling
        profiler = cProfile.Profile()
        profiler.enable()
    
    start_time = time.time()
    start_cpu = time.process_time()
    propagate(disable_tqdm)
    end_cpu = time.process_time()
    end_time = time.time()
    time_cpu = end_cpu - start_cpu
    time_elapsed = end_time - start_time
    print("\nSuccess in computing trajectory !!")
    print("Time elapsed = ", time_elapsed, "seconds")
    print("CPU Time = ", time_cpu, "seconds\n")
    
    if sys.argv[-1]:
        # Disable profiling and create the Stats object
        profiler.disable()
        # Sort the statistics by cumulative time
        stats = pstats.Stats(profiler)
        stats.sort_stats('cumulative')
        # Print the sorted statistics
        stats.print_stats()
