"""
Modified on 19/02/2024

@author: Tomás NUNES, feliu
"""

import numpy as np

# Import Planets Characteristics (from predefined_bodies.py, used in Propagator)
known_bodies = {           #[mu(m^3.kg^-1.s^-2)    , naifid,  Radius(m)]     
                "sun":     [132712440041940000000.0, 10,  696000000],
                "mercury": [22032080490394.35,       1,   2440530],
                "venus":   [324858606837104.9,       2,   6051800],
                "earth":   [398659293629478.30,      399, 6378136.3],
                "moon":    [4843941639988.467,       301, 1738000],
                "mars":    [42828313289311.500,      499, 3396190],
                "jupiter": [126686536751784000.0,    599, 71492000],
                "saturn":  [37931239677504600.0,     699, 60268000],
                "uranus":  [5793939212817970.0,      799, 25559000],
                "neptune": [6835099203587360.0,      899, 24764000]
                }
bodies_names = list(known_bodies.keys())

# Functions
def periapsis(radius, altitude):
    return radius + altitude

def semi_major_axis(periapsis, eccentricity):
    return periapsis/(1-eccentricity)
        
def apoapsis(semi_major_axis, eccentricity):
    ra = semi_major_axis*(1+eccentricity)
    return ra

def velocity(mu, periapsis, semi_major_axis):
    V = np.sqrt(2*mu/periapsis-mu/semi_major_axis)
    return V

def orbital_period(semi_major_axis, mu):
    T = 2 * np.pi * np.sqrt(semi_major_axis**3/mu)
    T /= 3600
    return T

# Initial Time (ISOT)
time_init = "2015-06-06T12:00:00"

# Input The Selected Body
print()
for i,p in enumerate(bodies_names):
    print(f'{i}: {p.capitalize()}')
planet_index = int(input("\nChoose a body by its number : "))
body_selected = bodies_names[planet_index]
print(f'\nBody Selected: {body_selected.capitalize()}\n')

# Define Orbit
alt = float(input("Choose an altitude at periapsis (km) : "))*1000
ecc = float(input("Choose an eccentricity : "))
inc = float(input("Choose an inclination (°): "))*np.pi/180

# Get Planet Characteristics
mu = known_bodies[body_selected][0]
R = known_bodies[body_selected][2]

# Compute Orbit Parameters
rp = periapsis(R, alt) # m
sma = semi_major_axis(rp, ecc) # m
ra = apoapsis(sma, ecc) # m
V = velocity(mu, rp, sma) # m/s
T = orbital_period(sma, mu) # h

# Detailed Orbital Period
time = T
hours = int(time) 
days = (hours / 24)
days = days - (days % 1)
minutes = (time*60) % 60
seconds = (time * 3600) % 60 
hours = hours %24

# Obtain Initial State
state_vector = np.zeros(6) # ISOT, km and km/s
state_vector[0] = rp / 1000
state_vector[4] = V*np.cos(inc) / 1000
state_vector[5] = V*np.sin(inc) / 1000

# Write Initial Conditions File
file_write = f'{time_init}\t\t{"{:.15e}".format(state_vector[0])}\t{"{:.15e}".format(state_vector[1])}\t{"{:.15e}".format(state_vector[2])}\t{"{:.15e}".format(state_vector[3])}\t{"{:.15e}".format(state_vector[4])}\t{"{:.15e}".format(state_vector[5])}'
# Write Initial Conditions File
with open(f'Init_cond_{body_selected.capitalize()}.txt', 'w') as fo:
    fo.write(file_write)
print()
print(75*"#","\n")
print(f"Your initial conditions file '{f'Init_cond_{body_selected.capitalize()}.txt'}' for '{body_selected.capitalize()}' trajectory is ready")

# Print in Terminal
print(f"""\nYour trajectory have the following parameters:
    - Periapsis : {rp/1000:.5f} km
    - Apoapsis : {ra/1000:.5f} km
    - Semi major axis : {sma/1000:.5f} km
    - Velocity : {V/1000:.5f} km/s
    - Orbital period : {T:.5f} hour(s)
                    -> {days:.0f} d {hours:.0f} h {minutes:.0f} m {seconds:.0f} s""")