#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 19/09/2019
#	Program  	: DOCKS Propagator
#	Name 		: bodies.py
#	Authors		: Nima TRAORE ; Florian JOUSSEAUME; Laëtitia LEBEC; Rashika JAIN
#	Version 	: 4.0
######################################################################

import numpy as np
from pyquaternion import Quaternion

import spice_body_acess as sba
from src import eph_parser
from src import quaternion_parser
from src import rotation_matrix_parser
from src import sph_harmonics_parser


class Body:
    """Class defining a celestial body.
  
    Attributes defined here:
    -name: the name of the body.
    -equatorial_radius: equatorial radius of the body
    -mu: gravitational parameter of the body

    Method defined here:
    -acceleration_intensity: gives the acceleration intensity of body
    -relative_position(): defines relative position of spacecraft with 
    respect to body.

    """

    def __init__(self, name, mu=0.0):  # , equatorial_radius = 0.0):
        """Constructor of the class Body."""
        self._name = name
        self._mu = mu
        # self._equatorial_radius = equatorial_radius

    def _get_name(self):
        """Method called when trying to read the attribute 'name'."""
        return self._name

    def _set_name(self, new_name):
        """Method called when trying to modify the attribute 'name'."""
        self._name = new_name

    name = property(_get_name, _set_name)

    def __repr__(self):
        """Method displaying a customized message when an instance of the 
        class BaseModel is called in the command line.
        """
        # return "Body: name = '{}', mu = '{}', radius = '{}' m".format(
        # self.name, self.mu, self.equatorial_radius)
        return "Body: name = '{}', mu = '{}'".format(self._name, self._mu)

    def accel_intensity(self, date, satellite_position):
        """Computes the acceleration intensity undergone by spacecraft."""
        rp = self.relative_position(date, satellite_position)
        acc_intens = self._mu / (rp ** 2)
        return acc_intens

    def relative_position(self, date, satellite_position):
        """Gives the relative position of the spacecraft with respect to 
        a body. Only works if both eph and satellite pos are ssb centered. Returns a vector position.
        """
        body_position, _, _ = np.array(self.eph(date))
        r_p = satellite_position - body_position

        return r_p


class UnKnownBody(Body):
    """Class defining the gravitational model of desired celestial bodies.

    This class inherits from class Body.

    Attributes defined here:
    -input_dir: input file directory (class attribute)
    -traj_file: name of body's ephemerides file.
    
    Inputs:
    -name of the celestial body
    -mu
    -radius
    -traj_file: name of body's ephemerides files

    Method defined here:
    -eph(): gives the ephemeris of unknown body with respect to an epoch
  
    """

    input_dir = " "

    # def __init__(self, name, mu, radius, naif_id):
    def __init__(self, name, mu, traj_file, naif_id, radius):
        """Constructor of the class UnKnownBody."""
        Body.__init__(self, name)
        self._mu = mu
        self._equatorial_radius = radius
        self._traj_file = traj_file
        self._naif_id = naif_id
        if self._traj_file != 'none':
            self.function_int = eph_parser.EphParser()
            self.interpol = self.function_int.eph(traj_file)

    def _get_mu(self):
        """Method called when trying to read the attribute 'mu'."""
        return self._mu

    def _set_mu(self, new_mu):
        """Method called when trying to modify the attribute 'mu'."""
        self._mu = new_mu

    def _get_equatorial_radius(self):
        """Method called when trying to read the attribute 'equatorial_radius'."""
        return self._equatorial_radius
    
    def _set_equatorial_radius(self, new_equatorial_radius):
        """Method called when trying to modify the attribute 'equatorial_radius'."""
        self._equatorial_radius = new_equatorial_radius

    def _get_traj_file(self):
        """Method called when trying to read the attribute 'traj_file'."""
        return self._traj_file

    def _set_traj_file(self, new_traj_file):
        """Method called when trying to modify the attribute 'traj_file'."""
        self._traj_file = new_traj_file

    def _get_naif_id(self):
        """Method called when trying to read the attribute 'traj_file'."""
        return self._naif_id

    def _set_naif_id(self, new_naif_id):
        """Method called when trying to modify the attribute 'traj_file'."""
        self._naif_id = new_naif_id

    mu = property(_get_mu, _set_mu)
    equatorial_radius = property(_get_equatorial_radius, _set_equatorial_radius)
    traj_file = property(_get_traj_file, _set_traj_file)
    naif_id = property(_get_naif_id, _set_naif_id)

    def __repr__(self):
        """Method displaying a customized message when an instance of the 
        class UnKnownBody is called in the command line.
        """
        # return "UnKnownBody: name = '{}', mu = {} m³/s², radius = '{}' m".format(
        # self._name, self._mu, self._equatorial_radius)
        return "UnKnownBody: name = '{}', mu = {} m³/s²".format(
            self._name, self._mu)

    def eph(self, date):
        """Method providing the ephemeris of unknown body wrt ssb at a given epoch.
        date: MJD seconds
        Returns a tuple r, v, a
        """
        # Date in MJD days
        # date = Time(date, format='mjd', scale='tdb')
        if self._traj_file != 'none':
            if date <= self.interpol[2][0] - 86400 or \
                    date >= self.interpol[2][-1] + 86400:
                print("The date is out of bounds of the ", self.name, "ephemerides. ")
                quit()

            fr = self.interpol[0]
            fv = self.interpol[1]
            fa = self.interpol[4]

            r = fr(date)
            v = fv(date)
            a = fa(date)

        else:
            # date = 2400000.5 + date / 86400  # mjd seconds to jd
            # eph_values = sba.eph_object_SSB(eph_information['spice'], self._naif_id, date)
            # eph_values = np.array(eph_values) * 1000
            eph_values = sba.get_state_SSB(self._naif_id, date) * 1000

            r = eph_values[0:3]
            v = eph_values[3:6]
            a = eph_values[6:9]

        return r, v, a


class ComplexGravBody:
    """Class defining the complex gravitational model of a desired body

    This class inherits from class Body.

    Attributes defined here:
    -traj_file: name of body's ephemerides file.
    -quaternion_file: name of body's quaternion file.

    Method defined here:
    -eph(): gives the ephemeris of the body with respect to an epoch
    -quat(): gives the quaternion of the body with respect to an epoch
  
    """

    def __init__(self, name, eph_method, eph_name, rot_method, rot_name, quaternion_file, sph_harm_file, degree):
        """Constructor of the class UnKnownBody."""
        Body.__init__(self, name)
        self._eph_method = eph_method
        self._eph_name = eph_name
        if eph_method.lower() == 'file':
            self.function_int = eph_parser.EphParser()
            self.interpol = self.function_int.eph(eph_name)

        self._rot_method = rot_method
        self._rot_name = rot_name
        if rot_method.lower() == 'file':
            self.function_rotmatrix = rotation_matrix_parser.RotMatrixParser()
            self.rotation_matrix = self.function_rotmatrix.rotmatrix(rot_name)

        self._quaternion_file = quaternion_file
        self.function_quat = quaternion_parser.QuatParser()
        if quaternion_file != "none":
            self.quaternion = self.function_quat.quat(quaternion_file)

        self._sph_harm_file = sph_harm_file
        self.degree = degree
        if sph_harm_file != 'none':
            self.function_sph_harm = sph_harmonics_parser.SphHarm()
            self.sh_coeff = self.function_sph_harm.sph_harm(sph_harm_file, degree, degree)


    def _get_eph_method(self):
        """Method called when trying to read the attribute 'traj_file'."""
        return self._eph_method

    def _set_eph_method(self, new_eph_method):
        """Method called when trying to modify the attribute 'traj_file'."""
        self._eph_method = new_eph_method

    def _get_eph_name(self):
        """Method called when trying to read the attribute 'traj_file'."""
        return self._eph_name

    def _set_eph_name(self, new_eph_name):
        """Method called when trying to modify the attribute 'traj_file'."""
        self._eph_name = new_eph_name

    def _get_quaternion_file(self):
        """Method called when trying to read the attribute 'traj_file'."""
        return self._quaternion_file

    def _set_quaternion_file(self, new_quaternion_file):
        """Method called when trying to modify the attribute 'traj_file'."""
        self._quaternion_file = new_quaternion_file

    def _get_name(self):
        """Method called when trying to read the attribute 'name'."""
        return self._name

    def _set_name(self, new_name):
        """Method called when trying to modify the attribute 'name'."""
        self._name = new_name
    
    def _get_equatorial_radius(self):
        return self.sh_coeff[2]

    name = property(_get_name, _set_name)
    eph_method = property(_get_eph_method, _set_eph_method)
    eph_name = property(_get_eph_name, _set_eph_name)
    quaternion_file = property(_get_quaternion_file, _set_quaternion_file)

    def eph(self, date):
        """Method providing the ephemeris of a celestial body at a given epoch.
        Returns a tuple r, v, a
        """

        # Date in MJD days
        # date = Time(date, format='mjd', scale='tdb')

        if self._eph_method == "file":
            if date <= self.interpol[2][0] - 86400 or \
                    date >= self.interpol[2][-1] + 86400:
                print("The date is out of bounds of the ", self.name, "ephemerides.")
                quit()

            fr = self.interpol[0]
            fv = self.interpol[1]
            fa = self.interpol[4]

            r = fr(date)
            v = fv(date)
            a = fa(date)

        elif self._eph_method.lower() == "naifid":
            # date = date / 86400 + 2400000.5  # mjd to jd
            # values = sba.eph_object_SSB(eph_information['spice'], self._eph_name, date)  # km
            # values = np.array(values) * 1000  # m
            values = sba.get_state_SSB(self._eph_name, date) * 1000

            r = values[0:3]
            v = values[3:6]
            a = values[6:9]

        return r, v, a

    def quat(self, date):
        """Method providing the quaternion values of a celestial body at a given epoch.
        Return the quaternion q
        """

        if date <= self.quaternion[0][0] - 86400 or date >= self.quaternion[0][-1] + 86400:
            print("The date is out of bounds of the ", self.name, "ephemerides. ")
            quit()

        # QUATERNION METHOD
        fq = self.quaternion[1]
        for i in range(len(self.quaternion[0])):
            if date == self.quaternion[0][-1]:
                q = Quaternion.slerp(fq[i], fq[i], 1)
            if date != self.quaternion[0][-1]:
                if self.quaternion[0][i] <= date <= self.quaternion[0][i + 1]:
                    q = Quaternion.slerp(fq[i], fq[i + 1], 1 / (self.quaternion[0][i + 1] - self.quaternion[0][i]))

        return q

    def rotmatrix(self, date):
        """Method providing the rotation matrix of a celestial body at a given epoch.
        Return the rotation matrix rm
        """

        # Date in MJD
        # date = Time(date, format='mjd', scale='tdb')
        if self._rot_method.lower() == "file":
            if date <= self.rotation_matrix[0][0] - 86400 or \
                    date >= self.rotation_matrix[0][-1] + 86400:
                print("The date is out of bounds of the ", self.name, "ephemerides.")
                quit()
            frm = self.rotation_matrix[1]
            rm = frm(date).as_matrix()
            # rm = np.asmatrix(rm_tab)
            #rm = rm_tab.reshape((3,3))
        
        elif self._rot_method.lower() == "spice":
            rm = sba.get_rot_mat(self._rot_name, date)

        return rm
