#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 26/04/2024
#	Program  	: DOCKS Propagator
#	Name 		: complex_models.py
#	Authors		: Laëtitia LEBEC; Rashika JAIN; Tomas NUNES
#	Version 	: 4.2
######################################################################


import numpy as np
from pyquaternion import Quaternion

# Importing Complex Gravitational Models functions
from src.model_tensor.gravitational.accelHarmonic import AccelHarmonic
from src.config import list_sph_harm
from src.tools import spacecraft


class ComplexBody:
    """ Virtual class giving the potential acceleration of body as a non-perfect sphere.
    The content of this class is designed to allow the scientist to implement its own model.

    Attributes defined here:
    -body: instance of class ComplexGravBody.

    Method defines here:
    -get_accelerations(): Returns the acceleration vector, in m/s², at 
    the current date, in the computation_center centered ICRS frame
    -ephemeris(): Returns ephemeris, rotation matrix (if needed) and quaternion
    (if needed) values of the celestial body at the current date
    
    Input:
    -celestial body's name
    -current date in mjd
    -satellite position in m
    -central body position in m
    
    Returns the perturbative acceleration vector, in m/s², at the current date
    """

    def __init__(self, name):
        """Constructor of class Complex_models."""
        self.name = name
        from src.instances import cgm_bodies
        for bodi in cgm_bodies:
            if bodi.name == self.name:
                self.body = bodi
        
        self.sh = AccelHarmonic()
        # If Mass propagation is implemented this as to change like in SRP. [In this case, like it is, mass of sat used here will always be constant to m0]
        self.sat = spacecraft.Spacecraft()
        self.sat_mu = self.sat.get_mu()


    def ephemeris(self, date):
        """
        Method providing several information about the desired celestial body
        for a given epoch:
        -the ephemeris values of the body
        -the rotation matrix of the body if needed
        -the quaternion of the body if needed
        
        Inputs:
        -body's name
        -date, in seconds, in mjd format
        
        This method returns the body's position (in m), velocity (in m/s), 
        acceleration (in m/s²), rotation matrix and quaternion values for a 
        given epoch
        """

        body_position, body_velocity, body_acceleration = np.array(self.body.eph(date))  # Calling for ephemerides values

        if self.body._rot_method.lower() != "none":
            rotation_matrix = np.array(self.body.rotmatrix(date))  # Calling for rotation matrix
        else:
            rotation_matrix = "none"

        if self.body._quaternion_file != "none":
            quaternion = np.array(self.body.quat(date))  # Calling for quaternion
        else:
            quaternion = Quaternion(1, 0, 0, 0)

        return body_position, body_velocity, body_acceleration, rotation_matrix, quaternion, self.body.sh_coeff, self.body.degree

    def get_acceleration(self, date, satellite_position, local_frame, r_center_computation_wrt_ssb):
        """
        Method providing the sum of the accelerations, in m/s², undergone by the 
        spacecraft from complex gravitational models of the celestial bodies 
        (non-perfect sphere) to be integrated into the propagation.
        
        Inputs:
        -
        
        Returns the diagonal matrix of the sum of the accelerations given by the 
        several User's functions, each corresponding to the complex gravitational 
        model of a celestial body. This acceleration has to be in m/s².
        """

        body_position_inertial, _, _, rot_matrix, _, sh_coeff, deg = self.ephemeris(date)
        r_BC = r_center_computation_wrt_ssb - body_position_inertial
        r_p = satellite_position + r_BC #m
        # Inertial Acceleration
        acc = self.sh.compute_acceleration(r_p, rot_matrix, deg + 1, sh_coeff[3],
                                             sh_coeff[2], sh_coeff[0], sh_coeff[1])

        # Non Inertial Acceleration
        if local_frame.name != 'ssb':
            if self.name != local_frame.name:
                # Position of Center of computation wrt to body
                r_center = r_center_computation_wrt_ssb - body_position_inertial
                acc -= self.sh.compute_acceleration(r_center, rot_matrix, deg + 1, sh_coeff[3],
                                             sh_coeff[2], sh_coeff[0], sh_coeff[1])
            else: # Central Body Case
                acc -= self.sat_mu * (r_p / np.linalg.norm(r_p) ** 3)


        return np.diag(acc)
