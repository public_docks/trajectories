#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 26/04/2024
#	Program  	: DOCKS Propagator
#	Name 		: sphr.py
#	Authors		: Nima TRAORE ; Florian JOUSSEAUME; Laëtitia LEBEC; Rashika JAIN; Harshul SHARMA; Tomas NUNES
#	Version 	: 4.1
######################################################################


import numpy as np
from src.model_tensor import base_model
from src.tools import spacecraft

class SphericalBody(base_model.BaseModel):
    """Class giving the potential acceleration of body as a perfect sphere.

    This class inherits from class base_model.BaseModel.

    Attributes defined here:
    -body: instance of class UnKnownBody.

    Method defined here:
    -get_acceleration(): method inherits from Super-class.

    Input:
    -celestial body's name
    -current date in mjd
    -satellite position in m
    -central body position wrt SSB in m
    
    Returns the perturbative acceleration vector, in m/s², due to the given celestial body
    """

    def __init__(self, name):
        """Constructor of the class SphericalBody."""
        base_model.BaseModel.__init__(self, name)
        self.name = name
        from src.instances import Bodies
        for bodi in Bodies:
            if bodi.name == self.name:
                self.body = bodi
        # If Mass propagation is implemented this as to change like in SRP. [In this case like it is, mass of sat used here will always be constant to m0]
        self.sat = spacecraft.Spacecraft()
        self.sat_mu = self.sat.get_mu()

    def get_acceleration(self, date, satellite_position, local_frame, r_center_computation_wrt_ssb):
        """Method computing the potential acceleration of a given body as
        a perfect sphere. Returns an acceleration tensor 3*3, in m/s².
        """
        body_position_inertial, _, _ = np.array(self.body.eph(date))  #m #wrt ssb
        r_BC = r_center_computation_wrt_ssb - body_position_inertial
        r_p = satellite_position + r_BC #m
        mu_body = np.float64(self.body._get_mu()) #m³/s²
        # Inertial Accelration
        acc = -mu_body * (r_p / np.linalg.norm(r_p) ** 3) #m/s²

        # Non Inertial Acceleration
        if local_frame.name != 'ssb':
            if self.name != local_frame.name:
                # Position of Center of computation wrt to body
                r_center = r_center_computation_wrt_ssb - body_position_inertial
                acc -= -mu_body * (r_center / np.linalg.norm(r_center) ** 3) #m/s²
            else: # Central Body Case
                acc *= (1 + self.sat_mu / mu_body)


        return self.vec_to_tensor(acc)
