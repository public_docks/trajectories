#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 26/04/2024
#	Program  	    : DOCKS Propagator
#	Name 		    : accelHarmonic.py
#	Authors		    : Rashika JAIN; Tomas NUNES
#	Version 	    : 1.1
######################################################################


import numpy as np


class AccelHarmonic:

    def compute_acceleration(self, r_p, E, degree, GM, R_ref, C, S):

        """
        AccelHarmonic
        
        Purpose:
            
            Computes the acceleration due to the harmonic gravity field of the
            central body
            
        Input:
        
        :param r_p: Vector np.double 1*3, Satellite position vector in the inertial system
        :param E : Matrix np.double 3x3, Transformation matrix to body-fixed system
        :param degree : np.int, the degree upto which the effect of spherical harmonics should be considered
        :param GM : np.double, Gravitational coefficient
        :param R_ref : np.double, Reference radius
        :param C : Matrix degree*degree, Spherical harmonics coefficients Cnm
        :param S : Matrix degree*degree, Spherical harmonics coefficients Snm
    
        Output:
        
        :param accel : Vector np.double 1*3, Acceleration (a=d^2r/dt^2)
        
        """

        """
        Local variables
        
        np.double  r,r_sqr                         # Auxiliary quantities
        np.double  x0,y0,z0,x_iy                   # Normalized coordinates
        np.double  acc_x_sh,acc_y_sh,acc_z_sh      # Acceleration vector
        np.double  V                               # Gravitational potential
        
        """

        #################################################"""
        r_p = E.dot(r_p)
        r = np.linalg.norm(r_p)
        r_sqr = r ** 2
        x0 = r_p[0] / r_sqr
        y0 = r_p[1] / r_sqr
        z0 = r_p[2] / r_sqr
        x_iy = x0 + y0 * 1j

        V = np.zeros((degree + 1, degree + 1), dtype=complex)
        V[0][0] = 1 / r
        for n in range(1, degree + 1):
            for m in range(n + 1):
                if n == m:
                    V[m][m] = (2 * m - 1) * x_iy * V[m - 1][m - 1]
                # elif m == 0 and n != 1:
                #     V[n][0] = (((2 * n - 1) * z0 * V[n - 1][0]) - ((n - 1) * V[n - 2][0] / r_sqr)) / n
                elif n == m + 1:
                    V[m + 1][m] = (2 * m + 1) * z0 * V[m][m]
                elif n > m + 1:
                    V[n][m] = (((2 * n - 1) * z0 * V[n - 1][m]) - ((n + m - 1) * V[n - 2][m] / r_sqr)) / (n - m)
        V = -V

        # #----------------------------------
        # # Calculate accelerations ax,ay,az
        # #----------------------------------

        ### DERIVATIVES
        delV_x = np.zeros((degree, degree), dtype=complex)
        delV_y = np.zeros((degree, degree), dtype=complex)
        delV_z = np.zeros((degree, degree), dtype=complex)
        for n in range(degree):
            for m in range(n + 1):
                if m == 0:
                    delV_x[n][0] = (-V[n + 1][1] - np.conjugate(V[n + 1][1])) / 2
                    delV_y[n][0] = 1j * (V[n + 1][1] - np.conjugate(V[n + 1][1])) / 2
                    delV_z[n][0] = -(n + 1) * V[n + 1][0]
                else:
                    delV_x[n][m] = -V[n + 1][m + 1] / 2 + ((n - m + 1) * (n - m + 2) * V[n + 1][m - 1] / 2)
                    delV_y[n][m] = 1j * ((V[n + 1][m + 1] / 2) + ((n - m + 1) * (n - m + 2) * V[n + 1][m - 1] / 2))
                    delV_z[n][m] = -(n - m + 1) * V[n + 1][m]

        ### computing acceleration
        delV_x_total = 0 + 0j
        delV_y_total = 0 + 0j
        delV_z_total = 0 + 0j

        for n in range(degree):
            for m in range(n + 1):
                k = (R_ref ** n) * (C[n][m] - S[n][m] * 1j)
                delV_x_total += k * delV_x[n][m]
                delV_y_total += k * delV_y[n][m]
                delV_z_total += k * delV_z[n][m]

        acc_x_sh = -GM * delV_x_total.real
        acc_y_sh = -GM * delV_y_total.real
        acc_z_sh = -GM * delV_z_total.real

        accel_bd = np.array([acc_x_sh, acc_y_sh, acc_z_sh])

        ### inertial frame acceleration
        accel = E.T.dot(accel_bd)

        return accel
