#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 18/05/2016
#	Program  	: DOCKS Propagator
#	Name 		: base_model.py
#	Authors		: Nima TRAORE
#	Version 	: 1.1
######################################################################

import numpy as np


class BaseModel:
    """Class defining the acceleration's format for numerical integration.

    Attributed defined here:
    -name: the name of the object.

    Method defined here:
    -vec_to_tensor(): provides the acceleration of a given object as tensor.

    """

    def __init__(self, name):
        """Constructor of the class BaseModel."""
        self._name = name

    def _get_name(self):
        """Method called when trying to read the attribute 'name'."""
        return self._name

    def _set_name(self, new_name):
        """Method called when trying to modify the attribute 'name'."""
        self._name = new_name

    name = property(_get_name, _set_name)

    def __repr__(self):
        """Method displaying a customized message when an instance of the 
        class BaseModel is called in the command line.
        """
        return "BaseModel: name = '{}'".format(self.name)

    def get_acceleration(self):
        """Virtual method."""
        pass

    def vec_to_tensor(self, acc):
        """Transforms a given acceleration vector into a matrix tensor 3*3.
        """
        matrix_tensor = np.array([[acc[0], 0, 0], [0, acc[1], 0], [0, 0, acc[2]]])
        return matrix_tensor
