#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 26/04/2024
#	Program  	: DOCKS Propagator
#	Name 		: srp.py
#	Authors		: Nima TRAORE ; Florian JOUSSEAUME; Laëtitia LEBEC; Rashika JAIN; Tomas NUNES
#	Version 	: 4.1
######################################################################


import numpy as np

from src.model_tensor import base_model
from src.tools import spacecraft


class RadiationPressure(base_model.BaseModel):
    """Class giving the solar radiation pressure undergone by the spacecraft.

    This class inherits from class base_model.BaseModel.

    Attributes defined here:
    -solar_pression: the pression due to the interaction of photons
    -solar_flux: conventional flux got in Earth from Sun.

    Methods defined here:
    -get_acceleration(): computes the srp acceleration.
    
    Input:
    -celestial body's name
    -current date in mjd
    -satellite position in m
    -cental body position wrt SSb in m
    
    Returns the acceleration vector (in m/s²)
    """

    _luminosity = 0.0  # Watts, luminosity of sun

    def __init__(self, name):
        """Constructor of the class RadiationPressure."""
        base_model.BaseModel.__init__(self, name)
        # Careful when feature of propagatin mass is added
        self.sat = spacecraft.Spacecraft()
        from src.instances import Bodies
        for bodi in Bodies:
            if bodi.name.lower() == "sun":
                self.sun = bodi
        self.c = 299792458 # light speed in vaccum m/s

    def get_luminosity(self):
        """Method called when trying to read the attribute 'luminosity'."""
        return RadiationPressure._luminosity

    def __repr__(self):
        """Method displaying a customized message when an instance of the 
        class BaseModel is called in the command line.
        """
        return "SRP: name = '{}', solar luminosity = '{} W'".format(
            self.name, RadiationPressure._luminosity)

    def get_acceleration(self, date, satellite_position, r_center_computation_wrt_ssb, local_frame):
        """Method computing the solar radiation pressure (srp) undergone
        by the spacecraft. returns a tensor 3*3, in m/s².
        The parameter "satellite_position" is expressed in m.
        """

        # compute satellite position with respect to sun
        r_sun_ssb, _, _ = self.sun.eph(date)  # sun wrt ssb
        raux = r_center_computation_wrt_ssb - r_sun_ssb
        r_p = satellite_position + raux  # probe wrt sun
        r_p_norm = np.linalg.norm(r_p)
        satellite_position_norm = np.linalg.norm(satellite_position)

        # Eclipse, Umbra/Penumbra Computation due to only Central Body
        if not (local_frame.name.lower() == 'ssb' or local_frame.name.lower() == 'sun'):

            a = np.arcsin(self.sun._get_equatorial_radius() / r_p_norm)
            b = np.arcsin(local_frame._get_equatorial_radius() / satellite_position_norm)
            c = np.arccos(np.dot(-r_p/r_p_norm, -satellite_position/satellite_position_norm))

            # No Occultation (Full Sunlight)
            if c >= a+b:
                v = 1
            # Total Occulation (Umbra)
            elif c < abs(a-b):
                v = 0
            # Partial Occulation (Penumbra)
            else:
                x = (c**2 + a**2 - b**2) / (2*c)
                y = np.sqrt(a**2 - x**2)
                A = a**2 * np.arccos(x/a) + b**2 * np.arccos((c-x)/b) - c*y
                v = 1 - A / (np.pi * a**2)
        
        # When Central Body is Sun or SSB it is always assumed full sunlight
        else:
            v = 1
        
        # SRP Acceleration in Full Sunlight 
        acc = v * self.get_luminosity() / (4 * np.pi * self.c) * self.sat.get_reflectivity() * self.sat.get_area_exposed_to_sun() / self.sat.get_mass() * r_p / np.linalg.norm(r_p)**3

        return self.vec_to_tensor(acc)
