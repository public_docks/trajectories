#!/usr/bin/env python3
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 28/09/2021
#	Program  	: DOCKS Propagator
#	Name 		: input_scenario_parser.py
#	Authors		: Tim GLASIUS; Rashika JAIN; Harshul SHARMA
#	Version 	: 2.0
######################################################################

# Even if backward propagation is indicated in config file by a negative time-step,
# input Burn Sequence file must be sequenced in forward time direction
# and have the thrust direction with respect to forward propagation.
# All the required burn sequence modifications for backward propagation 
# are performed automatically in this script.  

from os import path

import numpy as np
from src.config import time_step
#import regex as re


# =============================================================================
# def trunc(date):
#     """
# 
#     Parameters
#     ----------
#     date : float
#         Input time in MJD seconds part.
# 
#     Returns
#     -------
#     date_conv : float
#         date truncated with 8 decimal numbers
# 
#     """
#     date = date / 86400.
#     date_conv = float(re.match(r'\d+.\d{8}', '{:.12f}'.format(date)).group(0))
#     # date_conv = float(re.match(r'\d+.\d{8}', str(date)).group(0))
#     return date_conv
# 
# 
# def round_up(date):
#     """
# 
#     Parameters
#     ----------
#     date : Float
#         Input time in MJD seconds part.
# 
#     Returns
#     -------
#     date_v : Float
#         Rounded up value of time with 7 decimal numbers.
# 
#     """
#     date = (date / 86400.) * 10 ** 7
#     date_v = np.ceil(date) / (10 ** 7)
#     return date_v
# =============================================================================


class input_file_data:
    """
    Class collecting all the data from the input propulsion file
    
    Input :  path + file name , time step of propagator
    
    Output : 5 lists ( magnitude , start time , end time , direction of thrust in tangential frame,
     tangential frame's body)
        
    """

    def __init__(self, file_name):
        self.file_name = file_name
        self.datafile = None
        self.file_filter = []
        self.mag = []
        self.t_start = []
        self.t_end = []
        self.dt = []
        self.body_prop = []
        self.direction = None
        self.read()
        self.split_man()
        #self.check_time_step(time_step)

    def read(self):

        file_path = path.realpath("%s" % self.file_name)
        with open(file_path, 'r') as F:
            self.datafile = F.readlines()

# =============================================================================
#     def check_time_step(self, time_step):
#         """
#         Def warning the user if the time between the beginning and the enndding of an propulsion event is too short
#         in comparaison to the propagator time step.
# 
#         Warning !!!!  This only works for RK4 and for a propagator time step expressed in seconds.
#         Parameters
#         ----------
#         time_step : float
#             DESCRIPTION: time step used by the propagator
# 
#         Returns
#         -------
#         Warning message + begin dates of the concerned events.
# 
#         """
#         l = len(self.t_start)
#         delta_T = []
#         events = []
# 
#         for i in range(l):
#             if ((np.float64(self.t_end[i]) - np.float64(self.t_start[i])) * 86400) < (time_step * 0.99):
#                 events.append(self.t_start[i])
#                 delta_T.append((np.float64(self.t_end[i]) - np.float64(self.t_start[i])) * 86400)
# 
#         if delta_T:
#             l2 = len(delta_T)
#             for i in range(l2):
#                 print('Warning propulsion time step too low : some event might be ignored. ', 'Events concerned : ',
#                       events[i], '. Prop time between start @ end : ', delta_T[i])
# =============================================================================

    def split_man(self):
        """
        Def placing all the values of the input file in their respective lists and if required modifies these values for
        backward propagation.        
        
        Returns
        -------
        5 lists ( magnitude , start time , end time , direction of thrust in tangential frame , tangential frame's body)

        """
        l = len(self.datafile)

        meta_stop = "META_STOP"
        start_line = 0

        for i in range(l):
            self.datafile[i] = self.datafile[i].split()

        for i in range(l):
            if self.datafile[i]:
                self.file_filter.append(self.datafile[i])

        l2 = len(self.file_filter)

        for i in range(l2):
            if self.file_filter[i][0] == meta_stop:
                start_line = i + 1
                break

        if start_line == 0:
            print("META_STOP not detected")

        self.direction = np.zeros((l2 - start_line, 3))

        for i in range(start_line, l2):
            self.t_start.append(str(float(self.file_filter[i][0])*86400. + (float(self.file_filter[i][1]))))
            self.t_end.append(str(float(self.file_filter[i][2])*86400. + (float(self.file_filter[i][3]))))
            self.dt.append(((np.float64(self.file_filter[i][2])-np.float64(self.file_filter[i][0]))*86400.)+(np.float64(self.file_filter[i][3])-np.float64(self.file_filter[i][1])))
            self.direction[i - start_line, 0] = self.file_filter[i][4]
            self.direction[i - start_line, 1] = self.file_filter[i][5]
            self.direction[i - start_line, 2] = self.file_filter[i][6]
            self.mag.append(self.file_filter[i][7])
            self.body_prop.append(self.file_filter[i][8])


        sign = np.sign(time_step)
        if sign == -1.0:
            self.t_start, self.t_end = self.t_end, self.t_start
            self.t_start.reverse()
            self.t_end.reverse()
            self.direction = -self.direction

# =============================================================================
#         if (sign*float(self.t_start[0]))>(sign*float(self.t_end[0])):
#             self.t_start, self.t_end = self.t_end, self.t_start
#             
#         if (sign*float(self.t_start[0]))>(sign*float(self.t_start[-1])):
#             self.t_start.reverse()
#             self.t_end.reverse()
# =============================================================================