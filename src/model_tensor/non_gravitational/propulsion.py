# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 28/09/2021
#	Program  	: DOCKS Propagator
#	Name 		: propulsion.py
#	Authors		: Tim GLASIUS; Rashika JAIN; Harshul SHARMA
#	Version 	: 3.0
######################################################################


import numpy as np

from src import config
from src.frame_switcher import TrajParser, frame_switcher

### DEBUG ### Write two files with information about the propulsion.
if config.Debug_Propulsion == "ON":  # SWITCH in config.py (at the bottom)
    print('Debug_Propulsion on')
    """
    ccsds.write_ccsds(config.output_dir, "CIC_PROP_FILE_DEBUG", "DOCKS Propagator", 
    ["acceleration","ICRF SSB"],None,["%s","%s","%s"],[0,0],[0,0])
    """
    with open("%s/%s" % (config.output_dir, "DEBUG_acc.txt"), 'w') as file_acc:
        file_acc.write("""CIC DEBUG_acc
                
META_START
        
ICRF SSB
Acceleration unit m/s
        
META_STOP\n\n""")

    with open("%s/%s" % (config.output_dir, "DEBUG_tan_frame.txt"), 'w') as file_frame_tan:
        file_frame_tan.write("""CIC DEBUG_frame_tan
                
META_START
        
ICRF SSB
Col 1:2 = time MJD
Col 3:5 = T_vec, V_vec, H_vec
Col 6 = Norm of vector (T , V, H )

        
META_STOP\n\n""")

        # write a document that is tracking the propulsion events
    with open("%s/%s" % (config.output_dir, "Prop_events.txt"), 'w') as file_prop:
        file_prop.write("""CIC_MEM_VERS = 1.0
CREATION_DATE = 2020-05-04T12:55:22.947
ORIGINATOR = DOCKS / CCERES / LabEx ESEP - Paris Observatory - PSL University Paris

META_START

COMMENT = Intervisibility for orbit : ../../vts/TRAJECTORY_01.txt

USER_DEFINED_PROTOCOL = NONE
USER_DEFINED_CONTENT = OEF
USER_DEFINED_SIZE = 1
USER_DEFINED_TYPE = STRING
USER_DEFINED_UNIT = [n/a]
TIME_SYSTEM = UTC

META_STOP\n\n""")


#############


# =============================================================================
# class time_step_manager:
#     """
#     This class is not functional and unused !
#     Class that is outputing the times where a propulsion event is happening.
#     
#     This could be used for the a futur module called the Time-Step-Manager
#     """
# 
#     def __init__(self, t_start, t_end):
# 
#         self.t_start = t_start
#         self.t_end = t_end
# 
#     def give_next_time_step(self, date, time_step):
# 
#         l = len(self.t_end)
#         index = []
#         new_time_step_list = []
# 
#         for ind in range(l):
#             if date <= self.t_start[ind] <= date + time_step:
#                 index.append(ind)
#             else:
#                 continue
#         l2 = len(index)
# 
#         for ind2 in range(l2):
#             new_time_step_list.append(self.t_start[index[ind2]])
#             new_time_step_list.append(self.t_end[index[ind2]])
# 
#         return index, new_time_step_list
# =============================================================================


class ref_tangential:
    """
    Class buiding a tangentiale reference frame
    """

    def __init__(self, body, date, pos, vel, local_frame):

        self.data = None
        self.T_vec = None  # ndarray
        self.N_vec = None  # ndarray
        self.W_vec = None  # ndarray
        self.body_prop = body
        self.date = date
        self.position = pos
        self.velocity = vel
        self.local_frame = local_frame
        if self.local_frame.name!=body or config.ref_frame!="ICRF":
            self.mod_data()
        self.calc_component()

    def mod_data(self):
        """ 
        Modifies position and velocity of the spacecraft to ICRF Body
        """
        #data = ref_tangential._read(config.output_file)
        #self.data = data[-1]  # reads last line of output file
        if self.local_frame.name!='ssb':
            traj_pars = TrajParser()
            sat_info = traj_pars.parse_traj(self.date, self.position, self.velocity, self.local_frame)  # transforms time/position/velocity to MJS, SSB ICRF format
    
            self.position = sat_info[1]  # position In ICRF SSB
            self.velocity = sat_info[2]  # velocity in ICRF SSB
        
        switch = frame_switcher()
        body_data = switch.ssb_to_body(self.body_prop, self.date, self.position, self.velocity)

        self.position = body_data[0]  # position of body in ICRF Body
        self.velocity = body_data[1]  # velocity of body in ICRF Body

# =============================================================================
#     @staticmethod
#     def _read(file_name):
# 
#         file_path = "%s%s%s" % (config.output_dir, '/', file_name)
#         with open(file_path, 'r') as F:
#             datafile = F.readlines()
#         return datafile
# =============================================================================

    @staticmethod
    def _normalize(Vector):

        norm = np.linalg.norm(Vector)
        if norm == 0:
            return Vector
        return Vector / norm

    def calc_component(self):
        """
        Method building a tangential frame from position and velocity of the spacecraft around a body

        Returns
        -------
        The 3 normilized components of the tangential frame
        """

        # R_vec = np.array([0, 0, 0])
        # self.T_vec = np.array([0, 0, 0])
        # self.N_vec = np.array([0, 0, 0])
        # self.W_vec = np.array([0, 0, 0])

        R_vec = self.position
        self.T_vec = self.velocity

        # W_vec  = R_vec cross T_vec
        self.W_vec = np.cross(R_vec, self.T_vec)

        # N_vec  = W_vec cross T_vec
        self.N_vec = np.cross(self.W_vec, self.T_vec)

        # Normilizisation of vectors 
        self.T_vec = ref_tangential._normalize(self.T_vec)
        self.N_vec = ref_tangential._normalize(self.N_vec)
        self.W_vec = ref_tangential._normalize(self.W_vec)

        if config.Debug_Propulsion == "ON":
            file = open("%s/%s" % (config.output_dir, "DEBUG_tan_frame.txt"), 'a')
            time1, time2 = int(self.date), round(self.date - int(self.date), 3)
            file.write("%d\t%.2f\t%f\t%f\t%f\n" % (time1, time2, self.T_vec, self.N_vec, self.W_vec))
            file.close()

    def tan_to_body(self, vect):
        """
        Parameters
        ----------
        vect : vector to be expressed in the ICRF of body

        Returns
        -------
        vect : converted vector
        """
        vec = np.zeros(3)
        vec[0] = vect[0] * self.T_vec[0] + vect[1] * self.N_vec[0] + vect[2] * self.W_vec[0]
        vec[1] = vect[0] * self.T_vec[1] + vect[1] * self.N_vec[1] + vect[2] * self.W_vec[1]
        vec[2] = vect[0] * self.T_vec[2] + vect[1] * self.N_vec[2] + vect[2] * self.W_vec[2]

        return vec


class scenario:
    """
    class using the data from the input file to return the burn corresponding to the given time
    """

    def __init__(self, t_start, t_end, direction, mag, body_prop):

        self.mags = mag
        self.t_starts = t_start
        self.t_ends = t_end
        self.directions = direction
        self.body_prop = body_prop
        self.sign = np.sign(config.time_step)

    def __repr__(self):
        ind = 0
        return str([self.mags[ind], self.t_starts[ind], self.t_ends[ind], self.directions[ind]])

    def __call__(self, ind):
        return [self.mags[ind], self.t_starts[ind], self.t_ends[ind], self.directions[ind]]

    def which_burn(self, date):
        """
        method returning the Burn data corresponding to the given time
        
        input : 
            date
            
        output : 
            If a burn is detected : t start ; t end ; direction in tangential frame ; magnitude of thrust ; prop body
            If no burn is detected return "None"
        """

        l = len(self.directions)

        for ind in range(l):

            if (self.sign*date) > (self.sign*float(self.t_ends[l - 1])):
                return None
            # no more burn is planned in the input file from this time on

            elif (self.sign*date) < (self.sign*float(self.t_starts[0])):
                return None
            # no burn yet

            elif (self.sign*float(self.t_starts[ind])) <= (self.sign*date) <= (self.sign*float(self.t_ends[ind])):
                return [self.t_starts[ind], self.t_ends[ind], self.directions[ind], self.mags[ind], self.body_prop[ind]]
            # returns information about the the current Burn

            elif (self.sign*float(self.t_starts[ind + 1])) > (self.sign*date) > (self.sign*float(self.t_ends[ind])):
                return None
            # no burn is planned at this time, but there is still at least one more burn planned in the future

    def get_acc_prop(self, date, pos, vel, local_frame, flag=1):
        """
        method returning the propulsion acceleration
        
        input : date
        output : propulsion acceleration in tangential frame
        """
        current_burn = self.which_burn(date)  # t start ; t end ; direction in tangential frame ; magnitude of thrust ; prop body or None

        if current_burn is None:
            acc = np.array([0, 0, 0])  # = No propulsion

            if config.Debug_Propulsion == "ON":
                time1, time2 = int(date), round((date - int(date)), 5)
                a_x = acc[0]
                a_y = acc[1]
                a_z = acc[2]
                file = open("%s/%s" % (config.output_dir, "DEBUG_acc.txt"), 'a')
                # (%20.10f%20.10f%20.10f%20.10f pour alligner mais pas au format CIC)
                file.write("%d\t%.2f\t%f\t%f\t%f\n" %
                           (time1, time2, a_x, a_y, a_z))
                file.close()

        elif flag == 2 and date == float(current_burn[0]):
            acc = np.array([0, 0, 0])  # To be considered in next time-step

        elif flag == 0 and date == float(current_burn[1]):
            acc = np.array([0, 0, 0])  # Already considered in previous time-step

        else:
            # Burn following a direction expressed in the ICRF SSB frame (no use of tangential frame)
            if current_burn[4] == 'ICRF_PURE':
                acc = float(current_burn[3]) * (current_burn[2])

                if local_frame.name!='ssb':
                    switch = frame_switcher()
                    _, _, acc = switch.ssb_to_body(local_frame.name, date, accel=acc)

                if config.Debug_Propulsion == "ON":
                    file_event = open("%s/%s" % (config.output_dir, "Prop_events.txt"), 'a')
                    time1, time2 = int(date), round((date - int(date)), 5)
                    file_event.write("%d\t%.2f\t%s\n" % (time1, time2, "prop"))
                    file_event.close()

            else:
                # Burn using tangentiale frame (user friendly)
                a1 = float(current_burn[3]) \
                     * current_burn[2]  # magnitude of thrust (float) * direction of thrust (array) in tangential frame
                ref = ref_tangential(current_burn[4], date, pos, vel, local_frame)
                a2 = ref.tan_to_body(a1)  # acc switched from tangential frame into ICRF prop body frame
                switch = frame_switcher()
                a3 = switch.body_to_ssb(a2, current_burn[4], date)  # acc switch from ICRF prop body to ICRF SSB

                if local_frame.name=='ssb':
                    acc = a3
                else:
                    acc = a2

                if config.Debug_Propulsion == "ON":
                    time1, time2 = int(date), round((date - int(date)), 5)
                    a_x = acc[0]
                    a_y = acc[1]
                    a_z = acc[2]
                    file = open("%s/%s" % (config.output_dir, "DEBUG_acc.txt"), 'a')
                    file.write("%d\t%.2f\t%f\t%f\t%f\t%f\n" % (time1, time2, a_x, a_y, a_z, flag))
                    file.close()
                    # writting in event file
                    file_event = open("%s/%s" % (config.output_dir, "Prop_events.txt"), 'a')
                    time1, time2 = int(date), round((date - int(date)), 5)
                    file_event.write("%d\t%.2f\t%s\n" % (time1, time2, "prop"))
                    file_event.close()

        return acc  # Propulsion acceleration in ICRF SSB or Local Frame m/s²
