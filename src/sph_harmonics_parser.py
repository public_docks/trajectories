#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 19/09/2019
#	Program  	    : DOCKS Propagator
#	Name 		    : sph_harmonics_parser.py
#	Authors		    : Rashika JAIN
#	Version 	    : 1.0
######################################################################

import math

import numpy as np


class SphHarm:

    def sph_harm(self, file_path, degree, order):

        """
        SphHarm

        Purpose:
        
        Reads the file containing the coefficients of spherical harmonics given in standard form
        and returns them in form of matrices C and S

        Input:

        :param file_path: np.string, path of the file containing the coefficients
        :param degree : np.int, the degree upto which the effect of spherical harmonics should be considered
        :param order : np.int, the order upto which the effect of spherical harmonics should be considered
        
        Output:

        :param C_norm : Matrix degree*degree, Unnormalized spherical harmonics coefficients Cnm
        :param S_norm : Matrix degree*degree, Unnormalized spherical harmonics coefficients Snm
        :param R : np.double, Reference radius in meters
        :param mu : np.double, Gravitational coefficient in m^3/s^2

        """

        """
        Local variables
        
        np.array N                                 # Normalization matrix

        """

        deg_ord_req = int(degree * (degree + 1) / 2) + order + 1

        f = open(file_path)
        text = f.readlines()
        text = np.array(text)
        for i in range(text.size):
            text[i] = text[i].strip()

        text_filter = []
        for i in range(text.size):
            if text[i] != '':
                text_filter.append(text[i])
        text_filter = np.array(text_filter)

        datafloat = []
        for i in text_filter:
            datafloat.append(np.float64(i.split(",")))
        datafloat = np.array(datafloat, dtype=object)

        body_prop = [datafloat[0][0], datafloat[0][1],
                     datafloat[0][5]]  # R = km, mu = km^3/s^2, normalization-indicator

        CS = datafloat[1:deg_ord_req][:]

        C = np.zeros((degree + 1, degree + 1))
        S = np.zeros((degree + 1, degree + 1))
        C[0][0] = 1
        S[0][0] = 0
        r_end = 0
        for n in range(1, degree):
            for m in range(n + 1):
                C[n][m] = CS[r_end + m][2]
                S[n][m] = CS[r_end + m][3]
            r_end = int(n * (n + 3) / 2)

        for m in range(order + 1):
            C[degree][m] = CS[r_end + m][2]
            S[degree][m] = CS[r_end + m][3]

        R = body_prop[0] * 1000  # m
        mu = body_prop[1] * 1E+9  # m^3/s^2
        norm_indicator = int(body_prop[2])

        if norm_indicator == 1:
            N = np.zeros((degree + 1, degree + 1))
            for n in range(degree + 1):
                N[n][0] = 1 / np.sqrt((2 * n) + 1)
                for m in range(1, n + 1):
                    N[n][m] = 1 / np.sqrt((2 * n + 1) * math.factorial(n - m) * 2 / math.factorial(n + m))
        elif norm_indicator == 0:
            N = np.ones((degree + 1, degree + 1))

        C_norm = np.zeros((degree + 1, degree + 1))
        S_norm = np.zeros((degree + 1, degree + 1))
        for n in range(degree + 1):
            for m in range(n + 1):
                C_norm[n][m] = C[n][m] / N[n][m]
                S_norm[n][m] = S[n][m] / N[n][m]

        return C_norm, S_norm, R, mu
