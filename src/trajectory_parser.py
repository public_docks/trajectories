#!usr/bin/env python
# -*- coding: utf-8 -*-
########################################################################################
#	Modified date	: 26/04/2024
#	Program  	: DOCKS Propagator
#	Name 		: trajectory_parser.py
#	Authors		: Method first wrote by Jim ; Nima TRAORE ; Florian JOUSSEAUME; Laëtitia LEBEC; Rashika JAIN; Harshul SHARMA; Tomas NUNES
#	Version 	: 7.1
########################################################################################

import numpy as np
from astropy.coordinates import SkyCoord

from src.config import input_position_fmt, input_velocity_fmt
from src.config import ref_frame_input, init_date, inp_center_name, initial_pv
from src.tools import motion


class TrajParser:
    """Class giving the location of the spacecraft at a certain epoch.

    This class reads the contents of a host trajectory file and returns 
    data from the last line. The file should have as format a list of 
    data without containing any string. The class TrajParser uses following
    classes: Date, Position and Velocity.
    Recovered data are:
    -d: represents an epoch (a fixed point in time)
    -p: the position vector of the spacecraft
    -v: the velocity vector of the spacecraft.
    """

    def parse_traj_initial(self,computation_center):
        """Method reading the contents of a specified trajectory file and
        returning the data from last line of file as a tuple of an epoch,
        an array of position vector and an array of velocity vector.
        Returns date (YY/MM/DD HH/MM/SS), position (m), velocity (m/s).
        The returned position is computation_center centered and in the ICRF reference frame.
        """
        date = init_date.mjd * 86400
        p = motion.Position()
        v = motion.Velocity()

        init_pv = [0.] * 6

        # position in m
        if input_position_fmt == "M":
            init_pv[0:3] = np.array(initial_pv[0:3])
        if input_position_fmt == "KM":
            init_pv[0:3] = np.array(initial_pv[0:3]) * 1000
        if input_position_fmt == "AU":
            init_pv[0:3] = np.array(initial_pv[0:3]) * 1000 * 149597870.700

        # velocity in m/s
        if input_velocity_fmt == "M/S":
            init_pv[3:6] = np.array(initial_pv[3:6])
        if input_velocity_fmt == "KM/S":
            init_pv[3:6] = np.array(initial_pv[3:6]) * 1000
        if input_velocity_fmt == "AU/D":
            init_pv[3:6] = np.array(initial_pv[3:6]) * 1000 * 149597870.700 / 86400

        # change of frame
        if ref_frame_input == "ICRF":
            c_ic = SkyCoord(init_pv[0], init_pv[1], init_pv[2], frame='icrs', unit='m',
                            representation_type='cartesian')
            cv_ic = SkyCoord(init_pv[3], init_pv[4], init_pv[5], frame='icrs', unit='m',
                             representation_type='cartesian')

        if ref_frame_input == "ECLIPTICJ2000":
            c = SkyCoord(init_pv[0], init_pv[1], init_pv[2], frame='barycentricmeanecliptic',
                         unit='m', representation_type='cartesian')
            c_ic = c.transform_to('icrs')
            c_ic.representation_type = 'cartesian'
            # Velocity
            # UNIT TO CHANGE : SOLUTION TO FIND...
            cv = SkyCoord(init_pv[3], init_pv[4], init_pv[5], frame='barycentricmeanecliptic',
                          unit='m', representation_type='cartesian')
            cv_ic = cv.transform_to('icrs')
            cv_ic.representation_type = 'cartesian'
            
# =============================================================================
#         from src.instances import central_bodi_input
#         if inp_center_name.lower() != 'ssb':
#             body_self = central_bodi_input
#             pos, vel, _ = np.array(body_self.eph(date))  # recover pos (m) & vel (m/s)
#         else:
#             pos = np.array([0, 0, 0])
#             vel = np.array([0, 0, 0])
# =============================================================================
        from src.instances import central_bodi_input, Bodies
        if inp_center_name.lower() != computation_center[0]:
            body_self = central_bodi_input
            # for bodi in Bodies:
            #     if bodi.name == computation_center:
            #         body_center = bodi
            body_center = computation_center[1]
            pos, vel, _ = np.array(body_self.eph(date)) - np.array(body_center.eph(date)) # recover pos (m) & vel (m/s)
        else:
            pos = np.array([0,0,0])
            vel = np.array([0,0,0])

        p.x = c_ic.x.value + pos[0]
        p.y = c_ic.y.value + pos[1]
        p.z = c_ic.z.value + pos[2]
        v.vx = cv_ic.x.value + vel[0]
        v.vy = cv_ic.y.value + vel[1]
        v.vz = cv_ic.z.value + vel[2]

        return date, p.xyz_array(), v.vxvyvz_array()  # date (in mjd, in seconds), pos(m), v(m/s)
    
# =============================================================================
#     def step_parse_traj(self,date_s,pv,local_prev,computation_center):
#         """Method reading the contents of a specified trajectory file and
#         returning the data from last line of file as a tuple of an epoch,
#         an array of position vector and an array of velocity vector.
#         Returns date (YY/MM/DD HH/MM/SS), position (m), velocity (m/s).
#         The returned position is SSB centered and in the ICRF reference frame.
#         """
#         date = date_s
#         p = motion.Position()
#         v = motion.Velocity()
# 
#         init_pv = pv
# 
#         from src.instances import Bodies
#         if local_prev[0] != computation_center:
#             body_self = local_prev[1]
#             for bodi in Bodies:
#                 if bodi.name == computation_center:
#                     body_center = bodi
#             pos, vel, _ = np.array(body_self.eph(date)) - np.array(body_center.eph(date)) # recover pos (m) & vel (m/s)
#         else:
#             pos = np.array([0,0,0])
#             vel = np.array([0,0,0])
#         
#         p.x = init_pv[0] + pos[0]
#         p.y = init_pv[1] + pos[1]
#         p.z = init_pv[2] + pos[2]
#         v.vx = init_pv[3] + vel[0]
#         v.vy = init_pv[4] + vel[1]
#         v.vz = init_pv[5] + vel[2]
# 
#         return p.xyz_array(), v.vxvyvz_array()  # date (in mjd, in seconds), pos(m), v(m/s)
# =============================================================================