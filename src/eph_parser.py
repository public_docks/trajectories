#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 01/09/2019
#	Program  	: DOCKS Propagator
#	Name 		: eph_parser.py
#	Authors		: Florian JOUSSEAUME; Laëtitia LEBEC; Rashika JAIN
#	Version 	: 4.0
######################################################################

import numpy as np
import re
from astropy.time import Time
from scipy.interpolate import interp1d

# import ccsds as ccsds
from src.config import eph_information


class EphParser:
    """Class that load the datas of the unknown bodies to use them in the propagator
    
    Intput: Ephemeris files for perturbatives celestial bodies
        
    Returns:
    - the interpolation functions for position (in m), velocity (in m/s) and acceleration (in m/s²)
    - the current date, in mjd seconds
    """

    def eph(self, traj_file):

        with open(traj_file, 'r') as f:
            data = f.readlines()

        # detecting 'META_STOP' to read required data
        for i, d in enumerate(data):
            if 'META_STOP' in d:
                meta_stop = i
                break
        data_req = data[meta_stop + 2:]

        data = []
        for line in data_req:
            data.append(re.split(r'[\s,;]+', line.rstrip()))
        data = np.array(data, dtype=object)
        sci_start = 1  # column from which science data is starting

        eph_time_fmt = eph_information['time']
        eph_pos_fmt = eph_information['pos']
        eph_vel_fmt = eph_information['vel']

        # Declaration of the date
        if eph_time_fmt == "ISOT":
            tt = data[:,0].astype(str)
            current_time = Time(tt, format='isot', scale='tdb')
            current_time = current_time.mjd * 86400.

        elif eph_time_fmt == "JD":
            tt = np.float64(data[:,0])
            current_time = Time(tt, format='jd', scale='tdb')
            current_time = current_time.mjd * 86400.

        elif eph_time_fmt == "MJD_1COL":
            tt = np.float64(data[:, 0])
            current_time = Time(tt, format='mjd', scale='tdb')
            current_time = current_time.mjd * 86400.

        elif eph_time_fmt == "MJD_2COL":  # Default
            tt = np.float64(data[:, 0:2])
            eph_time = tt[:,0] + tt[:,1] / 86400.
            current_time = Time(eph_time, format='mjd', scale='tdb')
            current_time = current_time.mjd * 86400.
            sci_start = 2

        # Declaration of the position and velocity
        data = np.float64(data[:, sci_start:])
        if eph_pos_fmt == "AU":
            r = data[:, 0:3] * 149597870.700
        elif eph_pos_fmt == "KM":  # Default
            r = data[:, 0:3]
        elif eph_pos_fmt == "M":
            r = data[:, 0:3] / 1000

        if eph_vel_fmt == "AU/D":
            v = data[:, 3:6] * 149597870.700 / 86400
        elif eph_vel_fmt == "KM/S":
            v = data[:, 3:6]
        elif eph_vel_fmt == "M/S":
            v = data[:, 3:6] / 1000

        h = current_time[1:] - current_time[0:-1]  # Ephemeris' time step
        a = np.divide(v[1:, :] - v[0:-1, :], h[:,None])
        
        #import pdb
        #pdb.set_trace()

        # Definition of the interpolation functions
        fr = interp1d(current_time, r * 1000, kind='cubic', fill_value="extrapolate", axis=0)  # m
        fv = interp1d(current_time, v * 1000, kind='cubic', fill_value="extrapolate", axis=0)  # m/s
        fa = interp1d(current_time[0:-1], a * 1000, kind='cubic', fill_value="extrapolate", axis=0)  # m/s²

        return fr, fv, current_time, r, fa
