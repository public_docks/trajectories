# -*- coding: utf-8 -*-
###########################################################################
#	Modified date	: 26/04/2024
#	Program  	: DOCKS Propagator
#	Name 		: config.py
#	Authors		: Nima TRAORE ; Florian JOUSSEAUME; Laëtitia LEBEC; Rashika JAIN; Harshul SHARMA; Tomas NUNES
#	Version 	: 7.1
###########################################################################

# Propagator configuration file for propagating trajectories.
# This file is devoted to be the only interface where user can declare or
# create variables and to choose input and/or ouptut directories of files.

import os
import re
import sys
import time as tm
from pathlib import Path
import argparse

import numpy as np
import ruamel.yaml as yaml
from astropy.time import Time

from src.predefined_bodies import known_bodies

p = str(Path(__file__).resolve().parent.parent.parent / "docks_tools/")
if p not in sys.path:
    sys.path.append(p)
import spice_body_acess as sba

""" Meaning of different formats used in this file are:
DATE:         "MJD_2col" is Modified Julian Day 2-col format (days, sec)
# #           "MJD_1col" is Modified Julian Day 1-col format (days)
# #	          "JD" is Julian Day format
# #           "ISOT is ISOT format (YYYY-MM-DDTHH:mm:ss)
POSITION:     "KM" Kilometers ; "M" Meters  ; "AU" Astronomical unit
VELOCITY:     "KM/S" km/s  ; "M/S" m/s  ; "AU/D" au/d 
ACCELERATION: "M/S²" (m/s²), "KM/S²" (km/s²), "AU/D²" (au/d²)
"""

# Parse command-line arguments
parser = argparse.ArgumentParser(description='Run Propagator')
parser.add_argument('script', help='Name of Script .yaml file')
parser.add_argument('--performance', action='store_true', default=False, help='Print Current Propagator Performance if Enabled')
parser.add_argument('--disable_tqdm', action='store_true', default=False, help='Disable tqdm progress bar')
args = parser.parse_args(sys.argv[1:])
sys.argv[1:] = [args.script]
sys.argv.append(args.performance)

if args.disable_tqdm:
    sys.argv.append('disable_tqdm')

# Import config.txt #############################################################
file_path = args.script

config_loc = os.path.abspath(os.path.dirname(file_path))
with open(file_path) as file:
    data = yaml.round_trip_load(file, preserve_quotes=True)

# Different data sets
data_ic = dict(data["initialConditions"])
data_eph = dict(data["ephemInput"])  # ephemerides source
data_per = dict(data["perturbations"])  # space environment
data_newbodies = dict(data["new_grav_bodies"])
data_cgmbodies = dict(data["complex_grav_bodies"])
data_probe = dict(data["probeFeatures"])  # probe features
data_num = dict(data["numericalMethod"])  # numerical integration method
data_time = dict(data["timeSettings"])  # end time, time_step
data_out = dict(data["output"])  # parameters for output trajectory file

# Inputs parameters #########################################################

# initial conditions file
init_file = os.path.normpath(os.path.join(config_loc, data_ic["file_path"]))

# Initial condition values format
input_time_fmt = data_ic["format"][0].upper()
input_position_fmt = data_ic["format"][1].upper()
input_velocity_fmt = data_ic["format"][2].upper()

# Input center of system for the initial conditions
# (Needed to include the ephemerides of the center body)
input_center_form = data_ic["center"][0].lower()
inp_center_name = data_ic["center"][1][0].lower()
inp_center = {}
if input_center_form == 'predefined':
    inp_center = {inp_center_name: known_bodies[inp_center_name]}
elif input_center_form == 'custom':
    inp_center_mu = np.float64(data_ic["center"][1][1])
    inp_center_eph = data_ic["center"][1][2]
    inp_center_naif = data_ic["center"][1][3]
    if inp_center_eph.lower() != 'none':
        inp_center_eph = os.path.normpath(os.path.join(config_loc, inp_center_eph))
        inp_center_naif = 'none'
    else:
        inp_center_eph = 'none'
        inp_center_naif = int(inp_center_naif)
    inp_center = {inp_center_name: [inp_center_mu, inp_center_eph, inp_center_naif, 0.]}

# Input reference frame for the trajectory - ICRF or ECLIPTICJ2000
ref_frame_input = data_ic["frame"].upper()

# extract data from initial conditions file
with open(init_file) as f:
    lines = [line.rstrip('\n') for line in f]
    f.close()
line = lines[-1]
initial_conditions = re.split(r'[\s,;]+', line)
initial_pv = np.array(initial_conditions[1:], dtype=np.float64)

# extract init_date from initial conditions as astropy Time variable
if input_time_fmt == "ISOT":
    init_date = Time(initial_conditions[0], format='isot', scale='tdb')
elif input_time_fmt == "JD":
    init_date = Time(float(initial_conditions[0]), format='jd', scale='tdb')
elif input_time_fmt == "MJD_1COL":
    init_date = Time(float(initial_conditions[0]), format='mjd', scale='tdb')
elif input_time_fmt == "MJD_2COL":
    value = int(initial_conditions[0]) + np.float64(initial_conditions[1]) / 86400
    init_date = Time(value, format='mjd', scale='tdb')
    initial_pv = initial_pv[1:]
init_date_jd = init_date.jd

# Perturbations ############################################################

# Configuration of the unknown bodies and their ephemerides formats
eph_time_fmt = None
eph_pos_fmt = None
eph_vel_fmt = None
if data_eph["spice_kernels"]:
    spice_kernel_loc = os.path.normpath(os.path.join(config_loc, data_eph["spice_kernels"]))
    # Instance of loading ephemerides
    eph_path = os.path.relpath(spice_kernel_loc)
    sba.load_spice_kernel(eph_path)
if data_eph["text_files"]:
    eph_time_fmt = data_eph["text_files"][0].upper()
    eph_pos_fmt = data_eph["text_files"][1].upper()
    eph_vel_fmt = data_eph["text_files"][2].upper()

if not data_eph["spice_kernels"] and not data_eph["text_files"]:
    raise RuntimeError("Either spice kernels or text files or both should be used for ephemerides")

eph_information = {'time': eph_time_fmt, 'pos': eph_pos_fmt, 'vel': eph_vel_fmt}

# Central Body of Propagation
try:
    if data_per['central_body']:
        central_body_name = data_per['central_body'].lower()
    else:
        # Default Value [Maybe Change for Sun]
        central_body_name = 'ssb'
except KeyError:
    # Try/Except for Older Config Versions where Central Body wasn't implemented to still work [Eventually remove this]
    central_body_name = 'ssb'

# List of gravitational perturbations to be taken into account by the propagator
predef_bodies = {}
list_predef_models = []
if data_per["predefined_bodies"]:
    list_predef_models = [x.lower() for x in data_per["predefined_bodies"]]
    for x in list_predef_models:
        predef_bodies[x] = known_bodies[x]

# List of foreground celestial bodies to be taken into account by system
# to get ephemerides with respect to Spacecraft position
list_foreground_objects = []
# Ephemerides attributes
# Configuration for the ephemeride outputs
# d_opt : Optical precision (in degree °)
# step_size : Step between two points (in days)
foreground_objects_data = {"earth": {"d_opt": 0.001, "traj_file": "Earth.txt"},
                           "moon": {"d_opt": 0.28, "traj_file": "Moon.txt"},
                           "didymos": {"d_opt": 0.003, "traj_file": "Didymos.txt"},
                           "didymoon": {"d_opt": 0.003, "traj_file": "Didymoon.txt"},
                           "mothercraft": {"d_opt": 0.003, "traj_file": "Mothercraft.txt"},
                           "mars": {"d_opt": 0.003, "traj_file": "Mars.txt"}
                           }

# Recovering datas for additional perturbative bodies coming from the interface
# list_new_models = []
# newbodies_dic = {}
# for body in data_per["new_grav_perturbations"]:
#     list_new_models.append(body[0])
#     if body[2].lower() != 'none':
#         body_eph = os.path.normpath(os.path.join(config_loc, body[2]))
#         body_naif = body[3].lower()
#     else:
#         body_eph = body[2].lower()
#         body_naif = int(body[3])
#     newbodies_dic[body[0]] = [np.float64(body[1]), body_eph, body_naif]
# list_models = list_predef_models + list_new_models


list_new_models = []
newbodies_dic = {}
if data_per['new_bodies_added']:
    for _, body in data_newbodies.items():
        list_new_models.append(body['name'].lower())
        if body['ephFile']:
            body_eph = os.path.normpath(os.path.join(config_loc, body['ephFile']))
            body_naif = 'none'
        else:
            body_eph = 'none'
            body_naif = int(body['naifId'])
        if 'radius' in list(body.keys()):
            if body['radius']:
                R = np.float64(body['radius'])
            else:
                R = 0. 
        else:
            R = 0.
        newbodies_dic[body['name'].lower()] = [np.float64(body['mu']), body_eph, body_naif, R]
list_models = list_predef_models + list_new_models

# Merging predefined bodies with user's bodies
unknown_bodies = {**predef_bodies, **newbodies_dic}

# bodies present in spice kernels
# if data_eph["spice_kernels"]:
#     loaded_kernels = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 199, 299, 399, 301]
#     reqd_kernels = [e[2] for e in unknown_bodies.values()]
#     if not all(item in loaded_kernels for item in reqd_kernels):
#         warnings.warn("The required bodies are not included in the "
#                              "low resolution kernels. Make sure full kernels are loaded")

if inp_center_name not in unknown_bodies:
    unknown_bodies = {**unknown_bodies, **inp_center}

# List of non-gravitational perturbations undergone by the spacecraft
# srp = solar radiation pressure
# prop = continuous propulsion
# In the future, following models could be added in list_perturbations:
# "magnetic_field", "aerodynamic_drag", "plasma_radiation", ...

list_perturbations = []
if data_per["non_grav_perturbations"]: # Recovering datas from non-gravitational perturbations string
    list_perturbations = [x.lower() for x in data_per["non_grav_perturbations"]]

# burns sequence file for continuous propulsion
if "propulsion" in list_perturbations:
    if data_per["propulsion_burns_file"]:
        prop_file = os.path.normpath(os.path.join(config_loc, data_per["propulsion_burns_file"]))
    else:
        raise RuntimeError("A file with scheduled trajectory burns should be given for continuous propulsion")

# Complex gravitational models
# The User can add a complex gravitational model for a desired celestial body.
# In that case the required files - Ephemerides and Quaternion of the body - are recovered
# The scientist also has to add his own gravitational model computation code into the
# complex_model.py script
cgm_mode = data_per["complex_grav_model_activated"]

# Recovering datas for complex perturbative bodies coming from the interface
complex_gravitational_bodies = {}
list_sph_harm = []
if cgm_mode:
    for _, cgm_body in data_cgmbodies.items():
        if cgm_body['ephFile']:
            eph_cgm = os.path.normpath(os.path.join(config_loc, cgm_body['ephFile']))
            eph_method = 'file'
        else:
            eph_cgm = int(cgm_body['naifId'])
            eph_method = 'naifid'
        if cgm_body['rotMatrixFile']:
            rot_cgm = os.path.normpath(os.path.join(config_loc, cgm_body['rotMatrixFile']))
            rot_method = 'file'
        elif cgm_body['rotMatrixSpice']:
            rot_cgm = cgm_body['rotMatrixSpice']
            rot_method = 'spice'
        else:
            rot_cgm = 'none'
            rot_method = 'none'
        if cgm_body['quatFile']:
            quat_file = os.path.normpath(os.path.join(config_loc, cgm_body['quatFile']))
        else:
            quat_file = 'none'
        if cgm_body['sphCoeffFile']:
            sh_coeff = os.path.normpath(os.path.join(config_loc, cgm_body['sphCoeffFile']))
            degree = int(cgm_body['sphHarmDegree'])
            list_sph_harm.append(cgm_body['name'].lower())
        else:
            sh_coeff = 'none'
            degree = 0

        complex_gravitational_bodies[cgm_body['name'].lower()] = [eph_method, eph_cgm, rot_method, rot_cgm, quat_file, sh_coeff, degree]

# Spherical Harmonics ################################################
# Spherical harmonics attributes for each planet
# order : Order of the spherical harmonics (0,1,2...53...)
# radius : Equatorial radius of the body
# harmonics_attributes = {	"earth": {	"order":0,
#                               		"radius":6378137}}
# harmonics_attributes = {"earth": {"degree": 2},
#                         "mercury": {"degree": 0},
#                         "venus": {"degree": 0},
#                         "mars": {"degree": 0},
#                         "moon": {"degree": 0}
#                         }

# Propagator ########################################################
# Propagator type, choose between
# - classical Runge-Kutta4
# - Runge-Kutta-fehlberg with time step sampling method (eg: propagator_type = "rkf45_module")
# - "rk4_module" = classical Runge-Kutta4
# - "rkf45_module" = Runge-Kutta-fehlberg with variable time step
propagator_type = data_num["method"].lower()

# Propagator attributes:
if data_time["time_step_unit"].lower() == "hours":
    time_step = data_time["time_step"][0] * 3600
    time_step_min = data_time["time_step"][1] * 3600
    time_step_max = data_time["time_step"][2] * 3600

elif data_time["time_step_unit"].lower() == "minutes":
    time_step = data_time["time_step"][0] * 60
    time_step_min = data_time["time_step"][1] * 60
    time_step_max = data_time["time_step"][2] * 60

elif data_time["time_step_unit"].lower() == "seconds":
    time_step = data_time["time_step"][0]
    time_step_min = data_time["time_step"][1]
    time_step_max = data_time["time_step"][2]

if data_time["method"].lower() == "duration":
    nb_days = data_time["propagation_time"][0]
    nb_hour = int(data_time["propagation_time"][1].split(':')[0])
    nb_min = int(data_time["propagation_time"][1].split(':')[1])
    nb_sec = float(data_time["propagation_time"][1].split(':')[2])
    step_num = round(abs((nb_days * 86400 + nb_hour * 3600 + nb_min * 60 + nb_sec) / time_step))
    end_date = init_date.mjd * 86400 + np.sign(time_step) * (
                nb_days * 86400 + nb_hour * 3600 + nb_min * 60 + nb_sec)  # mjd
    # seconds
elif data_time["method"].lower() == "end_time":
    step_num = round(abs((data_time["propagation_time"][0] - init_date_jd) * 86400 / time_step))
    end_date = (data_time["propagation_time"][0] - 2400000.5) * 86400
else:
    step_num = data_time["propagation_time"][0]
    end_date = (init_date.mjd * 86400) + (step_num * time_step)

propagator_attributes = {"step_number": int(step_num),
                         "time_step": time_step,
                         "time_step_min": time_step_min,  # RKF45, RKF56, RKF78, IAS15
                         "time_step_max": time_step_max,  # RKF45, RKF56, RKF78
                         "tolerance": data_num["tolerance"],  # RKF45, RKF56, RKF78, IAS15
                         "safety_factor": data_num["safety_factor"],  # RKF45, RKF56, RKF78
                         "end_date": end_date}

# Spacecraft features ###############################################
# mass (in kg), area exposed to sun (in m²) and 0 <= radiation_pressure_coefficient <= 2
# Refl = 0 : Transparent ; Refl = 1 : Black Body (absorbs all) ; Refl = 2 : white body (reflects all)
# Default spacecraft features
spacecraft = {"mass": 10,
              "srp_area": 0.5,
              "srp_coefficient": 1.2}

for k, v in data_probe.items():
    if v:
        spacecraft[k] = float(v)

# Solar Radiation Pressure parameters ###############################
# Solar luminosity (in Watts)
SF_SRP = 1367. # W/m^2
Dist_SF_SRP = 149597870691 # m (1AU)
L = SF_SRP * Dist_SF_SRP**2 * 4 * np.pi # W
solar = {"luminosity": L}

# Output parameters #################################################
# Center output of system
# sun; earth
output_center_form = data_out["center"][0].lower()
out_center_name = data_out["center"][1][0].lower()
out_center = {}
if output_center_form == 'predefined':
    out_center = {out_center_name: known_bodies[out_center_name]}
elif output_center_form == 'custom':
    out_center_mu = np.float64(data_out["center"][1][1])
    out_center_eph = data_out["center"][1][2]
    out_center_naif = data_out["center"][1][3]
    if out_center_eph.lower() != 'none':
        out_center_eph = os.path.normpath(os.path.join(config_loc, out_center_eph))
        out_center_naif = 'none'
    else:
        out_center_naif = int(out_center_naif)
        out_center_eph = 'none'
    out_center = {out_center_name: [out_center_mu, out_center_eph, out_center_naif, 0.]}

# Merging output center body with already defined bodies
if out_center_name not in unknown_bodies:
    unknown_bodies = {**unknown_bodies, **out_center}

# Output reference frame
# ICRF ; ECLIPTICJ2000
ref_frame = data_out["frame"].upper()

# Data format for the reference trajectory output files
out_time_fmt = data_out["format"][0].upper()
out_position_fmt = data_out["format"][1].upper()
out_velocity_fmt = data_out["format"][2].upper()
out_acceleration_fmt = data_out["format"][3].upper()

# Output step divider
# this value allows to change the how frequently the output is written during propagation
# Example: if output_divider = 5, after 5 time steps these will be written and so on
output_divider = int(data_out["step_divider"])
# output directory
output_dir = os.path.normpath(os.path.join(config_loc, data_out["directory"]))
# Create Folder if it doesnt exists
if not os.path.isdir(output_dir):
    os.makedirs(output_dir)
# output file name
output_file = data_out["file_name"]

# Output files Headers ##############################################
# VTS format header for the reference trajectory output files
traj_vts_headers = [
    "CIC_OEM_VERS = 2.0\n",
    "CREATION_DATE 	= ", tm.strftime("%Y-%m-%dT%H:%M:%S\n"),
    "ORIGINATOR   	= DOCKS\n",
    "\n",
    "META_START\n",
    "\n",
    "COMMENT        DOCKS Propagator version 6.5\n",
    "COMMENT        Propagator Center of Computation : " + "%s" % central_body_name.upper() + "\n",
    "COMMENT        Propagator method : " + "%s" % propagator_type.upper() + "\n",
    "COMMENT        Column 1:2 Unit : Time (MJD)" + "\n",
    "COMMENT        Column 3:5 Unit : Position (" + "%s" % out_position_fmt + ")" + "\n",
    "COMMENT        Column 6:8 Unit : Velocity (" + "%s" % out_velocity_fmt + ")" + "\n",
    "COMMENT        Column 9:11 Unit : Acceleration (" + "%s" % out_acceleration_fmt + ")" + "\n",
    "COMMENT        Acceleration is given in the Reference Frame Centered in the Propagator Center of Computation (" + "%s" % central_body_name.upper() + ")" + "\n",
    "\n",
    "OBJECT_NAME  = DOCKS_Sat\n",
    "OBJECT_ID    = CORPS\n",
    "\n",
    "CENTER_NAME  = " + "%s" % out_center_name.upper() + "\n",
    "REF_FRAME    = " + "%s" % ref_frame + "\n",
    "TIME_SYSTEM  = TDB\n",
    "\n",
    "META_STOP\n",
    "\n"]

# VTS Position format header for the reference ephemerides output files
# Not used in Beta Release version
eph_vts_headers = [
    "CIC_OEM_VERS = 2.0\n",
    "CREATION_DATE 	= ", tm.strftime("%Y-%m-%dT%H:%M:%S\n"),
    "ORIGINATOR   	= DOCKS\n",
    "\n"]
eph_vts_headers_next = [
    "\n",
    "META_START\n",
    "\n",
    "COMMENT        Unit : Time (" + "%s" % out_position_fmt + ")" + "\n",
    "COMMENT        Unit : Position (" + "%s" % out_time_fmt + ")" + "\n",
    "\n",
    "CENTER_NAME  = SUN\n",
    "REF_FRAME    = ECLIPTICJ2000\n",
    "TIME_SYSTEM  = UTC\n",
    "\n",
    "META_STOP\n",
    "\n"]

# Change computation frame to local frame
# if len(list_models) == 1:
#     change_frame = True
# else:
#     change_frame = False  # if False frame is ssb centered

# Debug Modes
Debug_Switch = "OFF"

if Debug_Switch == 'ON':
    print("You are in DEBUG Mode")
    # --- to be edited by the user ----
    Debug_Switch_cp1 = "ON"
    Debug_folder = '../Propagator_validation'
    Debug_Propulsion = "OFF"
    # ---------------------------------
else:
    Debug_Switch_cp1 = "OFF"
    Debug_Propulsion = "OFF"
