#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 26/04/2024
#	Program  	: DOCKS Propagator
#	Name 		: ref_traj.py
#	Authors		: Nima TRAORE ; Florian JOUSSEAUME; Laëtitia LEBEC; Rashika JAIN; Tomas NUNES
#	Version 	: 6.1
######################################################################

from copy import copy

import numpy as np
from astropy.coordinates import SkyCoord
from astropy.time import Time

from src.config import out_center_name, ref_frame
from src.tools import motion


class TrajOutput:
    """Class exporting the spacecraft trajectory to a file in CIC format, compatible with VTS software
    """
    from src.config import out_position_fmt, out_velocity_fmt, out_time_fmt, out_acceleration_fmt

    def __init__(self, output_dir, output_file, traj_vts_headers=None):
        """Constructor of the class TrajOutput."""
        self.output_dir = output_dir
        self.output_file = output_file
        if traj_vts_headers is None:
            traj_vts_headers = []
        self.vts_headers = traj_vts_headers

    def tab_write(self, *args):
        """Method writting to file."""
        for val in args:
            self.file.write('%s\t' % val)

    def export_traj_all(self, trajectory, local_frame, disable_tqdm):
        """Exporting the trajectory of the spacecraft to specified file."""
        trajectory = np.array(trajectory)
        dt = motion.Date()
        # Precision of the data
        prec_pos = 15  # mm
        if self.out_position_fmt == "KM":
            pfmt = 1 / 1000.
        elif self.out_position_fmt == "M":
            pfmt = 1
        elif self.out_position_fmt == "AU":
            pfmt = 1 / 1000. / 149597870.700

        # scientific precision for velocity and acceleration
        prec_vel = 15  # mm/s
        if self.out_velocity_fmt == "KM/S":
            vfmt = 1 / 1000.
        elif self.out_velocity_fmt == "M/S":
            vfmt = 1
        elif self.out_velocity_fmt == "AU/D":
            vfmt = 1 * 86400. / 1000. / 149597870.700

        prec_acc = 15  # mm/s²
        if self.out_acceleration_fmt == "KM/S^2":
            afmt = 1 / 1000.
        elif self.out_acceleration_fmt == "M/S^2":
            afmt = 1
        elif self.out_acceleration_fmt == "AU/D^2":
            afmt = 1 * (86400. ** 2) / 1000. / 149597870.700

        from src.instances import central_bodi_output
        from tqdm import tqdm
        
        with open(self.output_dir + "/" + self.output_file, 'a') as self.file:
            time_list = []
            pos_eph_list = []
            vel_eph_list = []
            #acc_eph_list = []

            bar = tqdm(np.arange(0,len(trajectory),1), ascii=True, desc="Conversions ",
                       bar_format='{l_bar}{bar:5}|[{elapsed}<{remaining}, {rate_fmt}]{bar:-5b}', disable=disable_tqdm)
            for i in bar:

                # from Modified Julian seconds to required format
                if self.out_time_fmt == 'MJD_2COL':
                    mjd_integer_part, mjd_decimal_part = dt.mjd_vts(trajectory[i,0] / 86400)  # MJD 2 column
                    time = '{:d}'.format(mjd_integer_part) + '\t' + '{:10.{prec}f}'.format(mjd_decimal_part, prec=4)
                elif self.out_time_fmt == 'MJD_1COL':
                    time = '{:.{prec}f}'.format(trajectory[i,0] / 86400, prec=4)
                elif self.out_time_fmt == 'JD':
                    jd = (trajectory[i,0] / 86400) + 2400000.5
                    time = '{:.{prec}f}'.format(jd, prec=4)
                elif self.out_time_fmt == 'ISOT':
                    time = Time(trajectory[i,0] / 86400, format='mjd', scale='tdb')
                    time = time.isot
                time_list.append(time)
                
                if out_center_name.lower() != local_frame[0]:
                    self.body_self = central_bodi_output
                    # Conversion of the data in the chosen center.
                    pos_eph, vel_eph, acc_eph = np.array(self.body_self.eph(trajectory[i][0])) - np.array(local_frame[1].eph(trajectory[i][0]))
                else:
                    pos_eph = np.array([0, 0, 0])
                    vel_eph = np.array([0, 0, 0])
                    #acc_eph = np.array([0, 0, 0])
                pos_eph_list.append(pos_eph)
                vel_eph_list.append(vel_eph)
                #acc_eph_list.append(acc_eph)


            # Convert Frame
            trajectory[:,1:4] -= np.array(pos_eph_list)
            trajectory[:,4:7] -= np.array(vel_eph_list)
            # NOTE: Acceleration is only completely correct when output body = central body, because if not, acc_local_frame should cancel the acceleration
            # of the central body taken into account during the tensor_constructor. Howver, acc_local_frame is the acceleration from ephmerides (takes into
            # account the whole Universe), while the central body acceleration computed in tensor_constructor only takes into account bodies considered in this
            # system, so they are not completely the same (as they should to be able to do this conversion). Furthermore, the acceleration of the output body is
            # also taken from the ephemerides. So the output acceleration will be given in the reference frame centered in the central body.
            # trajectory[:,7:10] -= np.array(acc_eph_list)
            trajectory[:,1:4] *= pfmt
            trajectory[:,4:7] *= vfmt
            trajectory[:,7:10] *= afmt



            # Change of reference frame to ecliptic
            if ref_frame == "ECLIPTICJ2000":
                # position
                c = SkyCoord(trajectory[:,1], trajectory[:,2], trajectory[:,3], frame='icrs', unit='km', representation_type='cartesian')
                c_ec = c.transform_to('barycentricmeanecliptic')
                c_ec.representation_type = 'cartesian'
                trajectory[:,1] = c_ec.x.value
                trajectory[:,2] = c_ec.y.value
                trajectory[:,3] = c_ec.z.value

                # velocity
                # UNIT TO CHANGE : SOLUTION TO FIND...
                cv = SkyCoord(trajectory[:,4], trajectory[:,5], trajectory[:,6], frame='icrs', unit='km', representation_type='cartesian')
                cv_ec = cv.transform_to('barycentricmeanecliptic')
                cv_ec.representation_type = 'cartesian'
                trajectory[:,4] = cv_ec.x.value
                trajectory[:,5] = cv_ec.y.value
                trajectory[:,6] = cv_ec.z.value

                # acceleration
                # UNIT TO CHANGE : SOLUTION TO FIND...
                ca = SkyCoord(trajectory[:,7], trajectory[:,8], trajectory[:,9], frame='icrs', unit='km', representation_type='cartesian')
                ca_ec = ca.transform_to('barycentricmeanecliptic')
                ca_ec.representation_type = 'cartesian'
                trajectory[:,7] = ca_ec.x.value
                trajectory[:,8] = ca_ec.y.value
                trajectory[:,9] = ca_ec.z.value

            bar = tqdm(np.arange(0,len(trajectory),1), ascii=True, desc="Writing File ",
                       bar_format='{l_bar}{bar:5}|[{elapsed}<{remaining}, {rate_fmt}]{bar:-5b}', disable=disable_tqdm)
            for i in bar:
                self.tab_write(time_list[i],
                                '{:.{prec}e}'.format(trajectory[i,1], prec=prec_pos),
                                '{:.{prec}e}'.format(trajectory[i,2], prec=prec_pos),
                                '{:.{prec}e}'.format(trajectory[i,3], prec=prec_pos),
                                '{:.{prec}e}'.format(trajectory[i,4], prec=prec_vel),
                                '{:.{prec}e}'.format(trajectory[i,5], prec=prec_vel),
                                '{:.{prec}e}'.format(trajectory[i,6], prec=prec_vel),
                                '{:.{prec}e}'.format(trajectory[i,7], prec=prec_acc),
                                '{:.{prec}e}'.format(trajectory[i,8], prec=prec_acc))
                                #'{:15.{prec}e}'.format(acc[2], prec=prec_acc))
                self.file.write('%s' % '{:.{prec}e}'.format(trajectory[i,9], prec=prec_acc))
                self.file.write('\n')
        self.file.close()

        return
