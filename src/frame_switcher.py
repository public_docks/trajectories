#!/usr/bin/env python3
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 28/09/2021
#	Program  	: DOCKS Propagator
#	Name 		: frame_switcher.py
#	Authors		: Tim GLASIUS; Rashika JAIN; Harshul SHARMA
#	Version 	: 2.0
######################################################################

#import re

import numpy as np
from astropy.coordinates import SkyCoord

from src import config
from src.model_tensor.gravitational import bodies
from src.tools import motion

"""
TrajParser is a modified (adapted for the propulsion module) version of the same class in src/trajectory_parser.py
"""


class TrajParser:
    """Class giving the location of the spacecraft at a certain epoch.

    This class reads the contents of a host trajectory file and returns 
    data from the last line. The file should have as format a list of 
    data without containing any string. The class TrajParser uses following
    classes: Date, Position and Velocity.
    Recovered data are:
    -d: represents an epoch (a fixed point in time)
    -p: the position vector of the spacecraft
    -v: the velocity vector of the spacecraft.
    """

    def parse_traj(self, date, sat_pos, sat_vel, local_frame):
        """Method reading the contents of a specified trajectory file and 
        returning the data from last line of file as a tuple of an epoch, 
        an array of position vector and an array of velocity vector.
        Returns date (YY/MM/DD HH/MM/SS), position (m), velocity (m/s).
        The returned position is SSB centered and in the ICRF reference frame.
        """

        p = motion.Position()
        v = motion.Velocity()
        #data = data.strip()

        #values = [element for element in re.split(r'[\s,;]+', data)]
        #values = np.array(values, dtype=np.float64)

        #date = int(values[0]) + (float(values[1]) / 86400)
        #nb_col = 1

        # position in m
        #if config.out_position_fmt == "KM":
        #    values[2:5] = values[2:5] * 1000
        #if config.out_position_fmt == "AU":
        #   values[2:5] = values[2:5] * 1000. * 149597870.700

        # velocity in m/s
        #if config.out_velocity_fmt == "KM/S":
        #    values[5:8] = values[5:8] * 1000.
        #if config.out_velocity_fmt == "AU/D":
        #    values[5:8] = values[5:8] * 1000. * 149597870.700 / 86400.

        #from src.instances import central_bodi_output
        self.body_self = local_frame
        pos, vel, _ = np.array(self.body_self.eph(date))  # recover pos (m) & vel (m/s)

        if config.ref_frame == "ICRF":
            c_ic = SkyCoord(sat_pos[0], sat_pos[1], sat_pos[2], frame='icrs', unit='m',
                            representation_type='cartesian')
            p.x = c_ic.x.value + pos[0]
            p.y = c_ic.y.value + pos[1]
            p.z = c_ic.z.value + pos[2]
            cv_ic = SkyCoord(sat_vel[0], sat_vel[1], sat_vel[2], frame='icrs', unit='m',
                             representation_type='cartesian')
            v.vx = cv_ic.x.value + vel[0]
            v.vy = cv_ic.y.value + vel[1]
            v.vz = cv_ic.z.value + vel[2]

        if config.ref_frame == "ECLIPTICJ2000":
            c = SkyCoord(sat_pos[0], sat_pos[1], sat_pos[2], frame='barycentrictrueecliptic',
                         unit='m', representation_type='cartesian')
            c_ic = c.transform_to('icrs')
            c_ic.representation = 'cartesian'
            p.x = c_ic.x.value + pos[0]
            p.y = c_ic.y.value + pos[1]
            p.z = c_ic.z.value + pos[2]

            # Velocity
            # UNIT TO CHANGE : SOLUTION TO FIND...
            cv = SkyCoord(sat_vel[0], sat_vel[1], sat_vel[2], frame='barycentrictrueecliptic',
                          unit='m', representation_type='cartesian')
            cv_ic = cv.transform_to('icrs')
            cv_ic.representation = 'cartesian'
            v.vx = cv_ic.x.value + vel[0]
            v.vy = cv_ic.y.value + vel[1]
            v.vz = cv_ic.z.value + vel[2]

        return date, p.xyz_array(), v.vxvyvz_array()  # date (in mjd, in days), pos(m), v(m/s)


class frame_switcher:

    def ssb_to_body(self, body, date, position=0, velocity=0, accel=0):
        """
        Translation between ICRF SSB and ICRF of the chosen body.
        
        Parameters:
        ----------
        position : position in ICRF SSB m
        velocity : velocity in ICRF SSB m/s
        body : name of body
        date : date of iteration

        Returns:
        -------
        Position in ICRF of the chosen body in m
        Velocity in ICRF of the chosen body in m/s
        """
        body_prop = bodies.UnKnownBody("%s" % body, config.unknown_bodies[body][0], config.unknown_bodies[body][1],
                                       config.unknown_bodies[body][2])

        pos_body, vel_body, acc_body = np.array(body_prop.eph(date))

        position = position - pos_body
        velocity = velocity - vel_body
        accel = accel - acc_body
        return position, velocity, accel

    def body_to_ssb(self, accel, body, date):
        """
        Translation between ICRF of body and ICRF SSB.
        
        Parameters:
        ----------
        position : position in ICRF body m
        velocity : velocity in ICRF body m/s
        body : name of body
        date : date of iteration

        Returns:
        -------
        Position in ICRF SSB in m
        Velocity in ICRF SSB in m/s
        """
        body_prop = bodies.UnKnownBody("%s" % body, config.unknown_bodies[body][0],
                                       config.unknown_bodies[body][1],
                                       config.unknown_bodies[body][2])

        pos_body, vel_body, acc_body = np.array(body_prop.eph(date))
        # position = position - pos_body
        # velocity = velocity - vel_body
        accel = accel + acc_body
        return accel
