# -*- coding: utf-8 -*-
###########################################################################
#	Modified date	: 26/04/2024
#	Program  	: DOCKS Propagator
#	Name 		: compute_ref_frame.py.py
#	Authors		: Tomas NUNES
#	Version 	: 1.1
###########################################################################

import operator

from numpy import linalg

from src import config


class Select_frame:
    """A class selecting the frame in which the computations will be done (Central Body)"""

    def __init__(self, central_body, bodies, cgm_bodies):
        self.Bodies = bodies
        self.Cgm_Bodies = cgm_bodies
        self.Central_Body = central_body

    def Local_frame(self):
        return self.Central_Body.name.lower(), self.Central_Body
