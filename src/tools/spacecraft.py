#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 26/04/2024
#	Program  	: DOCKS Propagator
#	Name 		: spacecraft.py
#	Authors		: Nima TRAORE ; Florian JOUSSEAUME; Tomas NUNES
#	Version 	: 2.1
######################################################################

class Spacecraft:
    """Class defining a spacecraft features.

    Attributes defined here:
    -mass: the mass of the spacecraft, in kg
    -area_exposed_to_sun: the exposed area to the Sun
    -reflectivity: the coefficient indicating how the spacecraft reacts to
    the interaction with photons. 

    """
    _mass = 0.0
    _area_exposed_to_sun = 0.0
    _reflectivity = 0.0
    _mu = _mass * 6.67430 * 1e-11 # m^3/kg/s^2

    def get_mass(self):
        """Method called when trying to read the attribute 'mass'."""
        return Spacecraft._mass

    def get_area_exposed_to_sun(self):
        """Method called when trying to read the attribute 'area_exposed_to_sun'."""
        return Spacecraft._area_exposed_to_sun

    def get_reflectivity(self):
        """Method called when trying to read the attribute 'reflectivity'."""
        return Spacecraft._reflectivity
    
    def get_mu(self):
        """Method called when trying to read the attribute 'mu'."""
        return Spacecraft._mu
