#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 26/04/2024
#	Program  	: DOCKS Propagator
#	Name 		: tensor_constructor.py
#	Authors		: Nima TRAORE ; Florian JOUSSEAUME; Laëtitia LEBEC; Rashika JAIN; Harshul SHARMA; Tomas NUNES
#	Version 	: 5.1
######################################################################

import numpy as np

from src import instances
#from src.config import propagator_attributes
from src.model_tensor.gravitational import complex_models
from src.model_tensor.gravitational import sphr
from src.model_tensor.non_gravitational import srp
from src.model_tensor.non_gravitational.propulsion import scenario
from src.config import cgm_mode


class AccelConstructor:
    """Class computing the sum of accelerations to be integrated numerically.

    Attributes defined here:
    -list_models: list of gravitational perturbations to be taken into account by the propagator.
    -list_perturbations: list of non-gravitational perturbations that can interact with spacecraft.
    -list_sph_harm: list of complex gravitational perturbations that can interact with spacecraft.

    Method defined here:
    -get_sum_accelerations(): gives the sum of the accelerations in m/s².
    
    Input:
    -date in mjd
    -satellite position in m
    -satellite position in m
    -satellite velocity in m/s
    -cental body
    
    Returns the sum of the several perturbative accelerations, in m/s².
    """
    def __init__(self, list_models, list_perturbations, list_sph_harm):
        """Constructor of the class AccelConstructor."""
        self.list_models = list_models
        self.list_perturbations = list_perturbations
        self.list_sph_harm = list_sph_harm
        self.grav_bodies_dict = {}
        for model in self.list_models:
            self.grav_bodies_dict[model] = sphr.SphericalBody(model)
        if cgm_mode:
            for model in self.list_sph_harm:
                self.grav_bodies_dict[model] = complex_models.ComplexBody(model)
        for model in self.list_perturbations:
            if model == "srp":
                self.non_g_srp = srp.RadiationPressure(model)


    def get_sum_accelerations(self, date, satellite_position, satellite_velocity, flag_prop, local_frame):
        """Computes the sum of the different acceleration, in m/s², to be integrated
        numerically. Returns a tensor 3*3.
        The parameter "satellite_position" is expressed in m.
        """
        list_acc = []
        acc = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]])
        list_acc.append(acc)

        # Get Central Body state wrt to SSB
        r_center_computation_wrt_ssb, _, _ = np.array(local_frame.eph(date))
        # Get sattelite state wrt to SSB for easier relative position computation
        # satellite_position_wrt_ssb = r_center_computation_wrt_ssb + satellite_position
        
        # Non-gravitational perturbations
        for model in self.list_perturbations:
            if model == "srp":
                acc = self.non_g_srp.get_acceleration(date, satellite_position, r_center_computation_wrt_ssb, local_frame)
                list_acc.append(acc)
            # Propulsion
            if model == "propulsion":
                #time_step = propagator_attributes.get("time_step")
                scenario_prop = scenario(instances.prop_input.t_start, instances.prop_input.t_end,
                                         instances.prop_input.direction, instances.prop_input.mag,
                                         instances.prop_input.body_prop)
                prop_acc = scenario_prop.get_acc_prop(date, satellite_position, satellite_velocity,
                                                      local_frame, flag_prop)  # date conversion from sec to days
                list_acc.append(prop_acc)

        # # Gravitational perturbations
        # for model in self.list_models:
        #     acc = self.grav_bodies_dict[model].get_acceleration(date, satellite_position, local_frame, r_center_computation_wrt_ssb)
        #     list_acc.append(acc)

        # # Complex gravitational models defined by the scientist
        # for model in self.list_sph_harm:
        #     acc = self.grav_bodies_dict[model].get_acceleration(date, satellite_position, local_frame, r_center_computation_wrt_ssb)
        #     list_acc.append(acc)
        
        # Gravitational perturbations, Including Complex gravitational models
        for model in self.grav_bodies_dict:
            acc = self.grav_bodies_dict[model].get_acceleration(date, satellite_position, local_frame, r_center_computation_wrt_ssb)
            list_acc.append(acc)

        sum_acc = sum(acceleration for acceleration in list_acc)  # m/s²

        return sum_acc
