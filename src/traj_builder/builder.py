#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 26/04/2024
#	Program  	: DOCKS Propagator
#	Name 		: builder.py
#	Authors		: Nima TRAORE ; Florian JOUSSEAUME; Laëtitia LEBEC; Rashika JAIN: Tomas NUNES
#	Version 	: 6.1
######################################################################

import numpy as np

from src import trajectory_parser
from src.traj_builder import accumulator


class Builder:
    """Class computing the trajectory propagation of a given Spacecraft

    Attributes defined here:
    -time_step: time step between two propagation (in days)
    -step_number: number of propagation points to get

    Methods defined here:
    -compute_traj(): computes the trajectory propagation.
    
    Input: propagator type, step number, time step (in days), host trajectory file
    
    Returns the final accumulated trajectory with:
    -position (in m), velocity (in m/s) & acceleration (in m/s²) for each date (mjd)
    """

    def __init__(self, propagator, end_date, step_number, time_step, output_dir, output_file, computation_frame, traj_vts_headers=None):
        """Constructor of the class Builder."""
        if traj_vts_headers is None:
            traj_vts_headers = []
        self.propagator = propagator
        self.end_date = end_date
        self.step_number = step_number
        self.time_step = time_step
        ####
        self.output_dir = output_dir
        self.output_file = output_file
        self.computation_frame = computation_frame
        self.vts_headers = traj_vts_headers

    def __repr__(self):
        """Method displaying a customized message when an instance of the 
        class BaseModel is called in the command line.
        """
        return "Builder: time_step = '{}', step_number = '{}'".format(
            self.time_step, self.step_number)

    def compute_traj(self, disable_tqdm=False):
        """Computes the trajectory propagation of the spacecraft for a 
        given step_number. 
        Returns a tuple: epoch (mjd), position vector (m), velocity vector (m/s), acceleration vector (m/s²),
        and time step (in s),
        step number, propagator type
        """
        host_traj = trajectory_parser.TrajParser()
        local_frame = self.computation_frame.Local_frame()
        # if config.Debug_Switch_cp1 == 'ON':
        #     with open(config.Debug_folder + "/cp1.txt", 'a+') as cp1:
        #         ar = np.concatenate([probe_pos,probe_vel])
        #         ar = np.insert(ar,0, probe_date)
        #         cp1.write(', '.join(map(str, ar)))
        #         cp1.write("\n")
        #         cp1.close()
        cd = accumulator.Coordinate()
        t, p, v = host_traj.parse_traj_initial(local_frame)  # t: date (MJD seconds), p: pos (m), v: vel (m/s)

        a = np.array([0.0, 0.0, 0.0])
        value = np.concatenate([t, p, v, a], axis=None)

        return cd.accumulate_traj(self.propagator, t, value, self.time_step, self.end_date, self.step_number,
                                  self.output_dir, self.output_file, local_frame, self.vts_headers, disable_tqdm)