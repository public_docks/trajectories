#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 26/04/2024
#	Program  	: DOCKS Propagator
#	Name 		: accumulator.py
#	Authors		: Nima TRAORE ; Florian JOUSSEAUME; Laëtitia LEBEC; Rashika JAIN; Harshul SHARMA; Tomas NUNES
#	Version 	: 7.1
######################################################################

from copy import copy

import numpy as np
from tqdm import tqdm

from src import ref_traj
from src import config
from src import instances
from src.propagator import ias15_module
from src.propagator import rk_module


class Coordinate:
    """Class accumulating the trajectory.

    Method defined here:
    -accumulate_traj(): accumulates trajectories.

    Input:
    -propagator type, time step (in s) & step number
    -time (mjd in second)
    -values: date (in MJ seconds), position (in m), velocity (in m/s), acceleration vector, time step (in s)
    
    Returns values: position (in m), velocity (in m/s), acceleration (in m/s²), and time step (in s) for each date (mjs)
    """

    def accumulate_traj(self, propagator, t, ephem_prev_time, time_step, end_date, step_number, output_dir,
                        output_file, local_frame, traj_vts_headers=None, disable_tqdm=False):
        """Method accumulating spacecraft trajectory points."""
        if traj_vts_headers is None:
            traj_vts_headers = []
        time = t  # mj seconds
        traj = ref_traj.TrajOutput(output_dir, output_file, traj_vts_headers)


        with open(output_dir + "/" + output_file, 'w') as file:
            file.writelines(traj_vts_headers)
            file.close()
        index1 = 1
        index2 = 1
        values = copy(ephem_prev_time)
        values_list = []
        values_list.append(values)
        
        if isinstance(propagator, ias15_module.PropagatorIAS15):
            b_init = np.zeros((3,7))
            t0 = time
            tf = end_date
            dt = time_step
            sign = np.sign(tf-t0)
            bar = tqdm(total=100, ascii=True, desc="Propagation ",
                       bar_format='{l_bar}{bar:5}|{postfix[0]}:{postfix[1][value]:.4f}[{elapsed}<{remaining}, {rate_fmt}]{bar:-5b}',
                       postfix=["Current Time(MJD)", dict(value=time/86400.)],
                       disable=disable_tqdm)
            i_prev = 0

            while (sign*time)<(sign*tf):
                prop_dt_min = 0
                prop_end = False
                
                if 'propulsion' in config.list_perturbations:
                    dt, prop_dt_min, prop_end = instances.tm.next_dt(time, dt, instances.min_dt)

                cmp_pos_vel_acc, t_next, dt_next, err_b, b_final = propagator.propagate_traj(time, values[1:7], dt, b_init, local_frame[1], prop_dt_min)
                
                time = t_next*86400.

                i = ((time-t0)/(tf-t0))*100
                if not disable_tqdm:
                    bar.postfix[1]["value"] = time/86400.

                bar.update(i-i_prev) if not disable_tqdm else None
                i_prev = i

                if prop_end==False:
                    dt = dt_next
                    b_init = b_final + err_b
                else:
                    dt = instances.min_dt
                    b_init = np.zeros((3,7))

                if abs(tf-time)<abs(dt):
                    dt = tf-time

                values = np.append(time, cmp_pos_vel_acc)
                index1 += 1
                values_list.append(values)
            bar.close()
            

        elif isinstance(propagator, rk_module.PropagatorRk):
            t0 = time
            tf = end_date
            sign = np.sign(tf-t0)
            bar = tqdm(total=100, ascii=True, desc="Propagation ",
                       bar_format='{l_bar}{bar:5}|{postfix[0]}:{postfix[1][value]:.4f}[{elapsed}<{remaining}, {rate_fmt}]{bar:-5b}',
                       postfix=["Current Time(MJD)", dict(value=time/86400.)],
                       disable=disable_tqdm)
            i_prev = 0
            dt = time_step
            force = 0
            while (sign*time)<(sign*tf):
                prop_dt_min = 0
                prop_end = False

                if 'propulsion' in config.list_perturbations:
                    dt, prop_dt_min, prop_end = instances.tm.next_dt(time, dt, instances.min_dt)
                    force = 1
                    # NOTE: WARNING if using propulsion, the wau the code id now, force will  always be 1 and therefore, the rkf varaibale step-size wont work,
                    #       because they will be forced to used the current dt and wont get to the loop with algorith to compute optimal step-size.
                    #       For future developer that fixes propulsion, also fix this :)

                h_next, cmp_pos_vel_acc, h_current = propagator.propagate_traj(time, values[1:7], dt, local_frame[1], prop_dt_min, force)
                time = time + h_current

                i = ((time-t0)/(tf-t0))*100
                if not disable_tqdm:
                    bar.postfix[1]["value"] = time/86400.

                bar.update(i-i_prev) if not disable_tqdm else None
                i_prev = i

                if prop_end==False:
                    dt = h_next
                else:
                    dt = instances.min_dt

                if abs(tf-time)<=abs(dt):
                    dt = tf-time
                    force = 1
                    
                values = np.append(time, cmp_pos_vel_acc)
                index1 += 1
                values_list.append(values)
            bar.close()

        traj.export_traj_all(values_list, local_frame, disable_tqdm)
