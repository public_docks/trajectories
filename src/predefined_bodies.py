# -*- coding: utf-8 -*-
###########################################################################
#	Modified date	: 26/04/2024
#	Program  	: DOCKS Propagator
#	Name 		: config.py
#	Authors		: Rashika JAIN; Tomas NUNES
#	Version 	: 1.1
###########################################################################

# This file contains a dictionary with information about gravitational bodies
# that are predefined in the Propagator.

# The format is
# if using SPICE kernels for ephemeris ---> "name-of-body": [mu (m³/s²), 'none', NaifID, Radius (m)] or
# if using text file for ephemeris ---> "name-of-body": [mu (m³/s²), 'ephemeris-file-name' ,'none', Radius (m)]
# mu : float, NaifID : integer, ephemeris-file-name : string, Radius : float


known_bodies = {"ssb": [0, 'none', 0, 0],
                "sun": [132712440041940000000.0, 'none', 10, 696000000],
                "moon": [4843941639988.467, 'none', 301, 1738000],
                # Planetary system barycenters
                "mercury_barycenter": [22032080490394.35, 'none', 1, 2440530],
                "venus_barycenter": [324858606837104.9, 'none', 2, 6051800],
                "earth_barycenter": [403503235269466.8, 'none', 3, 6378136.3],
                "mars_barycenter": [42828314265797.03, 'none', 4, 3396190],
                "jupiter_barycenter": [126712767880664900.0, 'none', 5, 71492000],
                "saturn_barycenter": [37940604374607440.0, 'none', 6, 60268000],
                "uranus_barycenter": [5794549008117694.0, 'none', 7, 25559000],
                "neptune_barycenter": [6836534065205220.0, 'none', 8, 24764000],
                # Planets
                "mercury": [22032080490394.35, 'none', 1, 2440530],
                "venus": [324858606837104.9, 'none', 2, 6051800],
                "earth": [398659293629478.30, 'none', 399, 6378136.3],
                "mars": [42828313289311.500, 'none', 499, 3396190],
                "jupiter": [126686536751784000.0, 'none', 599, 71492000],
                "saturn": [37931239677504600.0, 'none', 699, 60268000],
                "uranus": [5793939212817970.0, 'none', 799, 25559000],
                "neptune": [6835099203587360.0, 'none', 899, 24764000]
                }
