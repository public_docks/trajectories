#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 20/04/2021
#	Program  	: DOCKS Propagator
#	Name 		: quaternion_parser.py
#	Authors		: Laëtitia LEBEC, Rashika JAIN
#	Version 	: 6.x
######################################################################

import re

import numpy as np
from scipy.interpolate import interp1d
from src.config import eph_information
from astropy.time import Time


class QuatParser:
    """Class that load the datas of the quaternion used in the complex gravitational model
    of a body
    
    Input: Quaternion file for complex gravitational model used
        
    Returns:
    - the interpolation functions for quaternions
    - the current date, in mjd seconds
    """

    def quat(self, quat_file):

        with open(quat_file, 'r') as f:
            data = f.readlines()

        # detecting 'META_STOP' to read required data
        for i, dt in enumerate(data):
            if 'META_STOP' in dt:
                stop = i
                break
        if not ('stop' in locals()):
            raise AttributeError("META_STOP is not detected")
        data_req = data[stop + 2:]

        data = []
        for line in data_req:
            data.append(re.split(r'[\s,;]+', line[0:-1]))
        data = np.array(data, dtype=object)
        sci_start = 1  # column from which QUATERNION data is starting

        eph_time_fmt = eph_information['time']
        if eph_time_fmt == "ISOT":
            tt = data[:, 0].astype(str)
            current_time = Time(tt, format='isot', scale='tdb')
            current_time = current_time.mjd * 86400.

        elif eph_time_fmt == "JD":
            tt = np.float64(data[:, 0])
            current_time = Time(tt, format='jd', scale='tdb')
            current_time = current_time.mjd * 86400.

        elif eph_time_fmt == "MJD_1COL":
            tt = np.float64(data[:, 0])
            current_time = Time(tt, format='mjd', scale='tdb')
            current_time = current_time.mjd * 86400.

        elif eph_time_fmt == "MJD_2COL":  # Default
            tt = np.float64(data[:, 0:2])
            eph_time = tt[:, 0] + tt[:, 1] / 86400.
            current_time = Time(eph_time, format='mjd', scale='tdb')
            current_time = current_time.mjd * 86400.
            sci_start = 2

        q = np.float64(data[:, sci_start:])

        # Definition of the interpolation functions
        fq = interp1d(current_time, q, kind='linear', fill_value="extrapolate", axis=0)

        return current_time, fq
