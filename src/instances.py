# usr/bin/env python
# -*- coding:utf-8 -*-
######################################################################
#   Modified date	: 26/04/2024
#   Program  	: DOCKS Propagator
#   Name 		: instances.py
#   Authors		: Nima TRAORE ; Florian JOUSSEAUME; Laëtitia LEBEC; Rashika JAIN; Harshul SHARMA; Tomas NUNES
#   Version 	: 7.1
######################################################################

# Trajectory Solver instances file for propagating trajectories.
# All variables (ojbects or any parameters) declared or created in the configuration
# file must be passed in this file. Thus, global variables in the configuration file
# should not be present anywhere.

import src.ref_eph as ref_eph
import src.tensor_constructor as tensor_constructor
import numpy as np
from src import config
from src.model_tensor.gravitational import bodies
from src.model_tensor.non_gravitational import srp
from src.model_tensor.non_gravitational.input_scenario_parser import input_file_data
from src.propagator import ias15_module
from src.propagator import rk_module
import src.propagator.master_time_manager as m_tm
from src.tools import spacecraft
from src.traj_builder import builder
from src import compute_ref_frame

# from src import sampling_quality

# Create Variable to Check if Central Body belong to Acceleration Model
central_body_defined = False

# Creation of the class Bodies
Bodies = [bodies.UnKnownBody('ssb', 0, 'none', 0, 0)]

# Define Central Body
if config.central_body_name == 'ssb':
        central_body = Bodies[-1]
        central_body_defined = True

# Set class Bodies
for body in config.list_models:
    bodi = bodies.UnKnownBody("%s" % body, config.unknown_bodies[body][0],
                              config.unknown_bodies[body][1],
                              config.unknown_bodies[body][2],
                              config.unknown_bodies[body][3])
    Bodies.append(bodi)
    # Define Central Body
    if body.lower() == config.central_body_name:
        central_body = Bodies[-1]
        central_body_defined = True

Central_body_input = config.inp_center_name
Central_body_output = config.out_center_name

central_bodi_input = bodies.UnKnownBody("%s" % Central_body_input, config.unknown_bodies[Central_body_input][0],
                                        config.unknown_bodies[Central_body_input][1],
                                        config.unknown_bodies[Central_body_input][2],
                                        config.unknown_bodies[Central_body_input][3])

central_bodi_output = bodies.UnKnownBody("%s" % Central_body_output, config.unknown_bodies[Central_body_output][0],
                                         config.unknown_bodies[Central_body_output][1],
                                         config.unknown_bodies[Central_body_output][2],
                                         config.unknown_bodies[Central_body_output][3])



# If complex gravitational model

cgm_bodies = []
if config.cgm_mode:
    for cgm_body in config.complex_gravitational_bodies:
        cgm_info = config.complex_gravitational_bodies[cgm_body]
        complex_grav_body = bodies.ComplexGravBody("%s" % cgm_body, cgm_info[0], cgm_info[1],
                                                   cgm_info[2], cgm_info[3], cgm_info[4], cgm_info[5], cgm_info[6])
        cgm_bodies.append(complex_grav_body)
        # Define Central Body
        if cgm_body.lower() == config.central_body_name:
            central_body = cgm_bodies[-1]
            central_body_defined = True

# Check if Central Body belongs to Model
if not central_body_defined:
    raise ValueError(f'The Central Body input: {config.central_body_name}, is not in the Acceleration Model.')

#
# compute_ref_frame = compute_ref_frame.Select_frame(config.change_frame, Bodies)
compute_ref_frame = compute_ref_frame.Select_frame(central_body, Bodies, cgm_bodies)

# Class attributes for the class UnKnownBody
# bodies.UnKnownBody.input_dir = config.new_bodies_input_dir

# Class attributes for the class RadiationPressure
srp.RadiationPressure._luminosity = config.solar["luminosity"]

# Class attributes for the Propulsion
# instance for propulsion
if 'propulsion' in config.list_perturbations:
    prop_input = input_file_data("%s" % config.prop_file)

    tm = m_tm.master_time_manager(prop_input.t_start, prop_input.t_end, prop_input.dt, np.sign(config.time_step)) # Initialization of Master Time Manager
    min_dt = tm.min_dt_check(config.propagator_attributes["time_step_min"])
else:
    min_dt = config.propagator_attributes["time_step_min"]

# Instance of class AccelConstructor
tensor_constructor = tensor_constructor.AccelConstructor(config.list_models,
                                                         config.list_perturbations,
                                                         config.list_sph_harm)
    
# Initialize Integrators
if "rk4" == config.propagator_type or "rkf45" == config.propagator_type or "rkf56" == config.propagator_type or "rkf78" == config.propagator_type:
    # Instance of class PropagatorRkf45
    propagator = rk_module.PropagatorRk(config.propagator_type, tensor_constructor,
                                              min_dt,
                                              config.propagator_attributes["time_step_max"],
                                              config.propagator_attributes["tolerance"],
                                              config.propagator_attributes["safety_factor"],
                                              config.propagator_attributes["time_step"])
    
if "ias15" == config.propagator_type:
    # Instance of class PropagatorIAS15
    propagator = ias15_module.PropagatorIAS15(tensor_constructor,
                                              min_dt,
                                              #config.propagator_attributes["time_step_max"],
                                              config.propagator_attributes["tolerance"],
                                              #config.propagator_attributes["safety_factor"]
                                              )

# Instance of class Builder
builder = builder.Builder(propagator,config.propagator_attributes["end_date"], config.propagator_attributes["step_number"],
                          config.propagator_attributes["time_step"], config.output_dir,
                          config.output_file, compute_ref_frame, config.traj_vts_headers)

# Class attributes for the class Spacecraft
spacecraft.Spacecraft._mass = config.spacecraft["mass"]
spacecraft.Spacecraft._area_exposed_to_sun = config.spacecraft["srp_area"]
spacecraft.Spacecraft._reflectivity = config.spacecraft["srp_coefficient"]

# Instance of class TrajOutput
# ref_traj = ref_traj.TrajOutput(config.output_dir, config.output_file, config.traj_vts_headers)

# Instance of class EphOutput
ref_eph = ref_eph.EphOutput(config.list_foreground_objects, config.output_dir,
                            config.foreground_objects_data, config.eph_vts_headers,
                            config.eph_vts_headers_next)

# Instance of sampling quality
# samp_qual = sampling_quality
