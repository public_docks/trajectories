#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 26/04/2024
#	Program  	: DOCKS Propagator
#	Name 		: rotation_matrix_parser.py
#	Authors		: Laëtitia LEBEC, Rashika JAIN; Tomas NUNES
#	Version 	: 6.1
######################################################################

import re

import numpy as np
from src.config import eph_information
from astropy.time import Time
import scipy
from scipy.spatial.transform import Rotation as R


class RotMatrixParser:
    """Class that load the data of the rotation matrix used in the complex gravitational model
    of a body
    
    Input: Rotation matrix file for complex gravitational model used
        
    Returns:
    - the interpolation functions for rotation matrix (slerp interpolation)
    - the current date, in mjd seconds
    """

    def rotmatrix(self, rotation_matrix_file):

        # Recover time and data (rotation matrix) from file
        with open(rotation_matrix_file, 'r') as f:
            data = f.readlines()

        # detecting 'META_STOP' to read required data
        for i, dt in enumerate(data):
            if 'META_STOP' in dt:
                stop = i
                break
        if not ('stop' in locals()):
            raise AttributeError("META_STOP is not detected")
        data_req = data[stop + 2:]

        data = []
        for line in data_req:
            data.append(re.split(r'[\s,;]+', line[0:-1]))
        data = np.array(data, dtype=object)
        sci_start = 1  # column from which ROTATION MATRIX data is starting

        eph_time_fmt = eph_information['time']
        if eph_time_fmt == "ISOT":
            tt = data[:,0].astype(str)
            current_time = Time(tt, format='isot', scale='tdb')
            current_time = current_time.mjd * 86400.

        elif eph_time_fmt == "JD":
            tt = np.float64(data[:,0])
            current_time = Time(tt, format='jd', scale='tdb')
            current_time = current_time.mjd * 86400.

        elif eph_time_fmt == "MJD_1COL":
            tt = np.float64(data[:, 0])
            current_time = Time(tt, format='mjd', scale='tdb')
            current_time = current_time.mjd * 86400.

        elif eph_time_fmt == "MJD_2COL":  # Default
            tt = np.float64(data[:, 0:2])
            eph_time = tt[:,0] + tt[:,1] / 86400.
            current_time = Time(eph_time, format='mjd', scale='tdb')
            current_time = current_time.mjd * 86400.
            sci_start = 2

        rm = np.float64(data[:, sci_start:])
        # Converto to Scipy Matrix Form
        r_list = []
        for rmm in rm:
            r = np.array(([ [rmm[0], rmm[1], rmm[2]],
                            [rmm[3], rmm[4], rmm[5]],
                            [rmm[6], rmm[7], rmm[8]] ]))
            r_list.append(r)
        r_list = R.from_matrix(r_list)

        # Definition of the interpolation functions
        frm = scipy.spatial.transform.Slerp(current_time, r_list)

        return current_time, frm
