#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 
#	Program  	: DOCKS Propagator
#	Name 		: ref_eph.py
#	Authors		: Nima TRAORE ; Florian JOUSSEAUME
#	Version 	: 2.0
######################################################################

import datetime as dtm
import time as tm

import numpy as np
from scipy.interpolate import interp1d

from src.config import unknown_bodies
from src.model_tensor.gravitational import bodies
from src.tools import motion


class EphOutput:
    """Class exporting ephemerides of bodies to a file in vts format
    
    This class hasn't been validated yet. Unused in Beta Release.
    """

    def __init__(self, foreground_objects, output_dir, objects_data, vts_headers=[], vts_headers_next=[]):
        """Constructor of class EphOutput."""
        self.foreground_objects = foreground_objects
        self.vts_headers = vts_headers
        self.vts_headers_next = vts_headers_next
        self.output_dir = output_dir
        self.objects_data = objects_data

    def tab_write(self, *args):
        """Method writting to file."""
        for val in args:
            self.file.write('%s\t' % val)

    def export_eph(self, trajectory):
        """Exporting ephemerides of forward celestial bodies to specified file
        with respect to the spacecraft trajectory."""
        dt = motion.Date()
        ps = motion.Position()
        """Calculation of the points selected for the ephemeris, these points
        depend of 3 different criterias dr(radius), dl(longitude) and 
        dth (latitude), respectively their acceptable difference between two points 
        of the trajectory."""
        for body in self.foreground_objects:
            # Definition of the vectors and variable
            time = list(range(len(trajectory)))
            time_sech = list(range(len(trajectory) * 2 - 1))
            xpos = list(range(len(trajectory)))
            ypos = list(range(len(trajectory)))
            zpos = list(range(len(trajectory)))

            dr = 100
            opt_err = self.objects_data[body]["d_opt"]

            # Time and pos of traj
            k = 0
            for val in trajectory:
                time[k] = val[0]
                xpos[k] = val[1]
                ypos[k] = val[2]
                zpos[k] = val[3]
                k = k + 1

            # First ephemeride of the body
            # bd = bodies.UnKnownBody("%s" % body, unknown_bodies[body]["mu"],\
            #     unknown_bodies[body]["radius"], \
            #     unknown_bodies[body]["traj_file"])
            bd = bodies.UnKnownBody("%s" % body, unknown_bodies[body]["mu"],
                                    unknown_bodies[body]["traj_file"],
                                    unknown_bodies[body]["naif_id"])
            time_eph, pos_eph = bd.eph()
            time_eph = time_eph / 86400

            # Oversampling of traj
            i = 1
            k = 0
            time_sech[0] = time[0]

            for values in list(range(len(time) - 1)):
                h = (time[k + 1] - time[k]) / 2
                time_sech[i] = time_sech[i - 1] + h
                time_sech[i + 1] = time[k + 1]
                k = k + 1
                i = i + 2

            # Assembly of time vectors 
            time_assembly = list(range(len(time_sech) + len(time_eph)))
            ind1 = 0
            ind2 = 0
            cond1 = 0
            cond2 = 0

            """ Determination of the upper and lower limits of the intersection """
            m1 = min(time_eph)
            m2 = min(time_sech)
            if m1 < m2:
                lim_inf = m2
            else:
                lim_inf = m1

            m3 = max(time_eph)
            m4 = max(time_sech)
            if m3 < m4:
                lim_sup = m3
            else:
                lim_sup = m4

            for value in list(range(len(time_sech) + len(time_eph) - 1)):
                if time_sech[ind1] > time_eph[ind2] or time_sech[ind1] == time_eph[ind2] or cond1 == 1 and cond2 == 0:
                    time_assembly[value] = time_eph[ind2]
                    if ind2 != len(time_eph) - 1:
                        ind2 = ind2 + 1
                    else:
                        cond2 = 1
                else:
                    time_assembly[value] = time_sech[ind1]
                    if ind1 != len(time_sech) - 1:
                        ind1 = ind1 + 1
                    if ind1 == len(time_sech) - 2:
                        cond1 = 1
                if cond2 == 1:
                    time_assembly[value] = time_sech[ind1]
                    ind1 = ind1 + 1

            new_time = []
            for value in time_assembly:
                if value >= lim_inf and value <= lim_sup and value not in new_time:
                    new_time.append(value)

            # Interpolation of the missing data of traj with time_assembly
            fx = interp1d(time, xpos, fill_value="extrapolate")
            fy = interp1d(time, ypos, fill_value="extrapolate")
            fz = interp1d(time, zpos, fill_value="extrapolate")
            pos_ech = np.array([fx(new_time), fy(new_time), fz(new_time)])

            x = list(range(len(pos_ech[0, :])))
            y = list(range(len(pos_ech[0, :])))
            z = list(range(len(pos_ech[0, :])))

            # Calculation of the different points relative position
            k = 0
            for values in list(range(len(pos_ech[0, :]))):
                _, _, r_lum = ps.car_to_sph(bd.relative_position(new_time[k], pos_ech[:, k]))
                temps_lum = r_lum / 299792458 / 86400
                view_time = new_time[k] - temps_lum
                x[k], y[k], z[k] = bd.relative_position(view_time, pos_ech[:, k])
                k = k + 1
            time2 = list(range(len(x)))
            time2[0] = new_time[0]
            #
            i = 1
            l = 0
            j = 0
            vect_u = np.zeros((1, 3))
            # Calculation of the evolution of delta and selection of the points
            for m in list(range(len(x) - 1)):

                vect_u = (x[i], y[i], z[i])
                vect_v = (x[l], y[l], z[l])

                d_opt = np.arccos(
                    np.dot(vect_u, vect_v) / (np.dot(np.linalg.norm(vect_u), np.linalg.norm(vect_v)))) * 180 / np.pi
                if d_opt > opt_err:
                    l = i
                    j = j + 1
                    time2[j] = new_time[m + 1]
                i = i + 1
            j = j + 1
            # Time is resized
            time2 = time2[0:j]
            fx = interp1d(time_eph, pos_eph[0], fill_value="extrapolate")
            fy = interp1d(time_eph, pos_eph[1], fill_value="extrapolate")
            fz = interp1d(time_eph, pos_eph[2], fill_value="extrapolate")
            pos = np.array([fx(time2), fy(time2), fz(time2)])

            # File writting
            with open(self.output_dir + "%s_" % body + "%s" % dtm.date.today() \
                      + "_%s.eph" % tm.strftime("%H%M%S"), 'w') as self.file:
                self.file.writelines(self.vts_headers)
                if self.vts_headers or self.vts_headers_next:
                    self.file.write("TARGET_NAME  = %s\n" % body.upper())
                    self.file.write("TARGET_ID    = OBJECT-%s\n" % body)
                self.file.writelines(self.vts_headers_next)
                for values in list(range(len(time2))):
                    x, y, z = pos[:, values]
                    mjd_integer_part, mjd_decimal_part = dt.mjd_vts(time2[values])
                    self.tab_write(mjd_integer_part, format(mjd_decimal_part, '.4f'), format(x, '.6f'),
                                   format(y, '.6f'), format(z, '.6f'))
                    self.file.write('\n')
