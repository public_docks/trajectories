#!usr/bin/env python
# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 28/09/2021
#	Program  	: DOCKS Propagator
#	Name 		: rk_module.py
#	Authors		: Nima TRAORE ; Florian JOUSSEAUME; Laëtitia LEBEC; Rashika JAIN; Harshul SHARMA
#	Version 	: 5.0
######################################################################


import numpy as np


class Propagator:
    """ Virtual class devoting to the trajectory propagation. 

    Attributes defined here:
    -tensor_constructor: provides the acceleration to be taken into account for
     trajectory propagation.

    Methods define here:
    -rate_function(): gives the rate of the evaluated function respects to 
    the position and velocity vectors.
    
    Input:
    -date, in seconds, in mjd format
    -position and velocity in m and m/s
    
    Returns the velocity (in m/s) and acceleration (in m/s²) at a given date
    """

    def __init__(self, tensor_constructor):
        """Constructor of class Propagator."""
        self.tensor_constructor = tensor_constructor

    def rate_function(self, date, pos_vel, flag_prop, local_frame):
        """Method providing the acceleration(s), in m/s², undergone by the spacecraft
        from celestial bodies like Sun, planets, asteroids... and from dissipative
        forces like solar radiation pressure, magnetic field... to be integrated 
        in order to return new both velocity, in m/s, and acceleration, in m/s²
        of the spacecraft at a given date, in mjd.
        """
        pos = pos_vel[0:3]  # m
        vel = pos_vel[3:6]  # m/s

        # Sum of perturbative accelerations
        acc = self.tensor_constructor.get_sum_accelerations(date, pos, vel, flag_prop, local_frame)  # m/s²
        acc = np.array([acc[0][0], acc[1][1], acc[2][2]])

        return np.append(vel, acc)

    def propagate_traj(self):
        """Virtual method."""
        pass
