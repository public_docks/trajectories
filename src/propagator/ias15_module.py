# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 26/04/2024
#	Program  	: DOCKS Propagator
#	Name 		: ias15_module.py
#	Authors		: Harshul SHARMA; Rashika JAIN; Tomas NUNES
#	Version 	: 1.1
######################################################################

import numpy as np
import src.propagator.propagator_module as pg_m


class PropagatorIAS15(pg_m.Propagator):
    """Class devoting to the trajectory propagation.

    This class inherits from class propagator_module.Propagator.

    Attributes defined here:
    -h_min: step time minimum (in s)
    -tolerance: tolerance to be compared with estimating error

    Methods define here:
    -propagate_traj(): computes the Implicit integrator with Adaptive time Stepping of 15th order numerical integration method.

    propagate_traj input:
    -current date in second, in mjd format
    -position and velocity in m and m/s
    -time step in s
    
    Returns the time step (in seconds), position (in m), the velocity (in m/s),
    and the acceleration (in m/s²) using the previous one
    """

    def __init__(self, tensor_constructor, h_min, tolerance):
        """Constructor of class PropagatorIAS15."""
        pg_m.Propagator.__init__(self, tensor_constructor)
        self.h_min = np.float64(h_min)
        #self.h_max = h_max
        self.tolerance = np.float64(tolerance)
        #self.safty_factor = safty_factor
        self.h_sub = np.array([0.0562625605369221464656521910318, 0.180240691736892364987579942780, 0.352624717113169637373907769648, 0.547153626330555383001448554766, 0.734210177215410531523210605558, 0.885320946839095768090359771030, 0.977520613561287501891174488626])
        
        self.c = np.zeros((7,7))
        for j in range(1,8):
            for k in range(1,8):
                self.c[j-1,j-1] = 1
        
                if j>1:
                    self.c[j-1,0] = -self.h_sub[j-2]*self.c[j-2,0]
            
                if j>k:
                    self.c[j-1,k-1] = self.c[j-2,k-2]-(self.h_sub[j-2]*self.c[j-2,k-1])
        
        self.d = np.zeros((7,7))
        for j in range(1,8):
            for k in range(1,8):
                self.d[j-1,j-1] = 1
        
                if j>1:
                    self.d[j-1,0] = self.h_sub[0]**(j-1)
            
                if j>k:
                    self.d[j-1,k-1] = self.d[j-2,k-2]+(self.h_sub[k-1]*self.d[j-2,k-1])

    def b_to_posvelacc(self, y0, yd0, ydd0, dt, b, date, n, local_frame):
        i = n-1
        flag = 1
        yd = yd0 + (self.h_sub[i]*dt*(ydd0+(self.h_sub[i]*(((b[:,0].reshape(3,1))/2.)+(self.h_sub[i]*(((b[:,1].reshape(3,1))/3.)+(self.h_sub[i]*(((b[:,2].reshape(3,1))/4.)+(self.h_sub[i]*(((b[:,3].reshape(3,1))/5.)+(self.h_sub[i]*(((b[:,4].reshape(3,1))/6.)+(self.h_sub[i]*(((b[:,5].reshape(3,1))/7.)+(self.h_sub[i]*(((b[:,6].reshape(3,1))/8.)))))))))))))))))
        y = y0 + (yd0*self.h_sub[i]*dt) + ((self.h_sub[i]**2)*(dt**2)*((ydd0/2.)+(self.h_sub[i]*(((b[:,0].reshape(3,1))/6.)+(self.h_sub[i]*(((b[:,1].reshape(3,1))/12.)+(self.h_sub[i]*(((b[:,2].reshape(3,1))/20.)+(self.h_sub[i]*(((b[:,3].reshape(3,1))/30.)+(self.h_sub[i]*(((b[:,4].reshape(3,1))/42.)+(self.h_sub[i]*(((b[:,5].reshape(3,1))/56.)+(self.h_sub[i]*(((b[:,6].reshape(3,1))/72.)))))))))))))))))
        ydd = (self.rate_function(date+(self.h_sub[i]*dt), np.append(y,yd,axis=0).reshape(6,), flag, local_frame))[3:].reshape(3,1)#/1000
        return y, yd, ydd
    
    def final_posvelacc(self, y0, yd0, ydd0, dt, b, date, local_frame):
        flag = 2
        vel = yd0 + (dt*(ydd0+((b[:,0].reshape(3,1))/2.)+((b[:,1].reshape(3,1))/3.)+((b[:,2].reshape(3,1))/4.)+((b[:,3].reshape(3,1))/5.)+((b[:,4].reshape(3,1))/6.)+((b[:,5].reshape(3,1))/7.)+((b[:,6].reshape(3,1))/8.)))
        pos = y0 + (yd0*dt) + ((dt**2)*((ydd0/2.)+((b[:,0].reshape(3,1))/6.)+((b[:,1].reshape(3,1))/12.)+((b[:,2].reshape(3,1))/20.)+((b[:,3].reshape(3,1))/30.)+((b[:,4].reshape(3,1))/42.)+((b[:,5].reshape(3,1))/56.)+((b[:,6].reshape(3,1))/72.)))
        acc = (self.rate_function(date+dt, np.append(pos,vel,axis=0).reshape(6,), flag, local_frame))[3:].reshape(3,1)#/1000
        posvelacc = (np.vstack((pos,vel,acc))).reshape(9,)
        return posvelacc
        
    
    def ydd_to_g(self, ydd, ydd0, g_prev, n):
        if n==1:
            g1 = ((ydd-ydd0))/self.h_sub[0]
            g2 = g_prev[:,1].reshape(3,1)
            g3 = g_prev[:,2].reshape(3,1)
            g4 = g_prev[:,3].reshape(3,1)
            g5 = g_prev[:,4].reshape(3,1)
            g6 = g_prev[:,5].reshape(3,1)
            g7 = g_prev[:,6].reshape(3,1)
        
        elif n==2:
            g1 = g_prev[:,0].reshape(3,1)
            g2 = ((ydd-ydd0-(g1*self.h_sub[1])))/(self.h_sub[1]*(self.h_sub[1]-self.h_sub[0]))
            g3 = g_prev[:,2].reshape(3,1)
            g4 = g_prev[:,3].reshape(3,1)
            g5 = g_prev[:,4].reshape(3,1)
            g6 = g_prev[:,5].reshape(3,1)
            g7 = g_prev[:,6].reshape(3,1)
        
        elif n==3:    
            g1 = g_prev[:,0].reshape(3,1)
            g2 = g_prev[:,1].reshape(3,1)
            g3 = ((ydd-ydd0-(g1*self.h_sub[2])-(g2*self.h_sub[2]*(self.h_sub[2]-self.h_sub[0]))))/(self.h_sub[2]*(self.h_sub[2]-self.h_sub[0])*(self.h_sub[2]-self.h_sub[1]))
            g4 = g_prev[:,3].reshape(3,1)
            g5 = g_prev[:,4].reshape(3,1)
            g6 = g_prev[:,5].reshape(3,1)
            g7 = g_prev[:,6].reshape(3,1)
            
        elif n==4:
            g1 = g_prev[:,0].reshape(3,1)
            g2 = g_prev[:,1].reshape(3,1)
            g3 = g_prev[:,2].reshape(3,1)
            g4 = ((ydd-ydd0-(g1*self.h_sub[3])-(g2*self.h_sub[3]*(self.h_sub[3]-self.h_sub[0]))-(g3*self.h_sub[3]*(self.h_sub[3]-self.h_sub[0])*(self.h_sub[3]-self.h_sub[1]))))/(self.h_sub[3]*(self.h_sub[3]-self.h_sub[0])*(self.h_sub[3]-self.h_sub[1])*(self.h_sub[3]-self.h_sub[2]))
            g5 = g_prev[:,4].reshape(3,1)
            g6 = g_prev[:,5].reshape(3,1)
            g7 = g_prev[:,6].reshape(3,1)
            
        elif n==5:
            g1 = g_prev[:,0].reshape(3,1)
            g2 = g_prev[:,1].reshape(3,1)
            g3 = g_prev[:,2].reshape(3,1)
            g4 = g_prev[:,3].reshape(3,1)
            g5 = ((ydd-ydd0-(g1*self.h_sub[4])-(g2*self.h_sub[4]*(self.h_sub[4]-self.h_sub[0]))-(g3*self.h_sub[4]*(self.h_sub[4]-self.h_sub[0])*(self.h_sub[4]-self.h_sub[1]))-(g4*self.h_sub[4]*(self.h_sub[4]-self.h_sub[0])*(self.h_sub[4]-self.h_sub[1])*(self.h_sub[4]-self.h_sub[2]))))/(self.h_sub[4]*(self.h_sub[4]-self.h_sub[0])*(self.h_sub[4]-self.h_sub[1])*(self.h_sub[4]-self.h_sub[2])*(self.h_sub[4]-self.h_sub[3]))
            g6 = g_prev[:,5].reshape(3,1)
            g7 = g_prev[:,6].reshape(3,1)
            
        elif n==6:
            g1 = g_prev[:,0].reshape(3,1)
            g2 = g_prev[:,1].reshape(3,1)
            g3 = g_prev[:,2].reshape(3,1)
            g4 = g_prev[:,3].reshape(3,1)
            g5 = g_prev[:,4].reshape(3,1)
            g6 = ((ydd-ydd0-(g1*self.h_sub[5])-(g2*self.h_sub[5]*(self.h_sub[5]-self.h_sub[0]))-(g3*self.h_sub[5]*(self.h_sub[5]-self.h_sub[0])*(self.h_sub[5]-self.h_sub[1]))-(g4*self.h_sub[5]*(self.h_sub[5]-self.h_sub[0])*(self.h_sub[5]-self.h_sub[1])*(self.h_sub[5]-self.h_sub[2]))-(g5*self.h_sub[5]*(self.h_sub[5]-self.h_sub[0])*(self.h_sub[5]-self.h_sub[1])*(self.h_sub[5]-self.h_sub[2])*(self.h_sub[5]-self.h_sub[3]))))/(self.h_sub[5]*(self.h_sub[5]-self.h_sub[0])*(self.h_sub[5]-self.h_sub[1])*(self.h_sub[5]-self.h_sub[2])*(self.h_sub[5]-self.h_sub[3])*(self.h_sub[5]-self.h_sub[4]))
            g7 = g_prev[:,6].reshape(3,1)
        
        elif n==7:
            g1 = g_prev[:,0].reshape(3,1)
            g2 = g_prev[:,1].reshape(3,1)
            g3 = g_prev[:,2].reshape(3,1)
            g4 = g_prev[:,3].reshape(3,1)
            g5 = g_prev[:,4].reshape(3,1)
            g6 = g_prev[:,5].reshape(3,1)
            g7 = ((ydd-ydd0-(g1*self.h_sub[6])-(g2*self.h_sub[6]*(self.h_sub[6]-self.h_sub[0]))-(g3*self.h_sub[6]*(self.h_sub[6]-self.h_sub[0])*(self.h_sub[6]-self.h_sub[1]))-(g4*self.h_sub[6]*(self.h_sub[6]-self.h_sub[0])*(self.h_sub[6]-self.h_sub[1])*(self.h_sub[6]-self.h_sub[2]))-(g5*self.h_sub[6]*(self.h_sub[6]-self.h_sub[0])*(self.h_sub[6]-self.h_sub[1])*(self.h_sub[6]-self.h_sub[2])*(self.h_sub[6]-self.h_sub[3]))-(g6*self.h_sub[6]*(self.h_sub[6]-self.h_sub[0])*(self.h_sub[6]-self.h_sub[1])*(self.h_sub[6]-self.h_sub[2])*(self.h_sub[6]-self.h_sub[3])*(self.h_sub[6]-self.h_sub[4]))))/(self.h_sub[6]*(self.h_sub[6]-self.h_sub[0])*(self.h_sub[6]-self.h_sub[1])*(self.h_sub[6]-self.h_sub[2])*(self.h_sub[6]-self.h_sub[3])*(self.h_sub[6]-self.h_sub[4])*(self.h_sub[6]-self.h_sub[5]))
        
        g = np.hstack((g1,g2,g3,g4,g5,g6,g7))
        return g
    
    def g_to_b(self, g):
        b_new = np.zeros((3,7))
        b_new[:,0] = (self.c[0,0]*g[:,0])+(self.c[1,0]*g[:,1])+(self.c[2,0]*g[:,2])+(self.c[3,0]*g[:,3])+(self.c[4,0]*g[:,4])+(self.c[5,0]*g[:,5])+(self.c[6,0]*g[:,6])
        b_new[:,1] = (self.c[1,1]*g[:,1])+(self.c[2,1]*g[:,2])+(self.c[3,1]*g[:,3])+(self.c[4,1]*g[:,4])+(self.c[5,1]*g[:,5])+(self.c[6,1]*g[:,6])
        b_new[:,2] = (self.c[2,2]*g[:,2])+(self.c[3,2]*g[:,3])+(self.c[4,2]*g[:,4])+(self.c[5,2]*g[:,5])+(self.c[6,2]*g[:,6])
        b_new[:,3] = (self.c[3,3]*g[:,3])+(self.c[4,3]*g[:,4])+(self.c[5,3]*g[:,5])+(self.c[6,3]*g[:,6])
        b_new[:,4] = (self.c[4,4]*g[:,4])+(self.c[5,4]*g[:,5])+(self.c[6,4]*g[:,6])
        b_new[:,5] = (self.c[5,5]*g[:,5])+(self.c[6,5]*g[:,6])
        b_new[:,6] = (self.c[6,6]*g[:,6])
        return b_new
    
    def b_to_g(self, b):
        g_new = np.zeros((3,7))
        g_new[:,0] = (self.d[0,0]*b[:,0])+(self.d[1,0]*b[:,1])+(self.d[2,0]*b[:,2])+(self.d[3,0]*b[:,3])+(self.d[4,0]*b[:,4])+(self.d[5,0]*b[:,5])+(self.d[6,0]*b[:,6])
        g_new[:,1] = (self.d[1,1]*b[:,1])+(self.d[2,1]*b[:,2])+(self.d[3,1]*b[:,3])+(self.d[4,1]*b[:,4])+(self.d[5,1]*b[:,5])+(self.d[6,1]*b[:,6])
        g_new[:,2] = (self.d[2,2]*b[:,2])+(self.d[3,2]*b[:,3])+(self.d[4,2]*b[:,4])+(self.d[5,2]*b[:,5])+(self.d[6,2]*b[:,6])
        g_new[:,3] = (self.d[3,3]*b[:,3])+(self.d[4,3]*b[:,4])+(self.d[5,3]*b[:,5])+(self.d[6,3]*b[:,6])
        g_new[:,4] = (self.d[4,4]*b[:,4])+(self.d[5,4]*b[:,5])+(self.d[6,4]*b[:,6])
        g_new[:,5] = (self.d[5,5]*b[:,5])+(self.d[6,5]*b[:,6])
        g_new[:,6] = (self.d[6,6]*b[:,6])
        return g_new
    
    def propagate_traj(self, date, pos_vel, dt, b_init, local_frame, prop_dt_min):
        """Complete the Implicit integrator with Adaptive time Stepping numerical integration method 
        by using an adaptive step time control during the computation.
        
        The parameter h, which is the variable time step of the simulation, is expressed in seconds
        """

        dt = np.float64(dt)
        flag = 0
        ydd0 = (self.rate_function(date, pos_vel, flag, local_frame))[3:].reshape(3,1)#/1000
        yd0 = pos_vel[3:].reshape(3,1)#/1000
        y0 = pos_vel[0:3].reshape(3,1)#/1000
        b = b_init
        
        if prop_dt_min==1:
            dt_min = dt
        else:
            dt_min = self.h_min

        g = self.b_to_g(b)
        rpt = 1
        itr = 1
        til_del_b6_prev = 0.0
        while rpt:
            for n in range(1,8):

                y, yd, ydd = self.b_to_posvelacc(y0, yd0, ydd0, dt, b, date, n, local_frame)
                g_new = self.ydd_to_g(ydd, ydd0, g, n)
                b_new = self.g_to_b(g_new)
                
                if n==7:
                    if (itr==1 and np.sum(b)==0):
                        b_predict = b_new
                
                    elif (itr==1 and np.sum(b)!=0):
                        b_predict = b_init
                    
                    del_b6 = np.float64(b_new[:,6].reshape(3,1)) - np.float64(b[:,6].reshape(3,1))
                        
                    til_del_b6 = np.float64(np.amax(abs(del_b6))/np.amax(abs(ydd)))
                    #til_del_b6 = np.float64(np.amax(abs(del_b6/ydd)))
                    
                    if (((til_del_b6<=np.float64(1e-16)) or (itr>2 and til_del_b6_prev<=til_del_b6) or (itr>=12)) and np.isfinite(til_del_b6)) :
                        til_b6 = np.float64(np.amax(abs((b_new[:,6].reshape(3,1))))/np.amax(abs((ydd))))
                        #til_b6 = np.float64(np.amax(abs((b_new[:,6].reshape(3,1))/(ydd))))
                        eps_b = self.tolerance
                        
                        if (np.isfinite(til_b6)):
                            dt_req = np.sign(dt)*np.float64(abs(dt)*(eps_b/til_b6)**(1/7))
                        else:
                            dt_req = np.float64(dt*4)
                        
                        if abs(dt_req) < abs(dt_min):
                            #print(dt_req)
                            dt_req = dt_min
                            
                        if abs(dt_req) > abs(dt):
                            
                            if abs(dt_req)>abs(4*dt):
                                dt_next = 4*dt
                                
                            else:
                                dt_next = dt_req
                                
                            t_next = (date + dt)/86400. #days
                            b_final = b_new
                            dt_final = dt
                            posvelacc = self.final_posvelacc(y0, yd0, ydd0, dt_final, b_final, date, local_frame)
                            rpt = 0
                            
                        elif abs(dt_req) < abs(dt/4):
                            dt = dt_req
                            b = b_init
                            g = b_init
                            til_del_b6_prev = 0.0
                            itr = 1
                        
                        else:
                            dt_next = dt_req
                            t_next = (date + dt)/86400. #days
                            b_final = b_new
                            dt_final = dt
                            posvelacc = self.final_posvelacc(y0, yd0, ydd0, dt_final, b_final, date, local_frame)
                            #print(dt)
                            rpt = 0
                    
                    else:
                        b = b_new
                        g = g_new
                        til_del_b6_prev = til_del_b6
                        itr = itr+1
                            
                else:
                    b = b_new
                    g = g_new
        
        if abs(dt_next/dt_final) < 20.:            
            err_b = b_final-b_predict
        else:
            err_b = np.zeros(b_final.shape)

        return posvelacc, t_next, dt_next, err_b, b_final