# This is currently not being used, it would be a cleaner way to write RK and RKF integrators, at the moment will propbably break T6 due to instability of RTC
# as referecd in documentation.

import numpy as np

import src.propagator.propagator_module as pg_m


class PropagatorRk(pg_m.Propagator):
    """Class devoting to the trajectory propation.

    This class inherits from class rk_module.Propagator.

    Attributes defined here:
    -h_min: step time minimum (in s)
    -h_max: step time maximum (in s)
    -tolerance: tolerance to be compared with estimating error
    -safty_factor: safety factor of the error evaluation.

    Methods define here:
    -propagate_traj(): computes the Runge-Kutta-felburg order 5 numerical integration method.

    propagate_traj input:
    -current date in second, in mjd format
    -position and velocity in m and m/s
    -time step in s
    
    Returns the time step (in seconds), position (in m), the velocity (in m/s),
    and the acceleration (in m/s²) using the previous one
    """

    def __init__(self, name, tensor_constructor, h_min, h_max, tolerance, safty_factor, initial_dt):
        """Constructor of class PropagatorRkf45."""
        pg_m.Propagator.__init__(self, tensor_constructor)
        self.initial_dt = initial_dt
        self.h_min = h_min
        self.h_max = h_max

        if h_min == h_max and initial_dt == h_max:
            self.propagate_traj = self.propagate_traj_step_size_fix
        else:
            self.propagate_traj = self.propagate_traj_step_size_control
        
        self.tolerance = tolerance
        self.safty_factor = safty_factor
        self.max_it = 50
        self.facmax = 4.
        self.facmin = 0.1
        self.name = name

        if self.name == 'rk4':
            self.coefficients_rk4()
            self.propagate_traj = self.propagate_traj_step_size_fix
        elif self.name == 'rkf45':
            self.coefficients_rkf45()
            self.trunc_order_inv = 1/5
        elif self.name == 'rkf56':
            self.coefficients_rkf56()
            self.trunc_order_inv = 1/6
        elif self.name == 'rkf78':
            self.coefficients_rkf78()
            self.trunc_order_inv = 1/8

        self.len_K = len(self.A)


    def coefficients_rk4(self):
        self.A = np.zeros((4,4))

        self.A[1,0] = 1 / 2

        self.A[2,1] = 1 / 2

        self.A[3,2] = 1

        self.C = np.zeros(4)
        self.C[1] = 1 / 2
        self.C[2] = 1 / 2
        self.C[3] = 1

        self.Flag = np.zeros(4)
        self.Flag[1] = 1
        self.Flag[2] = 1
        self.Flag[3] = 2

        self.B = np.zeros((1,6))
        self.B[0,0] = 1 / 6
        self.B[0,1] = 2 / 6
        self.B[0,2] = 2 / 6
        self.B[0,3] = 1 / 6


    def coefficients_rkf45(self):
        self.A = np.zeros((6,5))

        self.A[1,0] = 1 / 4

        self.A[2,0] = 3 / 32
        self.A[2,1] = 9 / 32

        self.A[3,0] = 1932 / 2197
        self.A[3,1] = -7200 / 2197
        self.A[3,2] = 7296 / 2197

        self.A[4,0] = 439 / 216
        self.A[4,1] = -8
        self.A[4,2] = 3680 / 513
        self.A[4,3] = -845 / 4104

        self.A[5,0] = -8 / 27
        self.A[5,1] = 2.
        self.A[5,2] = -3544 / 2565
        self.A[5,3] = 1859 / 4104
        self.A[5,4] = -11 / 40

        self.C = np.zeros(6)
        self.C[1] = 1 / 4
        self.C[2] = 3 / 8
        self.C[3] = 12 / 13
        self.C[4] = 1.
        self.C[5] = 1 / 2

        self.Flag = np.zeros(6)
        self.Flag[1] = 1
        self.Flag[2] = 1
        self.Flag[3] = 1
        self.Flag[4] = 2
        self.Flag[5] = 1

        self.B = np.zeros((2,6))
        self.B[0,0] = 25 / 216
        self.B[0,2] = 1408 / 2565
        self.B[0,3] = 2197 / 4104
        self.B[0,4] = -1 / 5

        self.B[1,0] = 16 / 135
        self.B[1,2] = 6656 / 12825
        self.B[1,3] = 28561 / 56430
        self.B[1,4] = -9 / 50
        self.B[1,5] = 2 / 55


    def coefficients_rkf56(self):
        self.A = np.zeros((8,7))

        self.A[1,0] = 1 / 6

        self.A[2,0] =  4 / 75
        self.A[2,1] = 16 / 75

        self.A[3,0] =  5 / 6
        self.A[3,1] = -8 / 3
        self.A[3,2] =  5 / 2

        self.A[4,0] =  -8 / 5
        self.A[4,1] = 144 / 25
        self.A[4,2] =  -4
        self.A[4,3] =  16 / 25

        self.A[5,0] = 361 / 320
        self.A[5,1] = -18 / 5
        self.A[5,2] = 407 / 128
        self.A[5,3] = -11 / 80
        self.A[5,4] =  55 / 128

        self.A[6,0] = -11 / 640
        self.A[6,2] =  11 / 256
        self.A[6,3] = -11 / 160
        self.A[6,4] =  11 / 256

        self.A[7,0] = 93 / 640
        self.A[7,1] = -18 / 5
        self.A[7,2] = 803 / 256
        self.A[7,3] = -11 / 160
        self.A[7,4] = 99 / 256
        self.A[7,6] = 1

        self.C = np.zeros(8)
        self.C[1] = 1 / 6
        self.C[2] = 4 / 15
        self.C[3] = 2 / 3
        self.C[4] = 4 / 5
        self.C[5] = 1
        self.C[7] = 1

        self.Flag = np.zeros(8)
        self.Flag[1] = 1
        self.Flag[2] = 1
        self.Flag[3] = 1
        self.Flag[4] = 1
        self.Flag[5] = 2
        self.Flag[7] = 2

        self.B = np.zeros((2,8))
        self.B[0,0] = 31 / 384
        self.B[0,2] = 1125 / 2816
        self.B[0,3] = 9 / 32
        self.B[0,4] = 125 / 768
        self.B[0,5] = 5 / 66

        self.B[1,0] = 7 / 1408
        self.B[1,2] = 1125 / 2816
        self.B[1,3] = 9 / 32
        self.B[1,4] = 125 / 768
        self.B[1,6] = 5 / 66
        self.B[1,7] = 5 / 66

  
    def coefficients_rkf78(self):
        self.A = np.zeros((13,12))

        self.A[1,0] = 2 / 27

        self.A[2,0] = 1 / 36
        self.A[2,1] = 1 / 12

        self.A[3,0] = 1 / 24
        self.A[3,2] = 1 / 8

        self.A[4,0] = 5 / 12
        self.A[4,2] = -25 / 16
        self.A[4,3] = 25 / 16

        self.A[5,0] = 1 / 20
        self.A[5,3] = 1 / 4
        self.A[5,4] = 1 / 5

        self.A[6,0] = -25 / 108
        self.A[6,3] = 125 / 108
        self.A[6,4] = -65 / 27
        self.A[6,5] = 125 / 54

        self.A[7,0] = 31 / 300
        self.A[7,4] = 61 / 225
        self.A[7,5] = -2 / 9
        self.A[7,6] = 13 / 900

        self.A[8,0] = 2
        self.A[8,3] = -53 / 6
        self.A[8,4] = 704 / 45
        self.A[8,5] = -107 / 9
        self.A[8,6] = 67 / 90
        self.A[8,7] = 3

        self.A[9,0] = -91 / 108
        self.A[9,3] = 23 / 108
        self.A[9,4] = -976 / 135
        self.A[9,5] = 311 / 54
        self.A[9,6] = -19 / 60
        self.A[9,7] = 17 / 6
        self.A[9,8] = -1 / 12

        self.A[10,0] = 2383 / 4100
        self.A[10,3] = -341 / 164
        self.A[10,4] = 4496 / 1025
        self.A[10,5] = -301 / 82
        self.A[10,6] = 2133 / 4100
        self.A[10,7] = 45 / 82
        self.A[10,8] = 45 / 164
        self.A[10,9] = 18 / 41

        self.A[11,0] = 3 / 205
        self.A[11,5] = -6 / 41
        self.A[11,6] = -3 / 205
        self.A[11,7] = -3 / 41
        self.A[11,8] = 3 / 41
        self.A[11,9] = 6 / 41

        self.A[12,0] = -1777 / 4100
        self.A[12,3] = -341 / 164
        self.A[12,4] = 4496 / 1025
        self.A[12,5] = -289 / 82
        self.A[12,6] = 2193 / 4100
        self.A[12,7] = 51 / 82
        self.A[12,8] = 33 / 164
        self.A[12,9] = 12 / 41
        self.A[12,11] = 1

        self.C = np.zeros(13)
        self.C[1] = 2 / 27
        self.C[2] = 1 / 9
        self.C[3] = 1 / 6
        self.C[4] = 5 / 12
        self.C[5] = 1 / 2
        self.C[6] = 5 / 6
        self.C[7] = 1 / 6
        self.C[8] = 2 / 3
        self.C[9] = 1 / 3
        self.C[10] = 1
        self.C[12] = 1

        self.Flag = np.zeros(13)
        self.Flag[1] = 1
        self.Flag[2] = 1
        self.Flag[3] = 1
        self.Flag[4] = 1
        self.Flag[5] = 1
        self.Flag[6] = 1
        self.Flag[7] = 1
        self.Flag[8] = 1
        self.Flag[9] = 1
        self.Flag[10] = 2
        self.Flag[12] = 2

        self.B = np.zeros((2,13))
        self.B[0,0] = 41 / 840
        self.B[0,5] = 34 / 105
        self.B[0,6] = 9 / 35
        self.B[0,7] = 9 / 35
        self.B[0,8] = 9 / 280
        self.B[0,9] = 9 / 280
        self.B[0,10] = 41 / 840

        self.B[1,5] = 34 / 105
        self.B[1,6] = 9 / 35
        self.B[1,7] = 9 / 35
        self.B[1,8] = 9 / 280
        self.B[1,9] = 9 / 280
        self.B[1,11] = 41 / 840
        self.B[1,12] = 41 / 840


    def compute_not_embedded_states(self, date, pos_vel, h, local_frame):
        K = np.zeros((self.len_K,6))
        next_pos_vel = pos_vel.copy()

        for i in range(self.len_K):
            pos_vel_add = pos_vel.copy()
            for ii in range(i):
                pos_vel_add += self.A[i,ii] * K[ii]
            K[i] = h * self.rate_function(date + self.C[i] * h, pos_vel_add, self.Flag[i], local_frame)
            next_pos_vel += self.B[0,i] * K[i]
        
        return next_pos_vel, None
    

    def compute_embedded_states(self, date, pos_vel, h, local_frame):
        K = np.zeros((self.len_K,6))
        next_pos_vel = pos_vel.copy()
        tild_pos_vel = pos_vel.copy()

        for i in range(self.len_K):
            pos_vel_add = pos_vel.copy()
            for ii in range(i):
                pos_vel_add += self.A[i,ii] * K[ii]
            K[i] = h * self.rate_function(date + self.C[i] * h, pos_vel_add, self.Flag[i], local_frame)
            next_pos_vel += self.B[0,i] * K[i]
            tild_pos_vel += self.B[1,i] * K[i]
        
        return next_pos_vel, tild_pos_vel


    def compute_low_high_states_rkf56(self, date, pos_vel, h, local_frame):
        flag = 0
        k1 = h * self.rate_function(date, pos_vel, 
                                    flag, local_frame)
        flag = 1
        k2 = h * self.rate_function(date + h / 6, 
                                    pos_vel + 1 / 6 * k1, 
                                    flag, local_frame)
        
        k3 = h * self.rate_function(date + 4 / 15 * h, 
                                    pos_vel + 4 / 75 * k1 + 16 / 75 * k2, 
                                    flag, local_frame)
        
        k4 = h * self.rate_function(date + 2 / 3 * h,
                                    pos_vel + 5 / 6 * k1 - 8 / 3 * k2 + 5 / 2 * k3, 
                                    flag, local_frame)
        
        k5 = h * self.rate_function(date + 4 / 5 * h, 
                                    pos_vel - 8 / 5 * k1 + 144 / 25 * k2 - 4 * k3 + 16 / 25 * k4,
                                    flag, local_frame)
        flag = 2
        k6 = h * self.rate_function(date + h,
                                    pos_vel + 361 / 320 * k1 - 18 / 5 * k2 + 407 / 128 * k3 - 11 / 80 * k4 + 55 / 128 * k5,
                                    flag, local_frame)
        flag = 0
        k7 = h * self.rate_function(date, 
                                    pos_vel - 11 / 640 * k1 + 11 / 256 * k3 - 11 / 160 * k4 + 11 / 256 * k5,
                                    flag, local_frame)
        flag = 2
        k8 = h * self.rate_function(date + h,
                                    pos_vel + 93 / 640 * k1 - 18 / 5 * k2 + 803 / 256 * k3 - 11 / 160 * k4 + 99 / 256 * k5 + k7,
                                    flag, local_frame)
        
        next_pos_vel = pos_vel + (31 / 384 * k1 + 1125 / 2816 * k3 + 9 / 32 * k4 + 125 / 768 * k5 + 5 / 66 * k6) # 5th order
        tild_pos_vel = pos_vel + (7 / 1408 * k1 + 1125 / 2816 * k3 + 9 / 32 * k4 + 125 / 768 * k5 + 5 / 66 * k7 + 5 / 66 * k8) # 6th order

        return next_pos_vel, tild_pos_vel
    

    def propagate_traj_step_size_control(self, date, pos_vel, h_current, local_frame, prop_dt_min, force):
        """Complete the Runge-Kutta-Fehlberg numerical integration method 
        by using an adaptive step time control during the computation.
        The parameter h, which is the variable time step of the simulation, is expressed in seconds.
        The Step-SIze Control Algorithm used here is: RSStep, and therefore, the relative error is with respect to
        the current step (the variation of the state due to the current step), followed by Root Sum Square (RSS).
        It uses Highest Order Computation
        """

        if prop_dt_min==1:
            h_min = h_current
        else:
            h_min = self.h_min

        if force:
            low_pos_vel, high_pos_vel = self.compute_embedded_states(date, pos_vel, h_current, local_frame)
            trunc_error = np.abs(high_pos_vel -  low_pos_vel)
            trunc_error[:3] /= np.linalg.norm(high_pos_vel[:3] - pos_vel[:3])
            trunc_error[3:] /= np.linalg.norm(high_pos_vel[3:] - pos_vel[3:])
            tol_error = self.tolerance
            max_err = np.linalg.norm(trunc_error) / tol_error
            delta = self.safty_factor * (1/max_err) ** self.trunc_order_inv
            h_next = h_current * min( self.facmax, max( self.facmin, delta ) )
            if abs(h_next) < abs(h_min):
                h_next = h_min
            elif abs(h_next) > abs(self.h_max):
                h_next = self.h_max
        
        else:
            active_loop = True
            it = 0
            h_prev = h_current
            while active_loop and it < self.max_it:
                # if it == 1: # if use this change to self and dont forget to reset self. after each loop
                #     facmax = 1
                low_pos_vel, high_pos_vel = self.compute_embedded_states(date, pos_vel, h_current, local_frame)
                trunc_error = np.abs(high_pos_vel -  low_pos_vel)
                trunc_error[:3] /= np.linalg.norm(high_pos_vel[:3] - pos_vel[:3])
                trunc_error[3:] /= np.linalg.norm(high_pos_vel[3:] - pos_vel[3:])
                tol_error = self.tolerance
                max_err = np.linalg.norm(trunc_error) / tol_error
                delta = self.safty_factor * (1/max_err) ** self.trunc_order_inv
                h_next = h_current * min( self.facmax, max( self.facmin, delta ) )

                if abs(h_next) < abs(h_min):
                    h_next = h_min
                elif abs(h_next) > abs(self.h_max):
                    h_next = self.h_max
                
                if max_err <= 1:
                    active_loop = False
                
                elif h_current == h_min or h_current/h_prev == self.facmin:
                    active_loop = False

                else:
                    h_prev = h_current
                    h_current = h_next

                it += 1
        
        flag = 2
        vel_acc = self.rate_function(date + h_current, high_pos_vel, flag, local_frame)
            
        return h_next, np.append(high_pos_vel, vel_acc[3:6]), h_current
    

    def propagate_traj_step_size_fix(self, date, pos_vel, h_current, local_frame, prop_dt_min, force):
        """Algorithm for fixed step size, (hmin=hmax).
           It uses Lowest Order Computation.
        """

        low_pos_vel, _ = self.compute_not_embedded_states( date, pos_vel, h_current, local_frame)     
        flag = 2
        vel_acc = self.rate_function(date + h_current, low_pos_vel, flag, local_frame)
            
        return self.initial_dt, np.append(low_pos_vel, vel_acc[3:6]), h_current





