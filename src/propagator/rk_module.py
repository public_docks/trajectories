# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 26/04/2024
#	Program  	: DOCKS Propagator
#	Name 		: rk_module.py.py
#	Authors		: Nima TRAORE ; Florian JOUSSEAUME; Laëtitia LEBEC; Rashika JAIN; Harshul SHARMA; Tomas NUNES
#	Version 	: 5.0
######################################################################

import numpy as np

import src.propagator.propagator_module as pg_m


class PropagatorRk(pg_m.Propagator):
    """Class devoting to the trajectory propation.

    This class inherits from class propagator_module.Propagator.

    Attributes defined here:
    -h_min: step time minimum (in s)
    -h_max: step time maximum (in s)
    -tolerance: tolerance to be compared with estimating error
    -safty_factor: safety factor of the error evaluation.
    -initial_dt: initial time step (in s)

    Methods define here:
    -propagate_traj(): computes the Runge-Kutta-felburg order 5 numerical integration method.
    -propagate_traj_step_size_fix(): computes the Runge-Kuttas fix step size
    -propagate_traj_step_size_control(): computes the Runge-Kuttas adaptive step size

    propagate_traj input:
    -current date in second, in mjd format
    -position and velocity in m and m/s
    -time step in s
    
    Returns the time step (in seconds), position (in m), the velocity (in m/s),
    and the acceleration (in m/s²) using the previous one
    """

    def __init__(self, name, tensor_constructor, h_min, h_max, tolerance, safty_factor, initial_dt):
        """Constructor of class PropagatorRkf45."""
        pg_m.Propagator.__init__(self, tensor_constructor)
        self.initial_dt = initial_dt
        self.h_min = h_min
        self.h_max = h_max

        if h_min == h_max and initial_dt == h_max:
            self.propagate_traj = self.propagate_traj_step_size_fix
        else:
            self.propagate_traj = self.propagate_traj_step_size_control

        self.tolerance = tolerance
        self.safty_factor = safty_factor
        self.max_it = 50
        self.facmax = 4.
        self.facmin = 0.1
        self.name = name

        if self.name == 'rk4':
            self.compute_low_high_states = self.compute_low_high_states_rk4
            self.propagate_traj = self.propagate_traj_step_size_fix
        elif self.name == 'rkf45':
            self.compute_low_high_states = self.compute_low_high_states_rkf45
            self.trunc_order_inv = 1/5
        elif self.name == 'rkf56':
            self.compute_low_high_states = self.compute_low_high_states_rkf56
            self.trunc_order_inv = 1/6
        elif self.name == 'rkf78':
            self.compute_low_high_states = self.compute_low_high_states_rkf78
            self.trunc_order_inv = 1/8
    

    def compute_low_high_states_rk4(self, date, pos_vel, h, local_frame):
        flag = 0
        k1 = h * self.rate_function(date, pos_vel, flag, local_frame)
        flag = 1
        k2 = h * self.rate_function(date + h / 2, pos_vel + k1 / 2, flag, local_frame)
        k3 = h * self.rate_function(date + h / 2, pos_vel + k2 / 2, flag, local_frame)
        flag = 2
        k4 = h * self.rate_function(date + h, pos_vel + k3, flag, local_frame)
        next_pos_vel = pos_vel + (k1 + 2 * k2 + 2 * k3 + k4) / 6

        return next_pos_vel, None


    def compute_low_high_states_rkf45(self, date, pos_vel, h, local_frame):
        flag = 0
        k1 = h * self.rate_function(date, pos_vel, flag, local_frame)
        flag = 1
        k2 = h * self.rate_function(date + h / 4, pos_vel + k1 / 4, flag, local_frame)
        k3 = h * self.rate_function(date + 3 / 8 * h, pos_vel + 3 / 32 * k1 + 9 / 32 * k2, flag, local_frame)
        k4 = h * self.rate_function(date + 12 / 13 * h,
                                    pos_vel + 1932 / 2197 * k1 - 7200 / 2197 * k2 + 7296 / 2197 * k3, flag, local_frame)
        flag = 2
        k5 = h * self.rate_function(date + h, pos_vel + 439 / 216 * k1 - 8 * k2 + 3680 / 513 * k3 - 845 / 4104 * k4,
                                    flag, local_frame)
        flag = 1
        k6 = h * self.rate_function(date + 1 / 2 * h,
                                    pos_vel - 8 / 27 * k1 + 2 * k2 - 3544 / 2565 * k3 + 1859 / 4104 * k4 - 11 / 40 * k5,
                                    flag, local_frame)
        next_pos_vel = pos_vel + (25 / 216 * k1 + 1408 / 2565 * k3 + 2197 / 4104 * k4 - 1 / 5 * k5) # 4th order
        tild_pos_vel = pos_vel + (16 / 135 * k1 + 6656 / 12825 * k3 + 28561 / 56430 * k4 - 9 / 50 * k5 + 2 / 55 * k6) # 5th order

        return next_pos_vel, tild_pos_vel


    def compute_low_high_states_rkf56(self, date, pos_vel, h, local_frame):
        flag = 0
        k1 = h * self.rate_function(date, pos_vel, 
                                    flag, local_frame)
        flag = 1
        k2 = h * self.rate_function(date + h / 6, 
                                    pos_vel + 1 / 6 * k1, 
                                    flag, local_frame)
        
        k3 = h * self.rate_function(date + 4 / 15 * h, 
                                    pos_vel + 4 / 75 * k1 + 16 / 75 * k2, 
                                    flag, local_frame)
        
        k4 = h * self.rate_function(date + 2 / 3 * h,
                                    pos_vel + 5 / 6 * k1 - 8 / 3 * k2 + 5 / 2 * k3, 
                                    flag, local_frame)
        
        k5 = h * self.rate_function(date + 4 / 5 * h, 
                                    pos_vel - 8 / 5 * k1 + 144 / 25 * k2 - 4 * k3 + 16 / 25 * k4,
                                    flag, local_frame)
        flag = 2
        k6 = h * self.rate_function(date + h,
                                    pos_vel + 361 / 320 * k1 - 18 / 5 * k2 + 407 / 128 * k3 - 11 / 80 * k4 + 55 / 128 * k5,
                                    flag, local_frame)
        flag = 0
        k7 = h * self.rate_function(date, 
                                    pos_vel - 11 / 640 * k1 + 11 / 256 * k3 - 11 / 160 * k4 + 11 / 256 * k5,
                                    flag, local_frame)
        flag = 2
        k8 = h * self.rate_function(date + h,
                                    pos_vel + 93 / 640 * k1 - 18 / 5 * k2 + 803 / 256 * k3 - 11 / 160 * k4 + 99 / 256 * k5 + k7,
                                    flag, local_frame)
        
        next_pos_vel = pos_vel + (31 / 384 * k1 + 1125 / 2816 * k3 + 9 / 32 * k4 + 125 / 768 * k5 + 5 / 66 * k6) # 5th order
        tild_pos_vel = pos_vel + (7 / 1408 * k1 + 1125 / 2816 * k3 + 9 / 32 * k4 + 125 / 768 * k5 + 5 / 66 * k7 + 5 / 66 * k8) # 6th order

        return next_pos_vel, tild_pos_vel
    

    def compute_low_high_states_rkf78(self, date, pos_vel, h, local_frame):
        flag = 0
        k1 = h * self.rate_function(date, pos_vel, 
                                    flag, local_frame)
        flag = 1
        k2 = h * self.rate_function(date + 2/27 * h, 
                                    pos_vel + 2/27*k1, 
                                    flag, local_frame)
        
        k3 = h * self.rate_function(date + 1/9 * h, 
                                    pos_vel + 1/36*k1 + 1/12*k2, 
                                    flag, local_frame)
        
        k4 = h * self.rate_function(date + 1/6 * h,
                                    pos_vel + 1/24*k1 + 1/8*k3, 
                                    flag, local_frame)
        
        k5 = h * self.rate_function(date + 5/12 * h, 
                                    pos_vel + 5/12*k1 - 25/16*k3 + 25/16*k4,
                                    flag, local_frame)
        
        k6 = h * self.rate_function(date + 1/2 * h,
                                    pos_vel + 1/20*k1 + 1/4*k4 + 1/5*k5,
                                    flag, local_frame)
        
        k7 = h * self.rate_function(date + 5/6 * h, 
                                    pos_vel - 25/108*k1 + 125/108*k4 - 65/27*k5 + 125/54*k6,
                                    flag, local_frame)
        
        k8 = h * self.rate_function(date + 1/6 * h,
                                    pos_vel + 31/300*k1 + 61/225*k5 - 2/9*k6 + 13/900*k7,
                                    flag, local_frame)
        
        k9 = h * self.rate_function(date + 2/3 * h,
                                    pos_vel + 2*k1 - 53/6*k4 + 704/45*k5 - 107/9*k6 + 67/90*k7 + 3*k8,
                                    flag, local_frame)
        
        k10 = h * self.rate_function(date + 1/3 * h,
                                    pos_vel - 91/108*k1 + 23/108*k4 - 976/135*k5 + 311/54*k6 - 19/60*k7 + 17/6*k8 - 1/12*k9,
                                    flag, local_frame)
        
        flag = 2
        k11 = h * self.rate_function(date + 1 * h,
                                    pos_vel + 2383/4100*k1 - 341/164*k4 + 4496/1025*k5 - 301/82*k6 + 2133/4100*k7 + 45/82*k8 + 45/164*k9 + 18/41*k10,
                                    flag, local_frame)
        
        flag = 0
        k12 = h * self.rate_function(date,
                                    pos_vel + 3/205*k1 - 6/41*k6 - 3/205*k7 - 3/41*k8 + 3/41*k9 + 6/41*k10,
                                    flag, local_frame)
        
        flag = 2
        k13 = h * self.rate_function(date + 1 * h,
                                    pos_vel - 1777/4100*k1 - 341/164*k4 + 4496/1025*k5 - 289/82*k6 + 2193/4100*k7 + 51/82*k8 + 33/164*k9 + 12/41*k10 + 1*k12,
                                    flag, local_frame)
        
        next_pos_vel = pos_vel + (41/840*k1 + 34/105*k6 + 9/35*k7 + 9/35*k8 + 9/280*k9 + 9/280*k10 + 41/840*k11) # 7th order
        tild_pos_vel = pos_vel + (34/105*k6 + 9/35*k7 + 9/35*k8 + 9/280*k9 + 9/280*k10 + 41/840*k12 + 41/840*k13) # 8th order
        

        return next_pos_vel, tild_pos_vel
    


    def propagate_traj_step_size_fix(self, date, pos_vel, h_current, local_frame, prop_dt_min, force):
        """Algorithm for fixed step size, (hmin=hmax).
           It uses Lowest Order Computation.
        """

        low_pos_vel, _ = self.compute_low_high_states( date, pos_vel, h_current, local_frame)     
        flag = 2
        vel_acc = self.rate_function(date + h_current, low_pos_vel, flag, local_frame)
            
        return self.initial_dt, np.append(low_pos_vel, vel_acc[3:6]), h_current


    def propagate_traj_step_size_control(self, date, pos_vel, h_current, local_frame, prop_dt_min, force):
        """Complete the Runge-Kutta-Fehlberg numerical integration method 
           by using an adaptive step time control during the computation.
           The parameter h, which is the variable time step of the simulation, is expressed in seconds.
           The Step-SIze Control Algorithm used here is: RSStep, and therefore, the relative error is with respect to
           the current step (the variation of the state due to the current step), followed by Root Sum Square (RSS).
           It uses Highest Order Computation
        """

        if prop_dt_min==1:
            h_min = h_current
        else:
            h_min = self.h_min

        if force:
            low_pos_vel, high_pos_vel = self.compute_low_high_states(date, pos_vel, h_current, local_frame)
            trunc_error = np.abs(high_pos_vel -  low_pos_vel)
            trunc_error[:3] /= np.linalg.norm(high_pos_vel[:3] - pos_vel[:3])
            trunc_error[3:] /= np.linalg.norm(high_pos_vel[3:] - pos_vel[3:])
            tol_error = self.tolerance
            max_err = np.linalg.norm(trunc_error) / tol_error
            delta = self.safty_factor * (1/max_err) ** self.trunc_order_inv
            h_next = h_current * min( self.facmax, max( self.facmin, delta ) )
            if abs(h_next) < abs(h_min):
                h_next = h_min
            elif abs(h_next) > abs(self.h_max):
                h_next = self.h_max
        
        else:
            active_loop = True
            it = 0
            h_prev = h_current
            while active_loop and it < self.max_it:
                # if it == 1: # if use this change to self and dont forget to reset self. after each loop
                #     facmax = 1
                low_pos_vel, high_pos_vel = self.compute_low_high_states(date, pos_vel, h_current, local_frame)
                trunc_error = np.abs(high_pos_vel -  low_pos_vel)
                trunc_error[:3] /= np.linalg.norm(high_pos_vel[:3] - pos_vel[:3])
                trunc_error[3:] /= np.linalg.norm(high_pos_vel[3:] - pos_vel[3:])
                tol_error = self.tolerance
                max_err = np.linalg.norm(trunc_error) / tol_error
                delta = self.safty_factor * (1/max_err) ** self.trunc_order_inv
                h_next = h_current * min( self.facmax, max( self.facmin, delta ) )

                if abs(h_next) < abs(h_min):
                    h_next = h_min
                elif abs(h_next) > abs(self.h_max):
                    h_next = self.h_max
                
                if max_err <= 1:
                    active_loop = False
                
                elif h_current == h_min or h_current/h_prev == self.facmin:
                    active_loop = False

                else:
                    h_prev = h_current
                    h_current = h_next

                it += 1
        
        flag = 2
        vel_acc = self.rate_function(date + h_current, high_pos_vel, flag, local_frame)
            
        return h_next, np.append(high_pos_vel, vel_acc[3:6]), h_current
    







