# -*- coding: utf-8 -*-
######################################################################
#	Modified date	: 28/09/2021
#	Program  	: DOCKS Propagator
#	Name 		: master_time_manager.py
#	Authors		: Harshul SHARMA
#	Version 	: 1.0
######################################################################

import numpy as np
 
class master_time_manager:
    def __init__(self, t_start, t_end, t_dt, sign):
        t_start = list(np.float_(t_start))
        self.t_end = list(np.float_(t_end))
        self.t_dt = t_dt
        self.sign = sign
        self.t_all = t_start + self.t_end
        self.t_all.sort(reverse=(self.sign==-1.0))
        
    def give_next_date(self, date, time_step):
        l = len(self.t_all)
        new_date = None

        for ind in range(l):
            if (self.sign*date) < (self.sign*self.t_all[ind]) <= (self.sign*(date + time_step)):
                new_date = self.t_all[ind]
                break
            else:
                continue
        return new_date

    def min_dt_check(self, min_dt):
        if abs(min_dt)<=abs(min(self.t_dt, key=abs)):
            return min_dt
        else:
            return min(self.t_dt, key=abs)

    def next_dt(self, date, time_step, dt_min=0):
        next_date = self.give_next_date(date, time_step)

        if next_date!=None:
            new_dt = next_date - date
            if abs(new_dt) < abs(dt_min):
                prop_dt_min = 1
            else:
                prop_dt_min = 0
                
            if next_date in self.t_end:
                prop_end = True
            else:
                prop_end = False
            return new_dt, prop_dt_min, prop_end
        else:
            prop_dt_min = 0
            prop_end = False
            return time_step, prop_dt_min, prop_end