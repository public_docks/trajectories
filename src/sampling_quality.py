# -*- coding: utf-8 -*-
###########################################################################
#	Modified date	: 26/06/2019
#	Program  	: DOCKS Propagator
#	Name 		: sampling_quality.py
#	Authors		: Rashika JAIN
#	Version 	: 1
###########################################################################

# Propagator quality file.
# This file describes the quality of the trajectory computed by the Propagator.

import matplotlib as mpl
import numpy as np
import scipy
from matplotlib import pyplot as plt
from scipy.interpolate import splev, splrep, interp1d

import ccsds as cc
from src import config


def sampling_quality():
    """ Method for plotting the sampling quality of the Propagator and saving as png image"""

    # location of trajectory file
    prop_traj_file = config.output_dir + '/' + config.output_file

    # reading the trajectory file
    _, _, _, header, data_time, data_science = cc.read_ccsds(prop_traj_file)

    # generating time vector
    current_time = [data_time[:, 0] * 86400 + data_time[:, 1]]  # s
    current_time = np.array(current_time).transpose()

    vect_t_num = []
    time_scale = []
    for i in range(len(current_time)):
        vect_t_num.append((data_time[i, 0] + 2400000.5 + data_time[i, 1] / 86400) * 86400)  # time vector (s)
        time_scale.append((vect_t_num[i] - vect_t_num[0]) / 86400 / 365)
    time_scale = np.array(time_scale)

    # plotting the trajectory
    print("\nPlotting Trajectory")
    fig1 = plt.figure(num=1, figsize=(9, 5))
    bx = fig1.add_subplot(111, projection='3d')
    pnt3d = bx.scatter(data_science[:, 0], data_science[:, 1], data_science[:, 2], c=time_scale)
    bx.set_title('Propagator3D', fontweight='bold')
    bx.set_xlabel('x(km)', fontweight='bold', labelpad=14)
    bx.set_ylabel('y(km)', fontweight='bold', labelpad=14)
    bx.set_zlabel('z(km)', fontweight='bold', labelpad=14)
    cbar = plt.colorbar(pnt3d)
    cbar.set_label("Time(y)", fontweight='bold')
    fig1.savefig(config.output_dir + "/" + config.output_file[0:-4] + ".png")

    # sorting in increasing order for interpolation
    data = np.concatenate([current_time, data_time, data_science], axis=1)
    data = np.array(sorted(data, key=lambda x: x[0]))
    data_science = data[:, 3:]
    data_time = data[:, 1:3]
    current_time = data[:, 0]

    vect_t_num = []
    time_scale = []
    for i in range(len(current_time)):
        vect_t_num.append((data_time[i, 0] + 2400000.5 + data_time[i, 1] / 86400) * 86400)  # time vector (s)
        time_scale.append((vect_t_num[i] - vect_t_num[0]) / 86400 / 365)
    time_scale = np.array(time_scale)

    if len(current_time) >= 10:
        print("Analysing sampling quality of the obtained trajectory")

        # interpolating the points to get a curve of slopes of tangents
        tx = splrep(current_time, data_science[:, 0])
        fx = splev(current_time, tx, der=0)
        fx_ft = splev(current_time, tx, der=1)

        ty = splrep(current_time, data_science[:, 1])
        fy = splev(current_time, ty, der=0)
        fy_ft = splev(current_time, ty, der=1)

        tz = splrep(current_time, data_science[:, 2])
        fz = splev(current_time, tz, der=0)
        fz_ft = splev(current_time, tz, der=1)

        # calculating the intersection points of two consecutive tangents by least square method
        b = [[fx[1:] - fx[0:-1]], [fy[1:] - fy[0:-1]], [fz[1:] - fz[0:-1]]]
        b = np.array(b)

        A = [[fx_ft[0:-1], -fx_ft[1:]], [fy_ft[0:-1], -fy_ft[1:]], [fz_ft[0:-1], -fz_ft[1:]]]
        A = np.array(A)

        t = []
        for i in range(len(current_time) - 1):
            t.append(np.linalg.lstsq(A[:, :, i], b[:, :, i], rcond=None)[0].transpose())
        t = np.array(t).reshape(-1, 2)

        # intersection points of tangents
        x_intersect = fx_ft[0:-1] * t[:, 0] + fx[0:-1]
        y_intersect = fy_ft[0:-1] * t[:, 0] + fy[0:-1]
        z_intersect = fz_ft[0:-1] * t[:, 0] + fz[0:-1]

        intersection = np.array([x_intersect, y_intersect, z_intersect]).transpose()

        # calculating distance between above intersections and propagated curve
        data_pos = np.array([data_science[:, 0], data_science[:, 1], data_science[:, 2]])

        # cubic interpolation
        fr = interp1d(current_time, data_pos, kind='cubic', fill_value="extrapolate")

        # linear interpolation
        fr_linear = interp1d(current_time, data_pos, kind='linear', fill_value="extrapolate")

        # distances
        d_cubic = np.linalg.norm(fr(current_time[0:-1] + t[:, 0]).transpose() - intersection, axis=1)
        d_linear = np.linalg.norm(fr_linear(current_time[0:-1] + t[:, 0]).transpose() - intersection, axis=1)
        d_cub_lin = d_linear - d_cubic

        lin_res = d_linear
        cub_res = np.amax([d_cubic, d_cub_lin], axis=0)

        v = data_science[:, 3:]
        angle = np.degrees(np.arccos(np.sum(v[0:-1] * v[1:], axis=1) /
                                     (np.linalg.norm(v[0:-1], axis=1) * np.linalg.norm(v[1:], axis=1)))) * 3600

        # rate of change of angle between tangents deg/s with time (years)

        radius = scipy.linalg.norm([data_science[:, 0], data_science[:, 1], data_science[:, 2]], axis=0)
        dr = np.linalg.norm(data_pos[:, 1:] - data_pos[:, 0:-1], axis=0)
        dr_radius = radius[1:] - radius[0:-1]

        # plotting the results
        mpl.rcParams['axes.linewidth'] = 2
        font = {
            'weight': 'bold',
            'size': 15}
        mpl.rc('font', **font)

        def calculate_ticks(ax, ticks, round_to=0.01, center=False):
            upperbound = np.ceil(ax.get_ybound()[1] / round_to)
            lowerbound = np.floor(ax.get_ybound()[0] / round_to)
            dy = upperbound - lowerbound
            fit = np.floor(dy / (ticks - 1)) + 1
            dy_new = (ticks - 1) * fit
            if center:
                offset = np.floor((dy_new - dy) / 2)
                lowerbound = lowerbound - offset
            values = np.linspace(lowerbound, lowerbound + dy_new, ticks)
            return values[1:-1] * round_to

        color = "#1f77b4"
        fig2 = plt.figure(num=2, figsize=(13, 9))
        ax1 = fig2.add_subplot(311)
        ax1.plot(time_scale[1:], cub_res, linewidth=2)
        ax2 = fig2.add_subplot(311, sharex=ax1, frameon=False)
        ax2.plot(time_scale[1:], lin_res, color=[0.8500, 0.3250, 0.0980], linewidth=2)
        ax1.set_ylabel("Maximum cubic " "\n" r" residual (km)", fontweight='bold')
        ax1.tick_params(axis='y', colors=color)
        ax1.yaxis.label.set_color(color)
        ax1.set_ylim(min(cub_res) - 0.1, max(cub_res) + 0.2)
        ax1.set_yticks(calculate_ticks(ax1, 5, center=True))
        ax2.yaxis.tick_right()
        ax2.yaxis.set_label_position("right")
        ax2.set_ylim(min(lin_res) - 0.3, max(lin_res) + 0.1)
        ax2.set_ylabel("Maximum linear " "\n" r" residual (km)", fontweight='bold')
        ax2.yaxis.label.set_color([0.8500, 0.3250, 0.0980])
        ax2.set_yticks(calculate_ticks(ax2, 5, center=True))
        ax2.tick_params(axis='y', colors=[0.8500, 0.3250, 0.0980])
        ax1.spines['left'].set_color(color)
        ax1.spines['right'].set_color([0.8500, 0.3250, 0.0980])
        plt.grid(True)
        # ----
        ax3 = fig2.add_subplot(312, sharex=ax1)
        ax3.plot(time_scale[1:], dr, linewidth=2)
        ax3.set_ylabel("Interstep -" "\n" r"distance (km)", fontweight='bold')
        ax3.tick_params(axis='y', colors=color)
        ax3.ticklabel_format(axis='y', style='sci')
        ax3.yaxis.major.formatter.set_powerlimits((0, 0))
        ax3.set_yticks(calculate_ticks(ax3, 5))
        ax3.yaxis.label.set_color(color)
        ax4 = fig2.add_subplot(312, sharex=ax1, frameon=False)
        ax4.plot(time_scale[1:], dr_radius, color=[0.8500, 0.3250, 0.0980], linewidth=2)
        ax4.yaxis.tick_right()
        ax4.yaxis.set_label_position("right")
        ax4.set_ylabel("Radius" "\n" r" difference (km)   ", fontweight='bold')
        ax4.yaxis.label.set_color([0.8500, 0.3250, 0.0980])
        ax4.set_yticks(calculate_ticks(ax4, 5))
        ax4.tick_params(axis='y', colors=[0.8500, 0.3250, 0.0980])
        ax3.spines['left'].set_color(color)
        ax3.spines['right'].set_color([0.8500, 0.3250, 0.0980])
        plt.grid(True)
        # ----
        ax5 = fig2.add_subplot(313, sharex=ax1)
        ax5.plot(time_scale[1:], angle, linewidth=2)
        ax5.set_ylabel("Change of " "\n" r"direction (arcsec)", fontweight='bold')
        ax5.set_xlabel("Time (y)", fontweight='bold')
        ax5.yaxis.label.set_color(color)
        ax5.tick_params(axis='y', colors=color)
        ax5.spines['left'].set_color(color)
        plt.grid(True)
        plt.tight_layout()
        # plt.suptitle("Sampling Quality", fontweight='bold')
        fig2.align_ylabels([ax1, ax3, ax5])
        fig2.align_ylabels([ax2, ax4])
        fig2.savefig(config.output_dir + "/" + config.output_file[0:-4] + "_samp_qual.png")
    plt.show()
