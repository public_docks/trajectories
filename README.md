![image alt ><](./icons/DOCKS_icon.png)
<h1 style="text-align: center;">DOCKS Propagator

version 6.5.0</h1>

(readme last modification : *31-May-2024*)

**New features**:
- integrator RKF4(5) step-size control algorithm validated
- new integration methods added [RKF5(6), RKF7(8)]
- integrator IAS15 tolerance fixed
- rotation matrices interpolation fixed
- acceptance of rotation matrices from SPICE
- SRP including eclipse conditions: Penumbra and Umbra
- selection of central body of propagation
- general decrease of computation time.

**Limitation**: continuous propulsion was disabled (bug).

# Authors

Development by PSL Space Pole [CENSUS](https://census.psl.eu/?lang=en), Observatory of Paris - PSL Université

Past contributions: Audrey PORQUET (2014), Hao-Chih Jim LIN (2015), Nima TRAORE (2016), Florian JOUSSEAUME (2017), Laetitia LEBEC (2018), Sebastien DURAND (2019), Harshul SHARMA (2021), Rashika JAIN (2022), Tomás NUNES (2023), Boris SEGRET (2024)

Special thanks: Daniel Hestroffer (IMCCE), Emmanuel Grolleau (LESIA), LeeRoy Malac-Allain (LESIA), Laboratory of Excellence ESEP.

&nbsp;

----
<!-- TOC -->

- [Authors](#authors)
- [1. Features of DOCKS Propagator](#1-features-of-docks-propagator)
- [2. Installing DOCKS Propagator module](#2-installing-docks-propagator-module)
    - [2.1 Linux Users](#21-linux-users)
        - [2.1.1 With Miniconda/Anaconda](#211-with-minicondaanaconda)
        - [2.1.2 Without Miniconda/Anaconda](#212-without-minicondaanaconda)
    - [2.2 MS-Windows Users](#22-ms-windows-users)
        - [2.2.1 With Miniconda/Anaconda](#221-with-minicondaanaconda)
        - [2.2.2 Without Miniconda/Anaconda](#222-without-minicondaanaconda)
    - [2.3 Use of VTS displayer](#23-use-of-vts-displayer)
- [3. Use DOCKS Propagator](#3-use-docks-propagator)
- [4. Test DOCKS Propagator](#4-test-docks-propagator)

<!-- /TOC -->
----

&nbsp;

# 1. Features of DOCKS Propagator

DOCKS Propagator is a module in DOCKS open-source suite for early prototyping of a space mission profile. It is a numeric integrator and propagator of a body's trajectory. The environment of perturbations is parametrized and can include:

* Celestial bodies as point masses (identified by their mass and sampled ephemeris or NAIF ID)
* Celestial bodies as complex gravitational bodies (identified by Spherical Harmonics and sampled ephemeris or NAIF ID)
* Solar Radiation Pressure
* Continuous Propulsion Corrections (currently disabled)

It allows fowrward and backward propagation, and includes the following integrators: 

* RK4: classical Runge Kutta method of order 4 (Fix Step Size).
* RKF45: adaptive time-step Runge Kutta Fehlberg method of order 4 with embedded method of order 5.
* RKF56: adaptive time-step Runge Kutta Fehlberg method of order 5 with embedded method of order 6.
* RKF78: adaptive time-step Runge Kutta Fehlberg method of order 7 with embedded method of order 8.
* IAS15: adaptive time step 15th order method with high accuracy (Variable Step Size).

&nbsp;



# 2. Installing DOCKS Propagator module

**System requirements: 64-bits system**

**Python requirements: 3.9+ 64-bits**

*__Note__: If you want to install DOCKS and already have a previous version of DOCKS, do the section 2.1 or 2.2 even if you did it previously.

1. **These downloads are required with DOCKS Propagator:**
	* *docks_tools module*, [this CENSUS link](https://share.obspm.fr/s/yzKr2z8oWWsnDTL/download/*)
	* *spice kernels*, [this CENSUS link](https://share.obspm.fr/s/GwcRbYJHm4qJWKD/download/*). It is possible to use your own *kernels* directory if available. However, in order to run the examples from [SECTION EXAMPLE] the provided *kernels* are needed. These contain:
		- Sun + Mercury + Venus + Earth + Moon + all planetary barycenters, for time frame from 1550 to 2650, using DE440.
		- Orientation models for Earth (ITRF93 - from 2000 to 2024) and Moon (MOON_PA - from 1550 to 2650).
		- *pck00010.tpc* which has the orientation model and Radii of multiple celestial bodies (IAU_<body_name>)
2. Considering that `<myPROJECTS>` is the path to your local workspace, create there a subfolder __"DOCKS"__ to store all functional modules.
3. The previously downloaded `docks_tools` directory should be decompressed and renamed to "__docks_tools__" (case-sensitive). Place it in `<myPROJECTS>/DOCKS/` subfolder.
4. The decompressed  `kernel` directory can be placed anywhere in your system. It is advised to place it in the `<myPROJECTS>/DOCKS/` subfolder.
5. Download the full module from [DOCKS GitLab](https://gitlab.obspm.fr/public_docks/trajectories/-/releases) at CENSUS (tag to the latest release in `(Gitlab) Repository > Tags`). Decompress it and rename it to "**Propagator**" (case-sensitive) and place it in `<myPROJECTS>/DOCKS/` subfolder.
6. Do the rest of the installation procedure below, even if you already did it for a previous version of the Propagator module.

*__Note__: If you want to do the installation in a virtual environment, then either activate the virtual environment and carry on the installation process or replace `python3` by path to your desired python.*



## 2.1	Linux Users

1. Run the following command in a terminal:
```
$ sudo apt update
```
2. Continue the installation based on your choice of Python [with](#211-with-minicondaanaconda) or [without](#212-without-minicondaanaconda) Miniconda/Anaconda.


### 2.1.1	With Miniconda/Anaconda

1. To help choose the best conda for yourself have a look at [Miniconda/Anaconda](https://docs.anaconda.com/free/distro-or-miniconda/).
2. Then follow the instalation guide for either [Miniconda](https://docs.anaconda.com/free/miniconda/miniconda-install/) or [Anaconda](https://docs.anaconda.com/free/anaconda/install/).
3. To verify conda is installed, open the terminal and run:
```
$ conda --version
```
Sometimes it might be needed to close and re-open the terminal.

4. It is recommended to create a conda environment for DOCKS. To create this environment with python 3.9 (a version above 3.9 could also be used), run the following commands in the terminal:
```
$ conda update conda
$ conda create -n docks python=3.9
```
5. Activate the conda environment with:
```
$ conda activate docks
```
Run this command to activate the environment whenever you want to run DOCKS propagator.

6. To verify python3 is correctly installed in the environment, run:
```
$ python3 --version
```
7. Change the directory of the terminal to your `<myPROJECTS>/DOCKS/Propagator` folder and install python requirements as follows:
```
$ python3 -m pip install --upgrade pip setuptools wheel
$ python3 -m pip install -r requirements.txt
```
8. To ensure everything works, close and re-open the terminal, activate the environemnt and test the installation by following the steps in [Test DOCKS Propagator](#4-test-docks-propagator)



### 2.1.2	Without Miniconda/Anaconda

1. If you don't have python3, install python 3.9+ and its basic libraries for your system. Run the following commands in a terminal:
```
$ sudo apt install -y python3.9
$ sudo apt install -y python3.9-pip
```
If you want to use a python virtual environment, then either activate the virtual environment and carry on the installation process or replace `python3` by the path to your desired python.

2. Now, change the directory of the terminal to your `<myPROJECTS>/DOCKS/Propagator` folder and, from there, run the following commands:
```
$ python3 -m pip install --upgrade pip setuptools wheel
$ python3 -m pip install -r requirements.txt
```
3. To verify python3 is correctly installed in the environment, run:
```
$ python3 --version
```
4. To ensure everything works, close and re-open the terminal and test the installation by following the steps in [Test DOCKS Propagator](#4-test-docks-propagator)



## 2.2	MS-Windows Users

1. Continue the installation based on your choice of Python [with](#221-with-minicondaanaconda) or [without](#222-without-minicondaanaconda) Miniconda/Anaconda.


### 2.2.1	With Miniconda/Anaconda

1. To help choose the best conda for yourself have a look at [Miniconda/Anaconda](https://docs.anaconda.com/free/distro-or-miniconda/).
2. Then follow the instalation guide for either [Miniconda](https://docs.anaconda.com/free/miniconda/miniconda-install/) or [Anaconda](https://docs.anaconda.com/free/anaconda/install/).
3. To verify conda is installed, open a terminal and run:
```
$ conda --version
```
If during the instalation or after you didn´t add conda to your PATH, then you'll need to use *Anaconda Prompt* as your terminal, otherwise you can use *Command Prompt* or *Windows PowerShell*.

4. It is recommended to create a conda environment for DOCKS. To create this environment with python 3.9 (a version above 3.9 could also be used), run the following commands in the terminal:
```
$ conda update conda
$ conda create -n docks python=3.9
```
5. Activate the conda environment with:
```
$ conda activate docks
```
Run this command to activate the envrionment whenever you want to run DOCKS propagator.

6. To verify python3 is correctly installed in the environment, run:
```
$ python --version
```
7. Change the directory of the terminal to your `<myPROJECTS>/DOCKS/Propagator` folder and install python requirments by running:
```
$ python -m pip install --upgrade pip setuptools wheel
$ python -m pip install -r requirements.txt
```
8. To ensure everything works, test the installation by following the steps in [Test DOCKS Propagator](#4-test-docks-propagator)




### 2.2.2	Without Miniconda/Anaconda

1. Download and install Python 3 (3.9+), if you do not have it.
    * Download [Python 3](https://www.python.org/downloads/windows/). You __must__ download 64-bit Python for 64-bit windows.
    * Select the Option __"Add Python to Path"__ while installing Python, otherwise it is possible to add after installation.
3. To verify python is correctly installed in the environment, open a terminal and run:
```
$ python --version
```
4. Change the directory of the terminal to your `<myPROJECTS>/DOCKS/Propagator` folder and install python requirments by running:
```
$ python -m pip install --upgrade pip setuptools wheel
$ python -m pip install -r requirements.txt
```
5. To ensure everything works, test the installation by following the steps in [Test DOCKS Propagator](#4-test-docks-propagator)




## 2.3	Use of VTS displayer

We strongly recommend the excellent free software VTS as a displayer for your data. VTS is provided for free by the French space agency CNES:

* download [VTS from here](http://timeloop.fr/vts/)
* decompress into the desired local folder

&nbsp;


# 3. Use DOCKS Propagator

__A "User Manual" is provided in PDF at the root__, i.e. `<myPROJECTS>/DOCKS/Propagator` after install. The detailed documentation and validation report may be sent to you upon request to [DOCKS mail](mailto:docks.contact@obspm.fr).


For an operational use, the User needs to set-up DOCKS' Propagator parameters before actually running the propagation engine. The user has to produce a configuration file `<config_file_name>.yaml` that customizes the propagator parameters. For more information on how to write this configuration file see *config_template.yaml* in the folder `<myPROJECTS>/DOCKS/Propagator/tutorial` for a brief descriptions or check the user manual for the detailed decription.


***Note**: The Propagator should be run using the python where it was installed. If you used virtual environment, make sure to activate the environment before running Propagator.*

To run a propagation from the terminal for Windows type:
```
$ python [RelativeOrAbsolute/path/to/propagator.py] [RelativeOrAbsolute/path/to/<config_file_name>.yaml]
```

For Linux:
```
$ python3 [RelativeOrAbsolute/path/to/propagator.py] [RelativeOrAbsolute/path/to/<config_file_name>.yaml]
```

When the propagation is over, the output reference trajectory can be found in the path & folder given in `<config_file_name>.yaml`.

A tutorial to start with DOCKS Propagator can be found in `<myPROJECTS>/DOCKS/Propagator/tutorial`.

***Note**: You can run the Propagator from any directory, just give the path to the files `propagator.py` and `config.yaml` relative to your current terminal directory.*

&nbsp;




# 4. Test DOCKS Propagator

***Warning**: To run these examples the provided kernels directory in [2](#2-installing-docks-propagator-module) needs to be used. This folder *kernel* must be located in `<myPROJECTS>/DOCKS` directory next to `docks_tools` and `Propagator`.*

In order to test DOOCKS Propagator installation, two config files *Test1.yaml* and *Test2.yaml* inside the folder `<PROJECT>/DOCKS/Propagator/examples` are provided. Run one of them by opening a terminal, going to this directory and typing:

For Windows:
```
$ python ../propagator.py Test1.yaml
$ python ../propagator.py Test2.yaml
```

For Linux:
```
$ python3 ../propagator.py Test1.yaml
$ python3 ../propagator.py Test2.yaml
```

If everything ran successfully, in this same directory, two output files *Test1_output.txt* and *Test2_output.txt* should have been created, containing the respective trajectories.

&nbsp;

# 5. Bugs and Suggestions

Report any bugs to [DOCKS GitLab](https://gitlab.obspm.fr/public_docks/trajectories) at CENSUS, Paris Observatory-PSL.

Provide suggestions to [DOCKS mail](mailto:docks.contact@obspm.fr).

Try our remote service by sending the Subject: `[server]` to [DOCKS mail](mailto:docks.contact@obspm.fr), you will be replied with a path to your own folder on our server and README files on how to proceed.

Thank you for your interest.

![image](icons/CENSUS_icon.png)
<h2 style="text-align: center;">DOCKS by CENSUS</h2>
<img src="./icons/Observatoire_de_Paris-CoMarquage-RGB_Ultra_violet.png" alt="drawing" width="400" style="float: right;"/>

<!-- # 8. Notes to Developers

The following features are anticipated but not implemented yet. Source code is located in the __src__ folder. DOCKS team welcome any contributions or suggestions and may provide some support on a case-by-case basis.

------------------------

* __Propagator/src/model_tensor/non_gravitational/drag.py__: acceleration due to aerodynamic drag

* __Propagator/src/model_tensor/non_gravitational/magnetic.py__: acceleration due to magnetic field

* __Propagator/src/model_tensor/non_gravitational/plasma.py__: acceleration due to a plasma

* __Propagator/src/data_accuracy__: a package to ensure sampling quality

------------------------ -->
